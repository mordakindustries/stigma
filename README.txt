Copyright 2022-2024 William Palmer

Permission is hereby authorised to view the relevant source code for the purposes of but not limited to education.
Please contact the copyright holder at William.G.Palmer@alumni.uts.edu.au for further information.

Created in Godot 3.4.2. Runs correctly on Godot 3.6.

To run the main game, use the Godot Editor and open the "MainGameScene.tscn", and press the "Play Scene" button. Levels can take up to 12 seconds to generate by design.