extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var playerSpawnPoint
var player = preload("res://Assets/Player.tscn")
var spawnpoints
var informedParent = null

# Called when the node enters the scene tree for the first time.
func _ready():
	playerSpawnPoint = $PlayerSpawnNode
	spawnpoints = $SpawningNodes
	

func spawn_player():
	var playerClone = player.instance()
	var sceneRoot = get_tree().root.get_children()[0]
	sceneRoot.add_child(playerClone)
	
	playerClone.global_transform = playerSpawnPoint.global_transform
	
	print("player spawned")

func spawn_items(validItemList):
	for iCount in range(spawnpoints.get_child_count()):
		if (validItemList.size() > 0):
			var newItem = validItemList[randi() % (validItemList.size())].instance()
			
			var sceneRoot = get_tree().root.get_children()[0]
			if (informedParent):
				informedParent.add_child(newItem)
			else:
				sceneRoot.add_child(newItem)
			
			newItem.global_transform = spawnpoints.get_child(iCount).global_transform
			newItem.global_translate(Vector3(0, 3, 0))
			newItem.scale = Vector3(1, 1, 1)
		
	
	
	

func setInformedParent(newParent):
	informedParent = newParent
	

func getSpawnLocations():
	var spawnLocationArray = []
	for point in spawnpoints:
		spawnLocationArray.append(point.global_transform.origin)
		
	
	return spawnLocationArray
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
