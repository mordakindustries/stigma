extends Spatial

# Declare member variables here. Examples:

const DAMAGE = 100

const AMMO_TYPE = "Bolt"
const AMMO_COST = 1

var isWeaponEnabled = false

var playerNode = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func fireWeapon():
	var ray = $RayCast
	ray.force_raycast_update()
	
	if ray.is_colliding():
		var body = ray.get_collider()
		
		if body == playerNode:
			pass
		elif body.has_method("hitDetected"):
			body.hitDetected(DAMAGE, ray.global_transform)
		

func equipWeapon():
	if isWeaponEnabled == false:
		isWeaponEnabled = true
		return true
	else:
		return false

func unequipWeapon():
	if isWeaponEnabled == true:
		isWeaponEnabled = false
		return true
	else:
		return false


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
