extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const BULLET_VELOCITY = 90
const DAMAGE = 50
const EXPLOSIVE_DAMAGE = 75
const SPREAD = 0
const RAD_TIME = 5.0
const RAD_DAMAGE = 9.0


const AMMO_TYPE = "Warhead"
const AMMO_COST = 1

var isWeaponEnabled = false

var projectile = preload("res://Assets/Warhead.tscn")

var isFiring = false
var FIRE_RATE = 2.5
var curFireTimer = 0

export(float) var universalDamageMultiplier = 1.0

#var FLAT_RECOIL = 1
var curRecoil = 1
#var recoilRecoveryMod = 0.00004

var playerNode = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func fireWeapon():
	isFiring = true
	#createProjectile()
	

func createProjectile():
	var clone = projectile.instance()
	var sceneRoot = get_tree().root.get_children()[0]
	sceneRoot.add_child(clone)
	#print(curRecoil)
	#print(str(SPREAD * curRecoil))
	var randomSpread = Vector3(rand_range(-SPREAD * curRecoil, SPREAD * curRecoil), rand_range(-SPREAD * curRecoil, SPREAD * curRecoil), 0)
	
	$SpreadPoint.rotation_degrees = randomSpread
	clone.global_transform = $SpreadPoint.global_transform
	clone.scale = Vector3(1, 1, 1)
	clone.BULLET_DAMAGE = DAMAGE * universalDamageMultiplier
	clone.BULLET_SPEED = BULLET_VELOCITY
	clone.radiationTime = RAD_TIME
	clone.radiationDamage = RAD_DAMAGE * universalDamageMultiplier
	clone.EXPLOSION_DAMAGE = EXPLOSIVE_DAMAGE * universalDamageMultiplier
	#$SpreadPoint.rotation_degrees = Vector3(0, 0, 0)

func equipWeapon():
	if isWeaponEnabled == false:
		isWeaponEnabled = true
		return true
	else:
		return false

func unequipWeapon():
	if isWeaponEnabled == true:
		isWeaponEnabled = false
		return true
	else:
		return false

func _physics_process(delta):
	if isWeaponEnabled:
		
		
		if curFireTimer > 0:
			curFireTimer -= delta
		else:
			if isFiring:
				if playerNode.hasAmmo(AMMO_TYPE, AMMO_COST):
					playerNode.costAmmo(AMMO_TYPE, AMMO_COST)
					createProjectile()
					curFireTimer = FIRE_RATE
				
		
		#if curRecoil > FLAT_RECOIL:
		#	curRecoil -= delta * recoilRecoveryMod
		#	curRecoil = clamp(curRecoil, FLAT_RECOIL, 3000)
		

func stopFiring():
	#curFireTimer = 0
	#curRecoil += curFireTimer * 3000
	#curRecoil = clamp(curRecoil, FLAT_RECOIL, 3000)
	isFiring = false
	#print(curRecoil)

