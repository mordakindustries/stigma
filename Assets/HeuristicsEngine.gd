extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# In-level heuristics
var storedFiring
var storedPickups
var storedDamage

var storedHostiles
var storedCollectibles

var storedKilledHostiles

var storedWeaponRanges

# Heuristics for between level storage
var levelStoredFiring 
var levelStoredPickups
var levelStoredDamage

var levelTotalHostiles
var levelTotalPickups

var levelTotalKilledHostiles

var levelStoredWeaponRanges

var curTime = 0.0

export(float) var baseMinMaxRangeMultFactor = 0.25

# Called when the node enters the scene tree for the first time.
func _ready():
	storedFiring = []
	storedPickups = []
	storedDamage = []
	
	storedHostiles = []
	storedCollectibles = []
	
	storedKilledHostiles = []
	
	storedWeaponRanges = []
	
	levelStoredFiring = []
	levelStoredPickups = []
	levelStoredDamage = []
	
	levelTotalHostiles = []
	levelTotalPickups = []
	
	levelTotalKilledHostiles = []
	
	levelStoredWeaponRanges = []
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	curTime += delta

func addHostileData(arrayOfHostiles):
	storedHostiles = arrayOfHostiles
	

func addPickupData(arrayOfPickups):
	storedCollectibles = arrayOfPickups
	

func endCurrentLevelHeuristics():
	levelStoredFiring.append(storedFiring)
	levelStoredPickups.append(storedPickups)
	levelStoredDamage.append(storedDamage)
	
	levelTotalHostiles.append(storedHostiles)
	levelTotalPickups.append(storedCollectibles)
	
	levelTotalKilledHostiles.append(storedKilledHostiles)
	
	levelStoredWeaponRanges.append(storedWeaponRanges)
	
	storedFiring = []
	storedPickups = []
	storedDamage = []
	
	storedHostiles = []
	storedCollectibles = []
	
	storedKilledHostiles = []
	
	storedWeaponRanges = []
	

func getRawLevelHeuristic(levelNumber, heuristicType="damage"):
	# Returns null if unsuccessful
	if (levelNumber <= levelStoredDamage.size() - 1):
		if (heuristicType == "damage"):
			return levelStoredDamage[levelNumber]
		elif (heuristicType == "firing"):
			return levelStoredFiring[levelNumber]
		elif (heuristicType == "pickups"):
			return levelStoredPickups[levelNumber]
		elif (heuristicType == "collectibles"):
			return levelTotalPickups[levelNumber]
		elif (heuristicType == "hostiles"):
			return levelTotalHostiles[levelNumber]
		elif (heuristicType == "range"):
			return levelStoredWeaponRanges[levelNumber]
		
		return levelStoredDamage
	
	
	return null
	

func GetHeuristic(heuristicType="all"):
	
	
	if (heuristicType == "all"):
		if (storedDamage.size() >= 1 && storedFiring.size() >= 1 && storedPickups.size() >= 1):
			return str(storedFiring) + "," + str(storedPickups)+ "," + str(storedDamage)
	elif (heuristicType == "firing"):
		if (storedFiring.size() >= 1):
			return str(storedFiring)
	elif (heuristicType == "pickups"):
		if (storedPickups.size() >= 1):
			return str(storedPickups)
	elif (heuristicType == "damage"):
		if (storedDamage.size() >= 1):
			return str(storedDamage)
	
	
	return "NO RETURNABLE DATA"
	
	
	

func gatherDamageHeuristic(source):
	var heuristicArray = [curTime, source]
	
	storedDamage.append(heuristicArray)
	print(str(storedDamage))
	

func gatherFiringHeuristic(gunFired):
	var heuristicArray = [curTime, gunFired]
	
	storedFiring.append(heuristicArray)
	print(str(storedFiring))
	

func gatherPickupHeuristic(pickupAcquired):
	var heuristicArray = [curTime, pickupAcquired]
	
	storedPickups.append(heuristicArray)
	print(str(storedPickups))
	

func gatherCollectibleHeuristic(newPickup):
	if (newPickup.has_method("getItemType")):
		storedCollectibles.append(newPickup.getItemType())
	
	#pass

func gatherHostileHeuristic(newHostile):
	if (newHostile.has_method("getThreatLevel")):
		storedHostiles.append(newHostile.getThreatLevel())
	
	
	#pass

func gatherKilledHostile(newDead):
	if (newDead.has_method("getThreatLevel")):
		storedKilledHostiles.append(newDead.getThreatLevel())
	
	

func gatherRangeHeuristic(weaponName, distance):
	var newWeaponRange = []
	newWeaponRange.append(weaponName)
	newWeaponRange.append(distance)
	storedWeaponRanges.append(newWeaponRange)
	

func getRangeHeuristicForLevel(weaponName="HORNET", levelNo=0):
	# Structure of the resultant heuristic measure: [minRange, maxRange]
	var minRange = 0.0
	var maxRange = 150.0
	
	# Calculate range heuristics here for the particular weapon
	# First step: collect all observed range values for the weapon
	var observedRanges = []
	if (levelNo <= levelStoredWeaponRanges.size() - 1 && levelNo >= 0):
		for observation in levelStoredWeaponRanges[levelNo]:
			if (observation[0] == weaponName):
				observedRanges.append(observation[1])
	else:
		for observation in storedWeaponRanges:
			if (observation[0] == weaponName):
				observedRanges.append(observation[1])
	
	# Second step: id baseline min/max range from observations
	var minObservedRange = 0.0
	var maxObservedRange = 150.0
	if (weaponName == "HORNET"):
		minObservedRange = 15.0
		maxObservedRange = 35.0
	if (weaponName == "SHOTGUN"):
		minObservedRange = 0.0
		maxObservedRange = 15.0
	if (weaponName == "LAUNCHER"):
		minObservedRange = 75.0
		maxObservedRange = 120.0
	
	
	
	if (observedRanges.size() > 1):
		minObservedRange = observedRanges[0]
		maxObservedRange = observedRanges[0]
		for observation in observedRanges:
			if observation < minObservedRange:
				minObservedRange = observation
			
			if observation > maxObservedRange:
				maxObservedRange = observation
			
		
		minObservedRange = minObservedRange * (1 - baseMinMaxRangeMultFactor)
		maxObservedRange = maxObservedRange * (1 + baseMinMaxRangeMultFactor)
		#minRange = minObservedRange
		#maxRange = maxObservedRange
		
	elif (observedRanges.size() == 1):
		minObservedRange = observedRanges[0]
		minObservedRange = minObservedRange * (1 - baseMinMaxRangeMultFactor)
		
		maxObservedRange = observedRanges[0]
		maxObservedRange = maxObservedRange * (1 + baseMinMaxRangeMultFactor)
		
		#minRange = minObservedRange
		#maxRange = maxObservedRange
		
	
	minRange = minObservedRange
	maxRange = maxObservedRange
	
	var rangeArray = [minRange, maxRange]
	return rangeArray
	

func getHeuristicCount(category="firing", type="HORNET", levelNo=-1):
	var returnCount = 0
	if (category == "firing"):
		if (levelNo == -1):
			for item in storedFiring:
				if (item[1] == type):
					returnCount += 1
		else:
			if (levelNo <= levelStoredFiring.size() - 1):
				for item in levelStoredFiring[levelNo]:
					if (item[1] == type):
						returnCount += 1
		
	elif (category == "pickups"):
		if (levelNo == -1):
			for item in storedPickups:
				if (item[1] == type):
					returnCount += 1
		else:
			if (levelNo <= levelStoredPickups.size() - 1):
				for item in levelStoredPickups[levelNo]:
					if (item[1] == type):
						returnCount += 1
		
	elif (category == "damage"):
		if (levelNo == -1):
			for item in storedDamage:
				if (item[1] == type):
					returnCount += 1
		else:
			if (levelNo <= levelStoredDamage.size() - 1):
				for item in levelStoredDamage[levelNo]:
					if (item[1] == type):
						returnCount += 1
		
	elif (category == "collectibles"):
		if (levelNo == -1):
			for item in storedCollectibles:
				if (item == type):
					returnCount += 1
		else:
			if (levelNo <= levelTotalPickups.size() - 1):
				for item in levelTotalPickups[levelNo]:
					if (item == type):
						returnCount += 1
		
	elif (category == "hostiles"):
		if (levelNo == -1):
			for item in storedHostiles:
				if (item == type):
					returnCount += 1
		else:
			if (levelNo <= levelTotalHostiles.size() - 1):
				for item in levelTotalHostiles[levelNo]:
					if (item == type):
						returnCount += 1
		
	elif (category == "kills"):
		if (levelNo == -1):
			for item in storedKilledHostiles:
				if (item == type):
					returnCount += 1
		else:
			if (levelNo <= levelTotalKilledHostiles.size() - 1):
				for item in levelTotalKilledHostiles[levelNo]:
					if (item == type):
						returnCount += 1
		
	
	return returnCount
	#pass

func getPointValueForHeuristic(aspect="HORNET", levelTarget=-1):
	var instanceCount = 0.0
	var instanceMax = 0.0
	var secondAspect = ""
	var standardPickupAmount = 1
	
	if (aspect == "PISTOL"):
		secondAspect = "9mm"
		standardPickupAmount = 8
	elif (aspect == "SHOTGUN"):
		secondAspect = "Buckshot"
		standardPickupAmount = 8
	elif (aspect == "HORNET"):
		secondAspect = "12.7mm"
		standardPickupAmount = 30
	elif (aspect == "LAUNCHER"):
		secondAspect = "40mm HE"
		standardPickupAmount = 6
	
	if (aspect in ["PISTOL", "SHOTGUN", "HORNET", "LAUNCHER"]):
		var firingAmount = (getHeuristicCount("firing", aspect, levelTarget) / standardPickupAmount)
		#instanceCount += (getHeuristicCount("firing", aspect, levelTarget) / standardPickupAmount)
		var gunPickupValue = getHeuristicCount("pickups", aspect, levelTarget)
		instanceCount += getHeuristicCount("pickups", aspect, levelTarget)
		var ammoPickupValue = getHeuristicCount("pickups", secondAspect, levelTarget)
		instanceCount += getHeuristicCount("pickups", secondAspect, levelTarget)
		var gunCollectValue = getHeuristicCount("collectibles", aspect, levelTarget)
		instanceMax += getHeuristicCount("collectibles", aspect, levelTarget)
		var ammoCollectValue = getHeuristicCount("collectibles", secondAspect, levelTarget)
		instanceMax += getHeuristicCount("collectibles", secondAspect, levelTarget)
		
	elif (aspect in ["light", "moderate", "heavy"]):
		instanceCount += getHeuristicCount("kills", aspect, levelTarget)
		instanceMax += getHeuristicCount("hostiles", aspect, levelTarget)
		
	
	print("aspect: " + str(aspect) + "\niCount: " + str(instanceCount) + "\niMax: "+ str(instanceMax))
	
	var returnValue
	if (instanceMax == 0.0 || instanceCount == 0.0):
		returnValue = 0.0
	else:
		if (aspect in ["PISTOL", "SHOTGUN", "HORNET", "LAUNCHER"]):
			var firingAmount = (getHeuristicCount("firing", aspect, levelTarget) / standardPickupAmount)
			var ammoPickupValue = getHeuristicCount("pickups", secondAspect, levelTarget)
			var gunPickupValue = getHeuristicCount("pickups", aspect, levelTarget)
			if (ammoPickupValue < 1.0 && gunPickupValue < 1.0):
				ammoPickupValue += 1
			
			
			returnValue = ((instanceCount / instanceMax) * 2) + (firingAmount / (ammoPickupValue + gunPickupValue))
		else:
			returnValue = instanceCount / instanceMax
	
	return returnValue

func publishHeuristicInfo(levelNo):
	# Set up information to export
	var content = "Level " + str(levelNo) + " Heuristic Results."
	
	# Heuristic counts for pickups
	content += "\n" + "Total Pistol Pickups: " + str(getHeuristicCount("collectibles", "PISTOL", -1) + getHeuristicCount("collectibles", "9mm", -1))
	content += "\n" + "Total Shotgun Pickups: " + str(getHeuristicCount("collectibles", "SHOTGUN", -1) + getHeuristicCount("collectibles", "Buckshot", -1))
	content += "\n" + "Total Hornet Pickups: " + str(getHeuristicCount("collectibles", "HORNET", -1) + getHeuristicCount("collectibles", "12.7mm", -1))
	content += "\n" + "Total Launcher Pickups: " + str(getHeuristicCount("collectibles", "LAUNCHER", -1) + getHeuristicCount("collectibles", "40mm HE", -1))
	
	content += "\n" + "Acquired Pistol Pickups: " + str(getHeuristicCount("pickups", "PISTOL", -1) + getHeuristicCount("pickups", "9mm", -1))
	content += "\n" + "Acquired Shotgun Pickups: " + str(getHeuristicCount("pickups", "SHOTGUN", -1) + getHeuristicCount("pickups", "Buckshot", -1))
	content += "\n" + "Acquired Hornet Pickups: " + str(getHeuristicCount("pickups", "HORNET", -1) + getHeuristicCount("pickups", "12.7mm", -1))
	content += "\n" + "Acquired Launcher Pickups: " + str(getHeuristicCount("pickups", "LAUNCHER", -1) + getHeuristicCount("pickups", "40mm HE", -1))
	
	content += "\n" + "Total Light Hostiles: " + str(getHeuristicCount("hostiles", "light", -1))
	content += "\n" + "Total Moderate Hostiles: " + str(getHeuristicCount("hostiles", "moderate", -1))
	content += "\n" + "Total Heavy Hostiles: " + str(getHeuristicCount("hostiles", "heavy", -1))
	
	content += "\n" + "Killed Light Hostiles: " + str(getHeuristicCount("kills", "light", -1))
	content += "\n" + "Killed Moderate Hostiles: " + str(getHeuristicCount("kills", "moderate", -1))
	content += "\n" + "Killed Heavy Hostiles: " + str(getHeuristicCount("kills", "heavy", -1))
	
	content += "\n" + "Calculated Range for Pistol: " + str(getRangeHeuristicForLevel("PISTOL", -1))
	content += "\n" + "Calculated Range for Shotgun: " + str(getRangeHeuristicForLevel("SHOTGUN", -1))
	content += "\n" + "Calculated Range for Hornet: " + str(getRangeHeuristicForLevel("HORNET", -1))
	content += "\n" + "Calculated Range for Launcher: " + str(getRangeHeuristicForLevel("LAUNCHER", -1))
	
	# Modifying Point Score
	content += "\n" + "Modifying Point Score for Pistol: " + str(getPointValueForHeuristic("PISTOL", -1))
	content += "\n" + "Modifying Point Score for Shotgun: " + str(getPointValueForHeuristic("SHOTGUN", -1))
	content += "\n" + "Modifying Point Score for Hornet: " + str(getPointValueForHeuristic("HORNET", -1))
	content += "\n" + "Modifying Point Score for Launcher: " + str(getPointValueForHeuristic("LAUNCHER", -1))
	
	content += "\n" + "Modifying Point Score for Light Hostiles: " + str(getPointValueForHeuristic("light", -1))
	content += "\n" + "Modifying Point Score for Moderate Hostiles: " + str(getPointValueForHeuristic("moderate", -1))
	content += "\n" + "Modifying Point Score for Heavy Hostiles: " + str(getPointValueForHeuristic("heavy", -1))
	
	# Anything else of relevance I guess
	
	# Total Shots fired per weapon
	content += "\n" + "Total Pistol Shots Fired: " + str(getHeuristicCount("firing", "PISTOL", -1))
	content += "\n" + "Total Shotgun Shots Fired: " + str(getHeuristicCount("firing", "SHOTGUN", -1))
	content += "\n" + "Total Hornet Shots Fired: " + str(getHeuristicCount("firing", "HORNET", -1))
	content += "\n" + "Total Launcher Shots Fired: " + str(getHeuristicCount("firing", "LAUNCHER", -1))
	
	content += "\n" + "End of File."
	
	
	# Export as a text document with a unique title
	var documentTitle = "heuristics" + str(levelNo)
	var exportDirectoryString = "user://" + documentTitle + ".txt"
	
	var textFile = File.new()
	textFile.open(exportDirectoryString, File.WRITE)
	textFile.store_string(content)
	textFile.close()
	
	
	
