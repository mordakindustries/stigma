extends RigidBody

const GRENADE_DAMAGE = 60

const GRENADE_TIME = 2
var grenadeTimer = 0

const EXPLOSION_WAIT_TIME = 0.5
var explosionWaitTimer = 0

var rigidShape
var grenadeMesh
var blastArea
var explosionParticles

# Called when the node enters the scene tree for the first time.
func _ready():
	rigidShape = $CollisionArea
	grenadeMesh = $CSGMeshing
	blastArea = $BlastArea
	explosionParticles = $Explosion
	
	explosionParticles.emitting = false
	explosionParticles.one_shot = true
	

func _physics_process(delta):
	if grenadeTimer < GRENADE_TIME:
		grenadeTimer += delta
		return
	else:
		if explosionWaitTimer <= 0:
			explosionParticles.emitting = true
			grenadeMesh.visible = false
			rigidShape.disabled = true
			mode = RigidBody.MODE_STATIC
			
			var bodies = blastArea.get_overlapping_bodies()
			for body in bodies:
				if body.has_method("hitDetect"):
					body.hitDetect(GRENADE_DAMAGE, body.global_transform.looking_at(global_transform.origin, Vector3(0, 1, 0)))
					
			
			# Can add sound here
		
		if explosionWaitTimer < EXPLOSION_WAIT_TIME:
			explosionWaitTimer += delta
		
		if explosionWaitTimer >= EXPLOSION_WAIT_TIME:
			queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
