extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const BULLET_VELOCITY = 100
const DAMAGE = 8
const SPREAD = 5
const PROJECTILE_COUNT = 11

const AMMO_TYPE = "Buckshot"
const AMMO_COST = 1

var isWeaponEnabled = false

var projectile = preload("res://Assets/Bullet.tscn")

var isFiring = false
var FIRE_RATE = 0.85
var curFireTimer = 0

var playerNode = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func fireWeapon():
	isFiring = true
	#createProjectile()

func createProjectile():
	var clone
	var sceneRoot = get_tree().root.get_children()[0]
	
	var i = 0
	
	while i < (PROJECTILE_COUNT - 1):
		clone = projectile.instance()
		sceneRoot.add_child(clone)
		
		var randomSpread = Vector3(rand_range(-SPREAD, SPREAD), rand_range(-SPREAD, SPREAD), 0)
		
		$SpreadPoint.rotation_degrees = randomSpread
		clone.global_transform = $SpreadPoint.global_transform
		clone.scale = Vector3(1, 1, 1)
		clone.BULLET_DAMAGE = DAMAGE
		clone.BULLET_SPEED = BULLET_VELOCITY
		clone.setWeapon("SHOTGUN")
		
		if (playerNode != null):
			clone.originSource = playerNode
		
		
		i += 1
	
	
	# Last projectile always fires directly ahead with no spread
	$SpreadPoint.rotation_degrees = Vector3(0, 0, 0)
	clone = projectile.instance()
	sceneRoot.add_child(clone)
	clone.global_transform = $SpreadPoint.global_transform
	clone.scale = Vector3(1, 1, 1)
	clone.BULLET_DAMAGE = DAMAGE
	clone.BULLET_SPEED = BULLET_VELOCITY
	
	

func equipWeapon():
	if isWeaponEnabled == false:
		isWeaponEnabled = true
		return true
	else:
		return false

func unequipWeapon():
	if isWeaponEnabled == true:
		isWeaponEnabled = false
		return true
	else:
		return false

func _physics_process(delta):
	if isWeaponEnabled:
		if curFireTimer > 0:
			curFireTimer -= delta
		else:
			if isFiring:
				if playerNode.hasAmmo(AMMO_TYPE, AMMO_COST):
					playerNode.costAmmo(AMMO_TYPE, AMMO_COST)
					createProjectile()
					curFireTimer = FIRE_RATE
				

func stopFiring():
	#curFireTimer = 0
	isFiring = false

