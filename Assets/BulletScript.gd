extends Spatial

var BULLET_SPEED = 12
var BULLET_DAMAGE = 40

const KILL_TIMER = 4
var timer = 0

var hitDetect = false

# Entity that has shot the bullet
var originSource

# Entity weapon type
var originWeapon = "PISTOL"

# Origin point for the spawned bullet, stored in global space
var originPoint = null

# Called when the node enters the scene tree for the first time.
func _ready():
	$Area.connect("body_entered", self, "collided")
	
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if (!originPoint):
		originPoint = global_transform.origin
	
	
	var forDir = global_transform.basis.z.normalized()
	global_translate(forDir * BULLET_SPEED * delta)

	timer += delta
	if timer > KILL_TIMER:
		queue_free()

func setWeapon(weaponName):
	originWeapon = weaponName
	


func collided(body):
	if hitDetect == false:
		
		if body.has_method("relegateNewRangeHeuristic"):
			if (originPoint):
				var distance = self.global_transform.origin.distance_to(originPoint)
				body.relegateNewRangeHeuristic(originWeapon, distance)
			
		
		if body.has_method("hitDetected"):
			body.hitDetected(BULLET_DAMAGE, global_transform, originPoint)
		elif body.has_method("hitDetect"):
			if body.has_method("heuristicHitDetect"):
				body.heuristicHitDetect(BULLET_DAMAGE, global_transform, originPoint, "normal", originSource)
			else:
				body.hitDetect(BULLET_DAMAGE, global_transform, originPoint)
			
			
			
		
		
	
	hitDetect = true
	queue_free()
