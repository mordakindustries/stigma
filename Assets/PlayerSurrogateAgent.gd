extends KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# AI States
enum AI_STATES {IDLE, ATTACK, EVADE, SEEK, COLLECT, NAVIGATE, EXPLORE, EXIT}
var currentState = AI_STATES.IDLE
var targetHostile
var targetPickupIndex
var targetNavigationLocation
var targetLookLocation
var targetLookDirection
var navPath = []

enum STRAFE_DIR {NONE, LEFT, RIGHT}
var strafingDirection = STRAFE_DIR.NONE

var knownItemTypes = ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "MEDSTIM", "MEDPACK", "INSTA-MED", "9mm", "Buckshot", "12.7mm", "Gauss", "PowerCell", "Bolt", "Warhead", "40mm HE", "PISTOL", "SHOTGUN", "HORNET", "RAILGUN", "REDEEMER", "LAUNCHER"]

# AI Navigation
var navPointDistanceThreshold = 5.0


# Scroll wheel input
var mouseScrollValue = 0
const MOUSE_SENSITIVITY_SCROLL_WHEEL = 0.08

# Health
var health = 100
var maxHealth = 100

var lowHealthThreshold = 0.4
var lowHealthSprintThreshold = 0.3 # Percentage below which agent will always sprint

# Movement
const GRAV = -9.8
const MAX_SPEED = 9
const JUMP_SPEED = 10
const ACCEL = 4

const MAX_SPRINT_SPEED = 14
const SPRINT_ACCEL = 14
var isSprinting = false

var vel = Vector3()
var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

# Misc
var camera
var rotationHelper
var flashlight

var MOUSE_SENSITIVITY = 0.35
var ARBITRARY_AIM_MULTIPLIER = 2.5
# 1.0: Standard
# 1.75: +75% Aiming boost (in theory)

var teamDesignation = "Player"

# Weapons
var currentWeaponName = "UNARMED"
var weapons = {"UNARMED":null, "PISTOL":null, "SHOTGUN":null, "HORNET":null, "RAILGUN":null, "REDEEMER":null, "LAUNCHER":null}
var weaponGot = {"UNARMED":true, "PISTOL":true, "SHOTGUN":false, "HORNET":false, "RAILGUN":false, "REDEEMER":false, "LAUNCHER":false}
const WEAPON_NUMBER_TO_NAME = {0:"UNARMED", 1:"PISTOL", 2:"SHOTGUN", 3:"HORNET", 6:"REDEEMER", 5:"RAILGUN", 4:"LAUNCHER"}
const WEAPON_NAME_TO_NUMBER = {"UNARMED":0, "PISTOL":1, "SHOTGUN":2, "HORNET":3, "REDEEMER":6, "RAILGUN":5, "LAUNCHER":4}
var changingWeaponName = "UNARMED"
var isChangingWeapon = false

# Ammunition
var ammunition = {"9mm":50, "Buckshot":0, "12.7mm":0, "Gauss":0, "PowerCell":0, "Bolt":0, "Warhead":1, "40mm HE":0}

# GUI Elements
var UIHealthLabel
var UIWeaponLabel
var UIEquipmentLabel
var UISeenLabel
var UIMoveLabel
# UI Radiation Elements
var UIRadiationPanel
var UIRadiationBar
# UI Weapon/Ammo Indicators
var UIPistolInd
var UIShotgunInd
var UIHornetInd
var UILauncherInd

var UIPistolAmmoInd
var UIShotgunAmmoInd
var UIHornetAmmoInd
var UILauncherAmmoInd
# UI State Indicator
var UIStateLabel
var UIExitKnownLabel

# Limited Inventory variables
var currentEquipmentName = "GLOWSTICK"
const EQUIPMENT_NUMBER_TO_NAME = {0:"GLOWSTICK", 1:"GRENADE", 2:"IMPACT", 3:"DETPACK", 4:"DETONATOR", 5:"NVG", 6:"MEDSTIM", 7:"MEDPACK"}
const EQUIPMENT_NAME_TO_NUMBER = {"GLOWSTICK":0, "GRENADE":1, "IMPACT":2, "DETPACK":3, "DETONATOR":4, "NVG":5, "MEDSTIM":6, "MEDPACK":7}

var grenadeAmounts = {"GLOWSTICK":4, "GRENADE":2, "IMPACT":3, "DETPACK":2, "NVG":100, "MEDSTIM":2, "MEDPACK":1}
var glowstick = preload("res://Assets/Glowstick.tscn")
var grenade = preload("res://Assets/Grenade.tscn")
var impactGrenade = preload("res://Assets/ImpactGrenade.tscn")

var grenadePoint

var throwStrength = 35

# Night Vision Goggles
var NVGOn = false
var normalEnvironment
var NVGEnvironment = preload("res://Environments/NVGEnvironment.tres")
var NVG_RECHARGE_TIMER = 0.5
var NVG_DRAIN_TIMER = 0.25
var nvgRechargeTime = 0

# Radiation Variables
var radiationPresent = false
var radiationStep = 3.0
const RAD_DAMAGE_TIMER = 5.0
var radTime = 0.0
var radTotal = 0

# Heuristics Engine Variables
var HeuristicsEngineObject

# Perception and Memory
var navManager = null
var levelExit = null
var levelExitSeen = false

var knownPickups = []
var knownPickupLocations = []
var knownPickupCertainty = []

var knownHostiles = []

var recheckTime = 0.0
var recheckTimer = 3.0

enum Objectives {EXIT, SURVIVE}
var currentObjectives = []

var maxHostileEngagementRange = 120.0

onready var isFiring = false

var rangeShotgun = [0.0, 15.0]
var rangeHornet = [15.0, 35.0]
var rangeLauncher = [75.0, 120.0]

var idealWeaponDistance = {"PISTOL":45.0, "SHOTGUN":10.0, "HORNET":30.0, "LAUNCHER":80.0, "REDEEMER":140.0, "RAILGUN":200.0}
var idealWeaponDistanceVariance = 0.75

# Called when the node enters the scene tree for the first time.
func _ready():
	camera = $RotationHelper/Camera
	rotationHelper = $RotationHelper
	flashlight = $RotationHelper/Flashlight
	grenadePoint = $RotationHelper/GrenadeThrowingPoint
	
	normalEnvironment = camera.environment
	
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED) # Change this
	
	weapons["PISTOL"] = $RotationHelper/GunFiringPoints/PistolFiringPoint/Pistol
	weapons["SHOTGUN"] = $RotationHelper/GunFiringPoints/ShotgunFiringPoint/Shotgun
	weapons["HORNET"] = $RotationHelper/GunFiringPoints/HornetFiringPoint/Hornet
	weapons["RAILGUN"] = $RotationHelper/GunFiringPoints/RailgunFiringPoint/Railgun
	weapons["REDEEMER"] = $RotationHelper/GunFiringPoints/RedeemerFiringPoint/Redeemer
	weapons["LAUNCHER"] = $RotationHelper/GunFiringPoints/LauncherFiringPoint/GrenadeLauncher
	
	var aimingPoint = $RotationHelper/GunAimingPoint.global_transform.origin
	
	for weapon in weapons:
		var weaponNode = weapons[weapon]
		if weaponNode != null:
			weaponNode.playerNode = self
			weaponNode.look_at(aimingPoint, Vector3(0, 1, 0))
			weaponNode.rotate_object_local(Vector3(0, 1, 0), deg2rad(180))
			weaponNode.hide()
	
	currentWeaponName = "UNARMED"
	changingWeaponName = "UNARMED"
	
	# Assigning UI Elements
	UIHealthLabel = $HUD/HUDHealth/HUDHealthPanel/Health
	UIWeaponLabel = $HUD/HUDWeapon/WeaponName
	UIEquipmentLabel = $HUD/HUDEquipment/EquipmentName
	UIRadiationPanel = $HUD/HUDHealth/HUDRad
	UIRadiationBar = $HUD/HUDHealth/HUDRad/ProgressBar
	UISeenLabel = $HUD/HUDSeen/SeenObjectsList
	UIMoveLabel = $HUD/HUDMoveIndicator/MovementIDText
	
	UIPistolInd = $HUD/HUDWeaponPanel/HUDPistolInd
	UIShotgunInd = $HUD/HUDWeaponPanel/HUDShotgunInd
	UIHornetInd = $HUD/HUDWeaponPanel/HUDHornetInd
	UILauncherInd = $HUD/HUDWeaponPanel/HUDLauncherInd
	
	UIPistolAmmoInd = $HUD/HUDWeaponPanel/HUDPistolAmmoInd
	UIShotgunAmmoInd = $HUD/HUDWeaponPanel/HUDShotgunAmmoInd
	UIHornetAmmoInd = $HUD/HUDWeaponPanel/HUDHornetAmmoInd
	UILauncherAmmoInd = $HUD/HUDWeaponPanel/HUDLauncherAmmoInd
	
	UIStateLabel = $HUD/HUDState/HUDStatePanel/State
	UIExitKnownLabel = $HUD/HUDState/HUDStatePanel/ExitKnown
	
	# Heuristics Engine finding
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x > 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is Spatial:
			if (sceneNode.get_children()[sceneNode.get_child_count() - x].has_method("GetHeuristic")):
				HeuristicsEngineObject = sceneNode.get_children()[sceneNode.get_child_count() - x]
				x = 0
				print(HeuristicsEngineObject.name)
		x -= 1
	
	# Heuristics setup
	if (sceneNode.has_method("getWeaponRange")):
		if (sceneNode.getWeaponRange()):
			setWeaponRange("HORNET", sceneNode.getWeaponRange("HORNET"))
			setWeaponRange("SHOTGUN", sceneNode.getWeaponRange("SHOTGUN"))
			setWeaponRange("LAUNCHER", sceneNode.getWeaponRange("LAUNCHER"))
			
			setAbritraryIdealWeaponDistance("PISTOL")
			setAbritraryIdealWeaponDistance("SHOTGUN")
			setAbritraryIdealWeaponDistance("HORNET")
			setAbritraryIdealWeaponDistance("LAUNCHER")
			
		
		
	
	currentObjectives.append(Objectives.EXIT)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	process_ai_state(delta)
	process_ai_input(delta)
	process_movement(delta)
	process_changing_weapons(delta)
	process_firing(delta)
	process_misc(delta)
	process_UI(delta)
	#process_NVG(delta)

func setIdealWeaponDistance(weaponName="HORNET", newIdealDistance=30.0):
	idealWeaponDistance[weaponName] = newIdealDistance
	
	#pass

func setAbritraryIdealWeaponDistance(weaponName="HORNET"):
	if (weaponName == "HORNET"):
		idealWeaponDistance[weaponName] = rangeHornet[1] - 5.0
	if (weaponName == "SHOTGUN"):
		idealWeaponDistance[weaponName] = rangeShotgun[1] - 5.0
	if (weaponName == "LAUNCHER"):
		idealWeaponDistance[weaponName] = rangeLauncher[0] + 5.0
	if (weaponName == "PISTOL"):
		idealWeaponDistance[weaponName] = (rangeHornet[1] * 1.5 + rangeShotgun[1] * 1.2) / 2.0
	
	#pass

func setWeaponRange(weaponName="HORNET", rangeArray=[15.0, 35.0]):
	if (weaponName == "HORNET"):
		rangeHornet = rangeArray
	if (weaponName == "SHOTGUN"):
		rangeShotgun = rangeArray
	if (weaponName == "LAUNCHER"):
		rangeLauncher = rangeArray
	
	#pass

func setNavManager(navigationManagerNode):
	navManager = navigationManagerNode
	

# AI State Functionality
# enum AI_STATES {IDLE, ATTACK, EVADE, COLLECT, NAVIGATE, EXPLORE}
#var currentState = AI_STATES.IDLE
#var targetHostile
#var targetPickupIndex
#var targetNavigationLocation
#var navPath = []
func aiIdle(delta): #For idling/resetting and re-evaluation
	
	aim_neutral(delta)
	isFiring = false
	navPath.clear()
	targetNavigationLocation = null
	targetHostile = null
	targetPickupIndex = null
	

func aiAttack(delta): #For attacking known hostiles
	if(!is_instance_valid(targetHostile) || targetHostile == null):
		# TargetPrioritisation
		assessTargets()
	elif (targetHostile): # Check if the hostile actually exists
		# Aim at the hostile
		horizontal_aim_at(delta, targetHostile.global_transform.origin)
		vertical_aim_at(delta, targetHostile.global_transform.origin)
		
		# Switch weapons depending on ammo and distance
		# -----
		
		# Check distance to hostile
		var distanceToHostile = self.global_transform.origin.distance_to(targetHostile.global_transform.origin)
		# Will need to double-check distance calculation, it's way off somehow
		#var altDist = self.transform.origin.distance_to(self.to_local(targetHostile.global_transform.))
		
		var idealWeaponList = []
		if (distanceToHostile >= rangeShotgun[0] && distanceToHostile <= rangeShotgun[1]):
			idealWeaponList.append("SHOTGUN")
		if (distanceToHostile >= rangeHornet[0] && distanceToHostile <= rangeHornet[1]):
			idealWeaponList.append("HORNET")
		if (distanceToHostile >= rangeLauncher[0] && distanceToHostile <= rangeLauncher[1]):
			idealWeaponList.append("LAUNCHER")
		
		idealWeaponList.append("PISTOL")
		if !("HORNET" in idealWeaponList):
			idealWeaponList.append("HORNET")
		if !("LAUNCHER" in idealWeaponList):
			idealWeaponList.append("LAUNCHER")
		if !("SHOTGUN" in idealWeaponList):
			idealWeaponList.append("SHOTGUN")
		
		var weaponChangeNumber = WEAPON_NAME_TO_NUMBER[currentWeaponName]
		var iWLCount = 0
		while iWLCount < idealWeaponList.size():
			if (checkAmmo(idealWeaponList[iWLCount])):
				weaponChangeNumber = WEAPON_NAME_TO_NUMBER[idealWeaponList[iWLCount]]
				iWLCount += idealWeaponList.size() + 2
			
			iWLCount += 1
			if (iWLCount == idealWeaponList.size()):
				weaponChangeNumber = 0
			
		
		
		#var weaponChangeNumber = WEAPON_NAME_TO_NUMBER[currentWeaponName]
		
		#if (checkAmmo("PISTOL")):
		#	if (currentWeaponName != "PISTOL"):
		#		weaponChangeNumber = 1
		#else:
		#	weaponChangeNumber = 0
		
		if isChangingWeapon == false:
			if WEAPON_NUMBER_TO_NAME[weaponChangeNumber] != currentWeaponName:
				changingWeaponName = WEAPON_NUMBER_TO_NAME[weaponChangeNumber]
				isChangingWeapon = true
		
		# -----
		
		var direction = rotationHelper.global_transform.origin - (targetHostile.global_transform.origin + rotationHelper.transform.origin)
		var rotationalDifference = direction.angle_to(rotationHelper.global_transform.basis.z)
		
		#rotationalDifference = 0.4
		var rotationDiffInDeg = rad2deg(rotationalDifference)
		if (rotationDiffInDeg < 5.0):
			isFiring = true
		else:
			isFiring = false
		
		
		# Compute movement needed to ideal attack distance
		var inputMVector = Vector2(0.0, 0.0)
		
		if ((distanceToHostile <= (idealWeaponDistance[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]] - idealWeaponDistanceVariance)) || (distanceToHostile >= (idealWeaponDistance[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]] + idealWeaponDistanceVariance))):
			if (distanceToHostile <= (idealWeaponDistance[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]] - idealWeaponDistanceVariance)):
				inputMVector.y -= 1
			else:
				inputMVector.y += 1
			
			#inputMVector
		
		
		if strafingDirection == STRAFE_DIR.LEFT:
			inputMVector.x -= 1
		if strafingDirection == STRAFE_DIR.RIGHT:
			inputMVector.x += 1
		
		inputMVector.normalized()
		
		move(delta, inputMVector)
		targetPickupIndex = null
		navPath.clear()
		
		# Strafe if on low-ish health
		# Double check ammo situation against the current weapon
		#move(delta)
		
		
		#var targetPosXZ = Vector3(targetHostile.global_transform.origin.x, self.global_transform.origin.y, targetHostile.global_transform.origin.z)
		#
		#var estimatedTransform = rotationHelper.global_transform.looking_at(targetPosXZ, Vector3.UP)
		#var directiondifference = rad2deg(rotationHelper.global_transform.basis.get_euler().angle_to(estimatedTransform.basis.get_euler()))
		#var direction = rotationHelper.global_transform.origin - targetHostile.global_transform.origin
		#if (direction.dot(rotationHelper.global_transform.basis.z) < 20):
		#	isFiring = true
		#else:
		#	isFiring = false
		
		# As a fallback, switch to the pistol
		
		# If the switched weapon has ammo, fire at the hostile
		
	
	#pass
	
	# Check if the target hostile still exists
	
	# If the engaged hostile no longer exists, remove it from known hostiles
	
	# Seek out the next hostile. If there are no other hostiles, resume idling
	
	
	# If there is a hostile that can be engaged
	
	# Rotate to face hostile
	# Strafe to avoid fire
	# Assess which weapon to use
	# Shoot at hostile when facing said hostile
	

func aiEvade(delta): #For searching for health pickups
	# Reset firing status
	isFiring = false
	
	if (navPath.empty() || targetPickupIndex == null):
		if (!navPath.empty()):
			navPath.clear()
		
		if (!knownPickups.empty()):
			# Assess each pickup.
			# Find closest pickup, using the pickup's approx. location in the world to
			# calculate distance.
			var prioritisedItemTypes = ["MEDSTIM", "MEDPACK", "INSTA-MED"]
			
			var distanceToItem = 0.0
			var bestDistance = INF # represents infinity
			var itemIndex = 0
			var selectedIndex = 0
			while (itemIndex < knownPickups.size()):
				if (knownPickups[itemIndex].getItemType() in prioritisedItemTypes):
					distanceToItem = self.global_transform.origin.distance_to(knownPickupLocations[itemIndex])
					if (distanceToItem < bestDistance):
						selectedIndex = itemIndex
						bestDistance = distanceToItem
					
				itemIndex = itemIndex + 1
				#distanceToItem = self.global_transform.origin.distance_to(knownPickupLocations[knownPickups.find(item)])
			
			targetPickupIndex = selectedIndex
			
			# Generate Path
			generatePath(knownPickupLocations[selectedIndex])
			print("Location: " + str(global_transform.origin) + "\nDestination: " + str(knownPickupLocations[selectedIndex]))
			
	else:
		moveAlongPath(delta)
	#pass
	
	# Similar to exploration, except focused on finding health and running away
	# from hostiles
	
	

func aiSeek(delta): #For when the agent's been damaged by an unknown source
	# Reset firing status
	isFiring = false
	
	if (targetLookLocation == null):
		pass
	else:
		aim_neutral(delta)
		
		horizontal_aim_at(delta, targetLookLocation)
		
		# Check how "complete" the rotation is
		var targetPosXZ = Vector3(targetLookLocation.x, self.global_transform.origin.y, targetLookLocation.z)
		
		var estimatedTransform = rotationHelper.global_transform.looking_at(targetPosXZ, Vector3.UP)
		var directiondifference = rad2deg(rotationHelper.global_transform.basis.get_euler().angle_to(estimatedTransform.basis.get_euler()))
		#var direction = rotationHelper.global_transform.origin - targetLookLocation
		#if (direction.dot(rotationHelper.global_transform.basis.z) < deg2rad(10)):
		#	targetLookLocation = null
			
		
		var direction = rotationHelper.global_transform.origin - (targetLookLocation + rotationHelper.transform.origin)
		var rotationalDifference = direction.angle_to(rotationHelper.global_transform.basis.z)
		
		#rotationalDifference = 0.4
		var rotationDiffInDeg = rad2deg(rotationalDifference)
		
		if (rotationDiffInDeg < 5.0):
			targetLookLocation = null
		elif (directiondifference < 5.0):
			targetLookLocation = null
		
		var lookingCheck = direction.dot(rotationHelper.global_transform.basis.z)
		
		print("lalala")
	
	#pass
	
	# Process and rotate towards direction of damage
	
	# upon rotation completion, can return to idling if there's no hostiles
	

func aiCollect(delta): #For searching and collecting any nearby known pickups. Involves navigation.
	# Reset firing status
	isFiring = false
	
	# If a pickup hasn't been selected
	if (navPath.empty() || targetPickupIndex == null):
		if (!navPath.empty()):
			navPath.clear()
		
		
		if (!knownPickups.empty()):
			# Assess each pickup.
			# Find closest pickup, using the pickup's approx. location in the world to
			# calculate distance.
			var prioritisedItemTypes = ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "9mm", "Buckshot", "12.7mm", "Gauss", "PowerCell", "Bolt", "Warhead", "40mm HE", "PISTOL", "SHOTGUN", "HORNET", "RAILGUN", "REDEEMER", "LAUNCHER"]
			
			var distanceToItem = 0.0
			var bestDistance = INF # represents infinity
			var itemIndex = 0
			var selectedIndex = 0
			while (itemIndex < knownPickups.size()):
				if (knownPickups[itemIndex].getItemType() in prioritisedItemTypes):
					distanceToItem = self.global_transform.origin.distance_to(knownPickupLocations[itemIndex])
					if (distanceToItem < bestDistance):
						selectedIndex = itemIndex
						bestDistance = distanceToItem
					
				itemIndex = itemIndex + 1
				#distanceToItem = self.global_transform.origin.distance_to(knownPickupLocations[knownPickups.find(item)])
			
			targetPickupIndex = selectedIndex
			
			# Generate Path
			generatePath(knownPickupLocations[selectedIndex])
			print("Location: " + str(global_transform.origin) + "\nDestination: " + str(knownPickupLocations[selectedIndex]))
			
	
	# If a pickup has been selected, approximate inputs for the goal of reaching that pickup
	if (navPath.size() >= 1):
		#Re-assess the pickup certainty, maybe consider picking a different pickup?
		var reassessPickup = knownPickups[targetPickupIndex]
		if (is_instance_valid(reassessPickup)):
			updateVisuallyKnownPickupInformation(knownPickups[targetPickupIndex])
		moveAlongPath(delta)
		
	

func aiNavigate(delta): #For navigating to a specific point in the world
	# Reset firing status
	isFiring = false
	
	# If the navPath contains navigation points, follow the path
	#if (!navPath.empty()):
	#	moveAlongPath(delta)
	#	
	
	if (navPath.size() >= 1):
		moveAlongPath(delta)
		
	
	

func aiExplore(delta): #For open-ended exploring of the map to find anything
	# Reset firing status
	isFiring = false
	
	# Check if the path is empty
	if (navPath.size() >= 1):
		moveAlongPath(delta)
		
	else:
		generatePath(Vector3(0, 0, 0))
		
	
	# If the navPath contains navigation points, follow the path
	
	# If the navPath is empty, generate a random location in the level
	# Generate a pathway to that random location
	
	
	
	#pass
	

func aiExit(delta): #For intentionally exiting a level
	
	isFiring = false
	if (navPath.size() >= 1):
		moveAlongPath(delta)
	else:
		if (levelExit):
			generatePath(levelExit.global_transform.origin)
		else:
			generatePath(Vector3(0, 0, 0))
	

func assessPickups(itemTypeArray):
	var itemArray = []
	
	if (knownPickups):
		if(knownPickups.size() >= 1):
			for item in knownPickups:
				if (item.has_method("getItemType")):
					if (item.getItemType() in itemTypeArray):
						itemArray.append(item)
	
	return itemArray
	

func assessTargets():
	var currentTargetIndex = -1
	var currentDistance = INF
	var testDistance = 0.0
	
	for target in knownHostiles:
		#currentTargetIndex = currentTargetIndex + 1
		if (is_instance_valid(target)):
			testDistance = self.global_transform.origin.distance_to(target.global_transform.origin)
			print("TD: " + str(testDistance) + ", CD: " + str(currentDistance))
			if (testDistance < currentDistance):
				#currentDistance = testDistance
				prioritiseTarget(target)
				print("NT: " + str(target.global_transform.origin))
				currentDistance = self.global_transform.origin.distance_to(targetHostile.global_transform.origin)
	
	

func checkAllHostileEngagementDistances():
	var testDistance = 0.0
	if (knownHostiles.size() >= 1):
		for target in knownHostiles:
			if (is_instance_valid(target)):
				testDistance = self.global_transform.origin.distance_to(target.global_transform.origin)
				if (testDistance < maxHostileEngagementRange):
					return true
	
	return false
	

func prioritiseTarget(newTarget):
	targetHostile = newTarget
	

func cleanUpMemory():
	# Cleans up memory allocation on known hostiles
	var count = 0
	while (count < knownHostiles.size()):
		if (!knownHostiles[count] || !is_instance_valid(knownHostiles[count])):
			knownHostiles.remove(count)
		else:
			count = count + 1
	
	

func checkAmmo(weapon="ALL"):
	if weapon == "PISTOL":
		if ammunition[weapons[weapon].AMMO_TYPE] > 0:
			return true
		else:
			return false
	elif weapon == "SHOTGUN":
		if (ammunition[weapons[weapon].AMMO_TYPE] > 0 && weaponGot[weapon] == true):
			return true
		else:
			return false
	elif weapon == "HORNET":
		if (ammunition[weapons[weapon].AMMO_TYPE] > 0 && weaponGot[weapon] == true):
			return true
		else:
			return false
	elif weapon == "LAUNCHER":
		if (ammunition[weapons[weapon].AMMO_TYPE] > 0 && weaponGot[weapon] == true):
			return true
		else:
			return false
	else:
		if (ammunition[weapons["PISTOL"].AMMO_TYPE] > 0 || (ammunition[weapons["SHOTGUN"].AMMO_TYPE] > 0 && weaponGot["SHOTGUN"] == true) || (ammunition[weapons["HORNET"].AMMO_TYPE] > 0 && weaponGot["HORNET"] == true) || (ammunition[weapons["LAUNCHER"].AMMO_TYPE] > 0 && weaponGot["LAUNCHER"] == true)):
			return true
		else:
			return false
	
	return true

func generatePath(destinationPoint):
	navPath.clear()
	navPath.append(destinationPoint)
	

func move(delta, movementInput=Vector2(0.0, 0.0)):
	var inputMVector = movementInput
	dir = Vector3()
	var camXFor = camera.get_global_transform()
	
	#if strafingDirection == STRAFE_DIR.LEFT:
	#	inputMVector.x -= 1
	#if strafingDirection == STRAFE_DIR.RIGHT:
	#	inputMVector.x += 1
	
	inputMVector.normalized()
	
	dir += -camXFor.basis.z * inputMVector.y
	dir += camXFor.basis.x * inputMVector.x
	
	if (health < (maxHealth * lowHealthSprintThreshold)):
		isSprinting = true
	else:
		isSprinting = false
	
	
	
	pass

func moveAlongPath(delta):
	
	# Walking around
	dir = Vector3()
	var camXFor = camera.get_global_transform()
	
	var inputMVector = Vector2()
	
	if (navPath.empty()):
		print("They told me I was crazy.")
	
	# Moving forward
	if (!navPath.empty()):
		var navPoint = navPath[0]
		
		var targetPosXZ = Vector3(navPoint.x, self.global_transform.origin.y, navPoint.z)
		var angleDiffXZ = acos(transform.basis.z.dot(
			transform.looking_at(targetPosXZ,
			Vector3.UP).basis.z))
		
		var angleToProduct = transform.basis.z.angle_to(transform.looking_at(targetPosXZ,
			Vector3.UP).basis.z)
		
		angleDiffXZ = angleToProduct
		
		if (is_nan(angleDiffXZ)):
			angleDiffXZ = deg2rad(0)
		
		UIMoveLabel.text = str(angleDiffXZ)
		
		if (angleDiffXZ < deg2rad(30)):
			inputMVector.y += 1
	
	
	#if (navPath.size() >= 1):
	#	inputMVector.y += 0.1
	
	# Possibly Strafing
	if strafingDirection == STRAFE_DIR.LEFT:
		inputMVector.x -= 1
	if strafingDirection == STRAFE_DIR.RIGHT:
		inputMVector.x += 1
	
	inputMVector.normalized()
	
	dir += -camXFor.basis.z * inputMVector.y
	dir += camXFor.basis.x * inputMVector.x
	# -----
	
	# -----
	# Sprinting as a means of moving fast or dodging
	if (health < (maxHealth * lowHealthSprintThreshold)):
		isSprinting = true
	else:
		isSprinting = false
	
	# -----
	
	# Camera look/rotation
	
	# For now, we just want the agent to rotate to face towards the current navpoint
	# Represented by navPath[0]
	
	# First, determine if the navpoint is to the "left" or "right"
	
	# Next, calculate necessary change in rotation to face navpoint
	
	if (!navPath.empty()):
		var navPoint = navPath[0]
		
		var targetPosXZ = Vector3(navPoint.x, self.global_transform.origin.y, navPoint.z)
		#var targetPosY = Vector3(self.global_transform.origin.x, navPoint.y, self.global_transform.origin.z)
		
		# use self.rotate_y() to rotate self
		# use rotationHelper.rotate_x() to rotate vertical aim
		var angleDiffXZ = acos(transform.basis.z.dot(
			transform.looking_at(targetPosXZ,
			Vector3.UP).basis.z))
		
		var angleToProduct = transform.basis.z.angle_to(transform.looking_at(targetPosXZ,
			Vector3.UP).basis.z)
		
		if (is_nan(angleDiffXZ)):
			angleDiffXZ = deg2rad(0)
		
		# Determine direction
		
		var localPosXZ = self.to_local(targetPosXZ)
		if (localPosXZ.x > 0):
			angleDiffXZ = angleDiffXZ * -1
		
		#targetPosXZ
		
		# If the direction is to the right, multiply by -1
		
		var rotationXZAmount = 0.0
		# calculate amount of rotation for turning over time
		rotationXZAmount = clamp(angleDiffXZ, -1 * delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER, delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER)
		
		#if (abs(angleDiffXZ) < 0.01):
		#	rotationXZAmount = 0.0
		#	#self.rotate_y(angleDiffXZ)
		
		
		#rotationAmount = 0.0
		#if (abs(angleDiffXZ) >= 0.01):
		#	self.rotate_y(rotationXZAmount)
		self.rotate_y(rotationXZAmount)
		
		# if the rotation is over/under corrected, correct it
		var stateOfRotation = rad2deg(self.global_transform.basis.get_euler().y)
		if (stateOfRotation >= deg2rad(180)):
			self.rotate_y(deg2rad(-360))
		
		if (stateOfRotation <= deg2rad(-180)):
			self.rotate_y(deg2rad(360))
		
		
		#print("Co-ords: " + str(self.transform.origin))
		
		
		# assume vertical aim is different from horizontal aim
		#var angleDiffY = acos(rotationHelper.global_transform.basis.y.dot(
		#	transform.looking_at(targetPosY,
		#	Vector3.UP).basis.y))
		
		var currentY = rotationHelper.global_transform.basis.get_euler().x
		#var angleY = atan(self.global_transform.origin.y / (pow(navPoint.x, 2) + pow(navPoint.z, 2)))
		var angleY = 0.0
		var angleDiffY = currentY - angleY
		
		if (angleY < currentY):
			angleDiffY = angleDiffY * -1
		
		#print(str(angleDiffY))
		#print(str(targetPosY))
		#print(str(rotationHelper.transform.origin))
		UIMoveLabel.text += "\n" + str(inputMVector) + "\n" + str(angleDiffXZ)
		
		aim_neutral(delta)
		#aim_at(delta, navPoint)
		
		#if(abs(angleDiffY) >= 0.0001):
		#	rotationHelper.rotate_x(angleDiffY)
		
		#rotationHelper.rotate_x(angleDiffY)
		#rotationHelper.rotate_x(rotationYAmount)
		
		# use 'delta' as a means by which to determine rotation amount
		
		
	else:
		UIMoveLabel.text = str(inputMVector)
	
	# NavPath updating
	#print("Current Co-ords: " + str(self.transform.origin))
	#print("Destination: " + str(navPath[0]))
	#print("Distance to: " + str(self.transform.origin.distance_to(navPath[0])))
	
	if (self.transform.origin.distance_to(navPath[0]) < navPointDistanceThreshold):
		navPath.remove(0)
		print("Point removed.")
	
	#print(str(navPath))
	
	# UI Movement Indicator Update
	#UIMoveLabel.text = str(inputMVector) + "\n" + str(angleDiffXZ)
	#UIMoveLabel.text = str(inputMVector)
	
	
	#if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
	#	rotationHelper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * -1))
	#	self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))
	#	
	#	var cameraRot = rotationHelper.rotation_degrees
	#	cameraRot = clamp(cameraRot.x, -70, 70)
	#	rotationHelper.rotation_degrees.x = cameraRot
	
	
	

func aim_neutral(delta): # Returns vertical aiming to a neutral position
	#print("Aiming Neutral Function.")
	var currentY = rotationHelper.global_transform.basis.get_euler().x
	#var angleY = atan(self.global_transform.origin.y / (pow(navPoint.x, 2) + pow(navPoint.z, 2)))
	var angleY = 0.0
	var angleDiffY = abs(currentY - angleY)
	
	if (is_nan(angleDiffY)):
		angleDiffY = 0
	
	if (angleY < currentY):
		angleDiffY = angleDiffY * -1
	
	#print(str(angleDiffY))
	#print(str(targetPosY))
	#print(str(rotationHelper.transform.origin))
	
	var rotationYAmount = 0.0
	
	rotationYAmount = clamp(angleDiffY, -1 * delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER, delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER)
	
	#if(abs(angleDiffY) < 0.0001):
	#	rotationYAmount = 0.0
	#	
	
	rotationHelper.rotate_x(rotationYAmount)
	var cameraRot = rotationHelper.rotation_degrees
	cameraRot = clamp(cameraRot.x, -70, 70)
	rotationHelper.rotation_degrees.x = cameraRot
	

func horizontal_aim_at(delta, abitraryPoint):
	var targetPosXZ = Vector3(abitraryPoint.x, self.global_transform.origin.y, abitraryPoint.z)
	#var targetPosY = Vector3(self.global_transform.origin.x, navPoint.y, self.global_transform.origin.z)
	
	# use self.rotate_y() to rotate self
	# use rotationHelper.rotate_x() to rotate vertical aim
	var angleDiffXZ = acos(transform.basis.z.dot(
		transform.looking_at(targetPosXZ,
		Vector3.UP).basis.z))
	
	var dotProduct = transform.basis.z.dot(transform.looking_at(targetPosXZ,
		Vector3.UP).basis.z)
	
	var angleToProduct = transform.basis.z.angle_to(transform.looking_at(targetPosXZ,
		Vector3.UP).basis.z)
	
	angleDiffXZ = angleToProduct
	
	if (is_nan(angleDiffXZ)):
		angleDiffXZ = deg2rad(0)
	
	
	
	
	# Determine direction
	
	var localPosXZ = self.to_local(targetPosXZ)
	if (localPosXZ.x > 0):
		angleDiffXZ = angleDiffXZ * -1
	
	UIMoveLabel.text = "aDXZ: " + str(angleDiffXZ)
	UIMoveLabel.text += "\ntPXZ: " + str(targetPosXZ)
	UIMoveLabel.text += "\nlPXZ: " + str(localPosXZ.x)
	UIMoveLabel.text += "\nagTO: " + str(transform.origin)
	UIMoveLabel.text += "\nhDst: " + str(transform.origin.distance_to(targetPosXZ))
	
	#targetPosXZ
	
	# If the direction is to the right, multiply by -1
	
	var rotationXZAmount = 0.0
	# calculate amount of rotation for turning over time
	rotationXZAmount = clamp(angleDiffXZ, -1 * delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER, delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER)
	
	#if (abs(angleDiffXZ) < 0.01):
	#	rotationXZAmount = 0.0
	#	#self.rotate_y(angleDiffXZ)
	
	
	#rotationAmount = 0.0
	#if (abs(angleDiffXZ) >= 0.01):
	#	self.rotate_y(rotationXZAmount)
	if (abs(angleDiffXZ) < abs(rotationXZAmount)):
		self.rotate_y(angleDiffXZ)
	else:
		self.rotate_y(rotationXZAmount)
	
	# if the rotation is over/under corrected, correct it
	var stateOfRotation = rad2deg(self.global_transform.basis.get_euler().y)
	if (stateOfRotation >= deg2rad(180)):
		self.rotate_y(deg2rad(-360))
	
	if (stateOfRotation <= deg2rad(-180)):
		self.rotate_y(deg2rad(360))
	
	

func vertical_aim_at(delta, abitraryPoint):
	var currentY = rotationHelper.transform.basis.get_euler().x
	var newY = self.to_local(abitraryPoint)
	UIMoveLabel.text += "\nnewY: " + str(newY)
	#newY = newY - rotationHelper.transform.origin
	#UIMoveLabel.text += "\nmodY: " + str(newY)
	var angleY = atan(newY.y / (sqrt(pow(newY.x, 2) + pow(newY.z, 2))))
	#var angleY = 0.0
	var angleDiffY = abs(currentY - angleY)
	
	if(is_nan(angleDiffY)):
		angleDiffY = deg2rad(0)
	
	if (angleY < currentY):
		angleDiffY = angleDiffY * -1
	
	#print(str(angleDiffY))
	#print(str(targetPosY))
	#print(str(rotationHelper.transform.origin))
	
	UIMoveLabel.text += "\ncurY: " + str(currentY)
	UIMoveLabel.text += "\nangY: " + str(angleY)
	UIMoveLabel.text += "\naDfY: " + str(angleDiffY)
	
	
	var rotationYAmount = 0.0
	
	rotationYAmount = clamp(angleDiffY, -1 * delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER, delta * MOUSE_SENSITIVITY * ARBITRARY_AIM_MULTIPLIER)
	
	#if(abs(angleDiffY) < 0.0001):
	#	rotationYAmount = 0.0
	#	
	
	rotationHelper.rotate_x(rotationYAmount)
	var cameraRot = rotationHelper.rotation_degrees
	cameraRot = clamp(cameraRot.x, -70, 70)
	rotationHelper.rotation_degrees.x = cameraRot
	

func process_ai_state(delta):
	# Clean up
	cleanUpMemory()
	
	
	# States:
	# - AI_STATES.IDLE: Idling in place. Backup state.
	# - AI_STATES.ATTACK: Attacks visible hostiles.
	# - AI_STATES.SEEK: Seeks out direction of most recent damage.
	# - AI_STATES.EVADE: Seeks out known health pickups.
	# - AI_STATES.COLLECT: Seeks out known pickups.
	# - AI_STATES.EXPLORE: Explores the level.
	# - AI_STATES.NAVIGATE: Fallback navigation state.
	#
	# Assign the new state as currentState = AI_STATES.[newState]
	
	# Decision Tree 2.0
	if (levelExitSeen == true):
		currentState = AI_STATES.EXIT
	elif (health <= lowHealthThreshold * maxHealth): # Low health tree
		currentState = AI_STATES.EXPLORE
		
		if (assessPickups(["MEDSTIM", "MEDPACK", "INSTA-MED"]).size() >= 1):
			currentState = AI_STATES.EVADE
			
		if (currentState == AI_STATES.EXPLORE):
			if (knownPickups.size() >= 1):
				currentState = AI_STATES.COLLECT
			elif (navPath.size() >= 1):
				currentState = AI_STATES.NAVIGATE
			elif (navPath.size() < 1):
				if (currentObjectives.size() >= 1):
					currentState = AI_STATES.EXPLORE
				else:
					currentState = AI_STATES.IDLE
	else:
		currentState = AI_STATES.IDLE
		
		if (checkAmmo("ALL")):
			if (knownHostiles.size() < 1 && targetLookLocation != null):
				currentState = AI_STATES.SEEK
			elif (knownHostiles.size() >= 1):
				if (is_instance_valid(knownHostiles[0])):
					if (checkAllHostileEngagementDistances()):
						currentState = AI_STATES.ATTACK
				
				if (currentState == AI_STATES.IDLE):
					if (targetLookLocation != null):
						currentState = AI_STATES.SEEK
				
		
		
		if (currentState == AI_STATES.IDLE):
			if (knownPickups.size() >= 1):
				var prioritisedItems = assessPickups(["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "9mm", "Buckshot", "12.7mm", "Gauss", "PowerCell", "Bolt", "Warhead", "40mm HE", "PISTOL", "SHOTGUN", "HORNET", "RAILGUN", "REDEEMER", "LAUNCHER"])
				#print(str(prioritisedItems))
				if (prioritisedItems.size() >= 1):
					currentState = AI_STATES.COLLECT
			elif (navPath.size() >= 1):
				currentState = AI_STATES.NAVIGATE
			elif (navPath.size() < 1):
				if (currentObjectives.size() >= 1):
					currentState = AI_STATES.EXPLORE
				else:
					currentState = AI_STATES.IDLE
	
	
	
	

func process_ai_state_old(delta):
	# Clean up
	cleanUpMemory()
	
	# Decision Tree
	if (health <= lowHealthThreshold * maxHealth): # Low health tree
		if (knownHostiles.size() >= 1):
			if (is_instance_valid(knownHostiles[0])):
				if (checkAmmo("PISTOL")):
					currentState = AI_STATES.ATTACK
				else:
					currentState = AI_STATES.COLLECT
			else:
				currentState = AI_STATES.COLLECT
		elif (knownPickups.size() >= 1):
			currentState = AI_STATES.COLLECT
		else:
			currentState = AI_STATES.IDLE
	else: # High health tree
		if (knownHostiles.size() >= 1):
			if (is_instance_valid(knownHostiles[0])):
				if (checkAmmo("PISTOL")):
					currentState = AI_STATES.ATTACK
				else:
					currentState = AI_STATES.COLLECT
			else:
				currentState = AI_STATES.COLLECT
		elif (knownPickups.size() >= 1):
			currentState = AI_STATES.COLLECT
		else:
			currentState = AI_STATES.IDLE
		if (knownHostiles.size() >= 1):
			if (is_instance_valid(knownHostiles[0])):
				if (checkAmmo("PISTOL")):
					currentState = AI_STATES.ATTACK
				else:
					currentState = AI_STATES.COLLECT
			else:
				currentState = AI_STATES.COLLECT
		elif (knownPickups.size() >= 1):
			currentState = AI_STATES.COLLECT
		else:
			currentState = AI_STATES.IDLE
	

#Unreal Engine AI Decision Tree Code
#	if (CurrentAgentState == AgentState::RESPAWNING) { // If the agent is respawning
#                        AgentRespawning(DeltaTime); // Use the Agent Respawning State
#                }
#                else if (CurrentAgentState == AgentState::PATROL)
#                {
#                        AgentPatrol();
#                        if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() >= HealthThreshold)
#                        {
#                                CurrentAgentState = AgentState::ENGAGE;
#                                Path.Empty();
#                        }
#                        else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() < HealthThreshold)
#                        {
#                                CurrentAgentState = AgentState::EVADE;
#                                Path.Empty();
#                        } else if (HealthComponent->HealthPercentageRemaining() < HealthThreshold) {
#                                CurrentAgentState = AgentState::RECOVER;
#                                Path.Empty();
#                        }
#                }
#                else if (CurrentAgentState == AgentState::ENGAGE)
#                {
#                        AgentEngage();
#                        if (!bCanSeeActor)
#                        {
#                                CurrentAgentState = AgentState::PATROL;
#                        }
#                        else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() < HealthThreshold)
#                        {
#                                CurrentAgentState = AgentState::EVADE;
#                                Path.Empty();
#                        }
#      }
#                else if (CurrentAgentState == AgentState::EVADE)
#                {
#                        AgentEvade();
#                        if (!bCanSeeActor)
#                        {
#                                CurrentAgentState = AgentState::RECOVER;
#                                //Path.Empty();
#                        }
#                        else if (bCanSeeActor && HealthComponent->HealthPercentageRemaining() >= HealthThreshold)
#                        {
#                                CurrentAgentState = AgentState::ENGAGE;
#                                Path.Empty();
#                        }
#                } else if (CurrentAgentState == AgentState::RECOVER) {
#                        AgentRecover();
#                        if (bCanSeeActor) {
#                                CurrentAgentState = AgentState::EVADE;
#                                Path.Empty();
#                        } else if (!bCanSeeActor && HealthComponent->HealthPercentageRemaining() >= HealthThreshold) {
#                                CurrentAgentState = AgentState::PATROL;
#                                Path.Empty();
#                        }
#                }
#                MoveAlongPath();
#        }


func process_ai_input(delta):
	# Enacting AI State Behaviour. Each function handles the equivalent input
	#print(str(currentState))
	if (currentState == AI_STATES.IDLE):
		#print("Idling.")
		aiIdle(delta)
	elif (currentState == AI_STATES.ATTACK):
		aiAttack(delta)
	elif (currentState == AI_STATES.COLLECT):
		aiCollect(delta)
	elif (currentState == AI_STATES.SEEK):
		aiSeek(delta)
	elif (currentState == AI_STATES.EXPLORE):
		aiExplore(delta)
	elif (currentState == AI_STATES.NAVIGATE):
		aiNavigate(delta)
	elif (currentState == AI_STATES.EVADE):
		aiEvade(delta)
	elif (currentState == AI_STATES.EXIT):
		aiExit(delta)
	else:
		#print("Unexpected Result. Idling.")
		aiIdle(delta)
	

func process_firing(delta):
	if (isFiring):
		fireWeapon()
		#print("Should fire")
	else:
		if (weapons):
			if weapons[currentWeaponName] != null:
				if weapons[currentWeaponName].has_method("stopFiring"):
					weapons[currentWeaponName].stopFiring()

func process_input(delta):
	# Convert this into accepting AI logic-based input instead of player input
	
	# -----
	# Walking around
	dir = Vector3()
	var camXFor = camera.get_global_transform()
	
	var inputMVector = Vector2()
	
	if Input.is_action_pressed("move_forward"):
		inputMVector.y += 1
	if Input.is_action_pressed("move_backward"):
		inputMVector.y -= 1
	if Input.is_action_pressed("move_left"):
		inputMVector.x -= 1
	if Input.is_action_pressed("move_right"):
		inputMVector.x += 1
	
	inputMVector.normalized()
	
	dir += -camXFor.basis.z * inputMVector.y
	dir += camXFor.basis.x * inputMVector.x
	# -----
	
	# -----
	# Sprinting as a means of moving fast or dodging
	if Input.is_action_pressed("ui_sprint"):
		isSprinting = true
	else:
		isSprinting = false
	# -----
	
	# -----
	# Flashlight - let there be light
	if Input.is_action_just_pressed("ui_light"):
		if flashlight.is_visible_in_tree():
			flashlight.hide()
		else:
			flashlight.show()
	# -----
	
	# -----
	# Jumping from the floor
	if is_on_floor():
		if Input.is_action_just_pressed("ui_jump"):
			vel.y = JUMP_SPEED
	
	
	# -----
	
	# -----
	# Capturing/Freeing the mouse cursor as needed
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	# -----
	
	# -----
	# Changing weapons
	var weaponChangeNumber = WEAPON_NAME_TO_NUMBER[currentWeaponName]
	
	if Input.is_key_pressed(KEY_1):
		weaponChangeNumber = 0
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_2):
		weaponChangeNumber = 1
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_3):
		weaponChangeNumber = 2
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_4):
		weaponChangeNumber = 3
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_5):
		weaponChangeNumber = 4
	if Input.is_key_pressed(KEY_6):
		weaponChangeNumber = 5
	if Input.is_key_pressed(KEY_7):
		weaponChangeNumber = 6
	if Input.is_key_pressed(KEY_8):
		weaponChangeNumber = 7
	
	if Input.is_action_just_pressed("weapon_switch_positive"):
		weaponChangeNumber += 1
	if Input.is_action_just_pressed("weapon_switch_negative"):
		weaponChangeNumber -= 1
	
	weaponChangeNumber = clamp(weaponChangeNumber, 0, WEAPON_NUMBER_TO_NAME.size() - 1)
	
	if isChangingWeapon == false:
		if WEAPON_NUMBER_TO_NAME[weaponChangeNumber] != currentWeaponName:
			changingWeaponName = WEAPON_NUMBER_TO_NAME[weaponChangeNumber]
			isChangingWeapon = true
			mouseScrollValue = weaponChangeNumber
	
	# -----
	
	# -----
	# Fire away!
	if Input.is_action_just_pressed("weapon_fire"):
		print("Firing!")
		fireWeapon()
	
	if Input.is_action_just_released("weapon_fire"):
		if weapons[currentWeaponName] != null:
			if weapons[currentWeaponName].has_method("stopFiring"):
				weapons[currentWeaponName].stopFiring()
	
	# -----
	
	# -----
	# Using items
	if Input.is_action_just_pressed("ui_useitem"):
		useEquipment()
	
	# -----
	
	# -----
	# Changing equipment
	if Input.is_action_just_pressed("ui_changeequipment"):
		if currentEquipmentName == "GLOWSTICK":
			currentEquipmentName = "GRENADE"
		elif currentEquipmentName == "GRENADE":
			currentEquipmentName = "IMPACT"
		elif currentEquipmentName == "IMPACT":
			currentEquipmentName = "NVG" 
		elif currentEquipmentName == "NVG":
			currentEquipmentName = "GLOWSTICK"
	
	
	# -----
	
	

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()
	
	vel.y += delta*GRAV
	
	var hVel = vel
	hVel.y = 0
	
	var target = dir
	if isSprinting:
		target *= MAX_SPRINT_SPEED
	else:
		target *= MAX_SPEED
	
	var accel
	if dir.dot(hVel) > 0:
		if isSprinting:
			accel = SPRINT_ACCEL
		else:
			accel = ACCEL
	else:
		accel = DEACCEL
	
	hVel = hVel.linear_interpolate(target, accel * delta)
	vel.x = hVel.x
	vel.z = hVel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
	
	# Reset direction
	dir = Vector3()
	
	

func process_changing_weapons(delta):
	
	if isChangingWeapon == true:
		if weaponGot[changingWeaponName] == true:
			var weaponUnequipped = false
			var currentWeapon = weapons[currentWeaponName]
			
			if currentWeapon == null:
				weaponUnequipped = true
			else:
				if currentWeapon.isWeaponEnabled == true:
					weaponUnequipped = currentWeapon.unequipWeapon()
					currentWeapon.hide()
				else:
					weaponUnequipped = true
			
			if weaponUnequipped == true:
				
				var weaponEquipped = false
				var weaponToEquip = weapons[changingWeaponName]
				
				# check if we have the weapon we are attempting to switch to
				if weaponToEquip == null:
					weaponEquipped = true
				else:
					if weaponToEquip.isWeaponEnabled == false:
						weaponToEquip.show()
						weaponEquipped = weaponToEquip.equipWeapon()
					else:
						weaponEquipped = true
				
				
				if weaponEquipped == true:
					isChangingWeapon = false
					currentWeaponName = changingWeaponName
					changingWeaponName = ""
		elif weaponGot[changingWeaponName] == false:
			mouseScrollValue = WEAPON_NAME_TO_NUMBER[currentWeaponName]
			isChangingWeapon = false
	

func process_UI(delta):
	UIHealthLabel.text = str(health)
	if currentWeaponName == "UNARMED":
		UIWeaponLabel.text = ""
	else:
		UIWeaponLabel.text = currentWeaponName + "\n" + weapons[currentWeaponName].AMMO_TYPE + "\n" + str(ammunition[weapons[currentWeaponName].AMMO_TYPE])
	
	if ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "NVG"].has(currentEquipmentName):
		UIEquipmentLabel.text = currentEquipmentName + "\n" + str(grenadeAmounts[currentEquipmentName])
	else:
		UIEquipmentLabel.text = currentEquipmentName + "\n" + "unusable"
	
	if (camera.has_method("getSeenObjectIDs")):
		UISeenLabel.text = str(camera.getSeenObjectIDs())
	else:
		UISeenLabel.text = "nothing seen, sorry boss"
	
	if (weaponGot["PISTOL"] == true):
		UIPistolInd.text = "Pistol: "
	else:
		UIPistolInd = "<unknown>: "
	
	if (weaponGot["SHOTGUN"] == true):
		UIShotgunInd.text = "Shotgun: "
	else:
		UIShotgunInd.text = "N/A: "
	
	if (weaponGot["HORNET"] == true):
		UIHornetInd.text = "Hornet: "
	else:
		UIHornetInd.text = "N/A: "
	
	if (weaponGot["LAUNCHER"] == true):
		UILauncherInd.text = "Launcher: "
	else:
		UILauncherInd.text = "N/A: "
	
	
	UIPistolAmmoInd.text = str(ammunition["9mm"])
	UIShotgunAmmoInd.text = str(ammunition["Buckshot"])
	UIHornetAmmoInd.text = str(ammunition["12.7mm"])
	UILauncherAmmoInd.text = str(ammunition["40mm HE"])
	
	UIStateLabel.text = str(currentState)
	if (currentState == AI_STATES.SEEK):
		UIStateLabel.text += "\ntLL: " + str(targetLookLocation)
		UIStateLabel.text += "\nrHF: " + str(rotationHelper.global_transform.basis.z)
	
	if (levelExitSeen):
		UIExitKnownLabel.text = "Yes"
	else:
		UIExitKnownLabel.text = "Exit Unknown"
	
	# Radiation UI Handling
	if radiationPresent == true:
		if UIRadiationPanel.is_visible() != true:
			UIRadiationPanel.show()
			#print("you should see RAD")
		
		UIRadiationBar.value = radTotal
	elif radiationPresent == false:
		if UIRadiationPanel.is_visible() == true:
			UIRadiationPanel.hide()
	
	
	

func fireWeapon():
	
	
	
	if isChangingWeapon == true:
		return
	
	if weapons[currentWeaponName] != null:
		#if ammunition[weapons[currentWeaponName].AMMO_TYPE] >= weapons[currentWeaponName].AMMO_COST:
		#	ammunition[weapons[currentWeaponName].AMMO_TYPE] -= weapons[currentWeaponName].AMMO_COST
		#	weapons[currentWeaponName].fireWeapon()
		
		#if hasAmmo(weapons[currentWeaponName].AMMO_TYPE, weapons[currentWeaponName].AMMO_COST):
		#	costAmmo(weapons[currentWeaponName].AMMO_TYPE, weapons[currentWeaponName].AMMO_COST)
		#	weapons[currentWeaponName].fireWeapon()
		
		if hasAmmo(weapons[currentWeaponName].AMMO_TYPE, weapons[currentWeaponName].AMMO_COST):
			weapons[currentWeaponName].fireWeapon()
		
		
	
	

func hasAmmo(ammoType, ammoAmount):
	if ammunition[ammoType] >= ammoAmount:
		return true
	else:
		return false
	

func costAmmo(ammoType, ammoAmount):
	ammunition[ammoType] -= ammoAmount
	gatherFiringHeuristic(currentWeaponName)
	

func useEquipment():
	# What equipment is being used
	if ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK"].has(currentEquipmentName):
		if grenadeAmounts[currentEquipmentName] > 0:
			grenadeAmounts[currentEquipmentName] -= 1
			var clone
			var throwForce = throwStrength
			
			if currentEquipmentName == "GRENADE":
				clone = grenade.instance()
			elif currentEquipmentName == "GLOWSTICK":
				clone = glowstick.instance()
			elif currentEquipmentName == "IMPACT":
				clone = impactGrenade.instance()
				throwForce += 15
			
			var sceneRoot = get_tree().root.get_children()[0]
			sceneRoot.add_child(clone)
			
			clone.global_transform = grenadePoint.global_transform
			clone.scale = Vector3(1, 1, 1)
			if clone.has_method("apply_impulse"):
				
				clone.apply_impulse(Vector3(0, 0, 0), -clone.global_transform.basis.z.normalized() * throwForce)
				#clone.rotation_degrees = Vector3(90 + clone.rotation_degrees.x, clone.rotation_degrees.y, clone.rotation_degrees.z)
	elif ["MEDPACK", "MEDSTIM"].has(currentEquipmentName):
		health += 10
	elif currentEquipmentName == "NVG":
		if NVGOn == false and grenadeAmounts["NVG"] > 15:
			NVGOn = true
		elif NVGOn == true:
			NVGOn = false
	

#func pickup(itemType, amount, newWeapon="null"):
#	# if the pickup is equipment or ammunition or a weapon.
#	# weapons always come with ammo, but ammo doesn't always come with guns
#	if ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "MEDSTIM", "MEDPACK"].has(itemType):
#		grenadeAmounts[itemType] += amount
#	elif ["9mm", "Buckshot", "12.7mm", "Gauss", "PowerCell", "Bolt", "Warhead", "40mm HE"].has(itemType):
#		ammunition[itemType] += amount
#		if newWeapon != "null":
#			print("new weapon!")
#			weaponGot[newWeapon] = true
#			print(weaponGot[newWeapon])
#	elif ["INSTA-MED"].has(itemType):
#		restoreHealth(amount)
#	
#	if newWeapon != "null":
#		gatherPickupHeuristic(newWeapon)
#	else:
#		gatherPickupHeuristic(itemType)
	
	
	

func superPickup(pickupItem):
	# Get the pickup item's attributes
	print("superPickup")
	
	if (pickupItem == null):
		pass
	
	var itemType = pickupItem.getItemType()
	var amount = pickupItem.getAmount()
	var newWeapon = pickupItem.getWeapon()
	
	
	
	# if the pickup is equipment or ammunition or a weapon.
	# weapons always come with ammo, but ammo doesn't always come with guns
	if ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "MEDSTIM", "MEDPACK"].has(itemType):
		grenadeAmounts[itemType] += amount
	elif ["9mm", "Buckshot", "12.7mm", "Gauss", "PowerCell", "Bolt", "Warhead", "40mm HE"].has(itemType):
		ammunition[itemType] += amount
		if newWeapon != "null":
			print("new weapon!")
			weaponGot[newWeapon] = true
			print(weaponGot[newWeapon])
	elif ["INSTA-MED"].has(itemType):
		restoreHealth(amount)
	
	if newWeapon != "null":
		gatherPickupHeuristic(newWeapon)
	else:
		gatherPickupHeuristic(itemType)
	
	# Here we update memory relevant to the particular pickup
	if (knownPickups.find(pickupItem) != -1):
		var itemID = knownPickups.find(pickupItem)
		knownPickups.remove(itemID)
		knownPickupLocations.remove(itemID)
		knownPickupCertainty.remove(itemID)
		
	
	

func restoreHealth(amount):
	health += amount

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotationHelper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * -1))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))
		
		var cameraRot = rotationHelper.rotation_degrees
		cameraRot = clamp(cameraRot.x, -70, 70)
		rotationHelper.rotation_degrees.x = cameraRot
	
	# Supposed to be mouse-scrolling, does not actually work for some reason
	if event in InputEventMouseButton and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event.button_index == BUTTON_WHEEL_UP or event.button_index == BUTTON_WHEEL_DOWN:
			print("mousewheel!")
			if event.button_index == BUTTON_WHEEL_UP:
				mouseScrollValue += MOUSE_SENSITIVITY_SCROLL_WHEEL
				print("mousewheel + up")
			elif event.button_index == BUTTON_WHEEL_DOWN:
				mouseScrollValue -= MOUSE_SENSITIVITY_SCROLL_WHEEL
				print("mousewheel + down")
			
			mouseScrollValue = clamp(mouseScrollValue, 0, WEAPON_NUMBER_TO_NAME.size() - 1)
			print (str(mouseScrollValue))
			
			if isChangingWeapon == false:
				var roundMouseScrollValue = int(round(mouseScrollValue))
				if WEAPON_NUMBER_TO_NAME[roundMouseScrollValue] != currentWeaponName:
					changingWeaponName = WEAPON_NUMBER_TO_NAME[roundMouseScrollValue]
					isChangingWeapon = true
					mouseScrollValue = roundMouseScrollValue
	

func process_NVG(delta):
	if NVGOn == true:
		if camera.environment != NVGEnvironment:
			camera.environment = NVGEnvironment
		
		nvgRechargeTime += delta
		if nvgRechargeTime > NVG_DRAIN_TIMER:
			grenadeAmounts["NVG"] -= 1
			nvgRechargeTime -= NVG_DRAIN_TIMER
		
		if grenadeAmounts["NVG"] <= 0:
			NVGOn = false
			nvgRechargeTime = 0
	
	if NVGOn == false:
		if camera.environment != normalEnvironment:
			camera.environment = normalEnvironment
		
		nvgRechargeTime += delta
		if nvgRechargeTime > NVG_RECHARGE_TIMER:
			grenadeAmounts["NVG"] += 1
			nvgRechargeTime -= NVG_RECHARGE_TIMER
			
			if grenadeAmounts["NVG"] > 100:
				grenadeAmounts["NVG"] = 100
	

func process_misc(delta):
	# process used for handling radiation hazards, and other misc.
	if radiationPresent == true:
		radTime += delta
		if radTime >= RAD_DAMAGE_TIMER:
			radTime -= RAD_DAMAGE_TIMER
			radTotal -= radiationStep
			deal_damage(radiationStep, "radiation")
			if radTotal <= 0.0:
				radTime = 0
				radTotal = 0
				radiationPresent = false
			
		
	
	

# Gathering of Firing statistics
func gatherFiringHeuristic(gunFired):
	if (HeuristicsEngineObject != null):
		HeuristicsEngineObject.gatherFiringHeuristic(gunFired)

# Gathering of Picking up Heuristics
func gatherPickupHeuristic(pickupAcquired):
	if (HeuristicsEngineObject != null):
		HeuristicsEngineObject.gatherPickupHeuristic(pickupAcquired)

# Gathering of Taking Damage
func gatherDamageHeuristic(source):
	if (HeuristicsEngineObject != null):
		HeuristicsEngineObject.gatherDamageHeuristic(source)

func applyRadiation(radiationAmount):
	radiationPresent = true
	radTotal += radiationAmount
	if radTotal >= 100:
		radTotal = 100
	

func hitDetect(damage, impactGlobalTrans, originPoint=null, damageType="normal"):
	deal_damage(damage, damageType)
	
	if (originPoint):
		targetLookLocation = originPoint
	


func deal_damage(damageAmount, damageType="normal"):
	health -= damageAmount
	#gatherDamageHeuristic(damageType)
	

func heuristicHitDetect(damage, impactGlobalTrans, originPoint=null, damageType="normal", source=null):
	hitDetect(damage, impactGlobalTrans)
	if (source != null):
		gatherDamageHeuristic(source)
	
	if (originPoint):
		targetLookLocation = originPoint
	
	

func getTeam():
	return teamDesignation

# Perception/Memory related functionality
func interpretObject(otherObject):
	if (otherObject):
		if (otherObject.has_method("identifyPickup")):
			if (knownPickups.find(otherObject) == -1): # If the pickup is unknown
				knownPickups.append(otherObject)
				knownPickupLocations.append(otherObject.global_transform.origin)
				knownPickupCertainty.append(0.99)
		elif (otherObject.has_method("getTeam")):
			if (otherObject.getTeam() != teamDesignation):
				knownHostiles.append(otherObject)
		elif (otherObject.has_method("exitLevel")):
			if (levelExit == null):
				setLevelExitObject(otherObject)
				levelExitSeen = true
		else:
			print("Unable to ID otherObject")
	

func setLevelExitObject(newExit):
	levelExit = newExit

func updateVisuallyKnownPickupInformation(otherObject):
	if (otherObject):
		
		
		var objectDistance = otherObject.global_transform.origin.distance_to(knownPickupLocations[targetPickupIndex])
		
		
		if (objectDistance > 0.001):
			knownPickupLocations[targetPickupIndex] = otherObject.global_transform.origin
			
		
	
	
	#pass

