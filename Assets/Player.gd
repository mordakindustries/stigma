extends KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Scroll wheel input
var mouseScrollValue = 0
const MOUSE_SENSITIVITY_SCROLL_WHEEL = 0.08

# Health
var health = 100

# Movement
const GRAV = -9.8
const MAX_SPEED = 9
const JUMP_SPEED = 10
const ACCEL = 4

const MAX_SPRINT_SPEED = 14
const SPRINT_ACCEL = 14
var isSprinting = false

var vel = Vector3()
var dir = Vector3()

const DEACCEL= 16
const MAX_SLOPE_ANGLE = 40

# Misc
var camera
var rotationHelper
var flashlight

var MOUSE_SENSITIVITY = 0.35

var teamDesignation = "Player"

# Weapons
var currentWeaponName = "UNARMED"
var weapons = {"UNARMED":null, "PISTOL":null, "SHOTGUN":null, "HORNET":null, "RAILGUN":null, "REDEEMER":null, "LAUNCHER":null}
var weaponGot = {"UNARMED":true, "PISTOL":true, "SHOTGUN":false, "HORNET":false, "RAILGUN":false, "REDEEMER":false, "LAUNCHER":false}
const WEAPON_NUMBER_TO_NAME = {0:"UNARMED", 1:"PISTOL", 2:"SHOTGUN", 3:"HORNET", 6:"REDEEMER", 5:"RAILGUN", 4:"LAUNCHER"}
const WEAPON_NAME_TO_NUMBER = {"UNARMED":0, "PISTOL":1, "SHOTGUN":2, "HORNET":3, "REDEEMER":6, "RAILGUN":5, "LAUNCHER":4}
var changingWeaponName = "UNARMED"
var isChangingWeapon = false

# Ammunition
var ammunition = {"9mm":50, "Buckshot":0, "12.7mm":0, "Gauss":0, "PowerCell":0, "Bolt":0, "Warhead":1, "40mm HE":0}

# GUI Elements
var UIHealthLabel
var UIWeaponLabel
var UIEquipmentLabel
# UI Radiation Elements
var UIRadiationPanel
var UIRadiationBar

# Limited Inventory variables
var currentEquipmentName = "GLOWSTICK"
const EQUIPMENT_NUMBER_TO_NAME = {0:"GLOWSTICK", 1:"GRENADE", 2:"IMPACT", 3:"DETPACK", 4:"DETONATOR", 5:"NVG", 6:"MEDSTIM", 7:"MEDPACK"}
const EQUIPMENT_NAME_TO_NUMBER = {"GLOWSTICK":0, "GRENADE":1, "IMPACT":2, "DETPACK":3, "DETONATOR":4, "NVG":5, "MEDSTIM":6, "MEDPACK":7}

var grenadeAmounts = {"GLOWSTICK":4, "GRENADE":2, "IMPACT":3, "DETPACK":2, "NVG":100, "MEDSTIM":2, "MEDPACK":1}
var glowstick = preload("res://Assets/Glowstick.tscn")
var grenade = preload("res://Assets/Grenade.tscn")
var impactGrenade = preload("res://Assets/ImpactGrenade.tscn")

var grenadePoint

var throwStrength = 35

# Night Vision Goggles
var NVGOn = false
var normalEnvironment
var NVGEnvironment = preload("res://Environments/NVGEnvironment.tres")
var NVG_RECHARGE_TIMER = 0.5
var NVG_DRAIN_TIMER = 0.25
var nvgRechargeTime = 0

# Radiation Variables
var radiationPresent = false
var radiationStep = 3.0
const RAD_DAMAGE_TIMER = 5.0
var radTime = 0.0
var radTotal = 0

# Heuristics Engine Variables
var HeuristicsEngineObject


# Called when the node enters the scene tree for the first time.
func _ready():
	camera = $RotationHelper/Camera
	rotationHelper = $RotationHelper
	flashlight = $RotationHelper/Flashlight
	grenadePoint = $RotationHelper/GrenadeThrowingPoint
	
	normalEnvironment = camera.environment
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	weapons["PISTOL"] = $RotationHelper/GunFiringPoints/PistolFiringPoint/Pistol
	weapons["SHOTGUN"] = $RotationHelper/GunFiringPoints/ShotgunFiringPoint/Shotgun
	weapons["HORNET"] = $RotationHelper/GunFiringPoints/HornetFiringPoint/Hornet
	weapons["RAILGUN"] = $RotationHelper/GunFiringPoints/RailgunFiringPoint/Railgun
	weapons["REDEEMER"] = $RotationHelper/GunFiringPoints/RedeemerFiringPoint/Redeemer
	weapons["LAUNCHER"] = $RotationHelper/GunFiringPoints/LauncherFiringPoint/GrenadeLauncher
	
	var aimingPoint = $RotationHelper/GunAimingPoint.global_transform.origin
	
	for weapon in weapons:
		var weaponNode = weapons[weapon]
		if weaponNode != null:
			weaponNode.playerNode = self
			weaponNode.look_at(aimingPoint, Vector3(0, 1, 0))
			weaponNode.rotate_object_local(Vector3(0, 1, 0), deg2rad(180))
			weaponNode.hide()
	
	currentWeaponName = "UNARMED"
	changingWeaponName = "UNARMED"
	
	UIHealthLabel = $HUD/HUDHealth/HUDHealthPanel/Health
	UIWeaponLabel = $HUD/HUDWeapon/WeaponName
	UIEquipmentLabel = $HUD/HUDEquipment/EquipmentName
	UIRadiationPanel = $HUD/HUDHealth/HUDRad
	UIRadiationBar = $HUD/HUDHealth/HUDRad/ProgressBar
	
	# Heuristics Engine finding
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x > 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is Spatial:
			if (sceneNode.get_children()[sceneNode.get_child_count() - x].has_method("GetHeuristic")):
				HeuristicsEngineObject = sceneNode.get_children()[sceneNode.get_child_count() - x]
				x = 0
				print(HeuristicsEngineObject.name)
		x -= 1
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	process_input(delta)
	process_movement(delta)
	process_changing_weapons(delta)
	process_misc(delta)
	process_UI(delta)
	process_NVG(delta)

func process_input(delta):
	
	# -----
	# Walking around
	dir = Vector3()
	var camXFor = camera.get_global_transform()
	
	var inputMVector = Vector2()
	
	if Input.is_action_pressed("move_forward"):
		inputMVector.y += 1
	if Input.is_action_pressed("move_backward"):
		inputMVector.y -= 1
	if Input.is_action_pressed("move_left"):
		inputMVector.x -= 1
	if Input.is_action_pressed("move_right"):
		inputMVector.x += 1
	
	inputMVector.normalized()
	
	dir += -camXFor.basis.z * inputMVector.y
	dir += camXFor.basis.x * inputMVector.x
	# -----
	
	# -----
	# Sprinting as a means of moving fast or dodging
	if Input.is_action_pressed("ui_sprint"):
		isSprinting = true
	else:
		isSprinting = false
	# -----
	
	# -----
	# Flashlight - let there be light
	if Input.is_action_just_pressed("ui_light"):
		if flashlight.is_visible_in_tree():
			flashlight.hide()
		else:
			flashlight.show()
	# -----
	
	# -----
	# Jumping from the floor
	if is_on_floor():
		if Input.is_action_just_pressed("ui_jump"):
			vel.y = JUMP_SPEED
	
	
	# -----
	
	# -----
	# Capturing/Freeing the mouse cursor as needed
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	# -----
	
	# -----
	# Changing weapons
	var weaponChangeNumber = WEAPON_NAME_TO_NUMBER[currentWeaponName]
	
	if Input.is_key_pressed(KEY_1):
		weaponChangeNumber = 0
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_2):
		weaponChangeNumber = 1
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_3):
		weaponChangeNumber = 2
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_4):
		weaponChangeNumber = 3
		#print(weaponGot[WEAPON_NUMBER_TO_NAME[weaponChangeNumber]])
	if Input.is_key_pressed(KEY_5):
		weaponChangeNumber = 4
	if Input.is_key_pressed(KEY_6):
		weaponChangeNumber = 5
	if Input.is_key_pressed(KEY_7):
		weaponChangeNumber = 6
	if Input.is_key_pressed(KEY_8):
		weaponChangeNumber = 7
	
	if Input.is_action_just_pressed("weapon_switch_positive"):
		weaponChangeNumber += 1
	if Input.is_action_just_pressed("weapon_switch_negative"):
		weaponChangeNumber -= 1
	
	weaponChangeNumber = clamp(weaponChangeNumber, 0, WEAPON_NUMBER_TO_NAME.size() - 1)
	
	if isChangingWeapon == false:
		if WEAPON_NUMBER_TO_NAME[weaponChangeNumber] != currentWeaponName:
			changingWeaponName = WEAPON_NUMBER_TO_NAME[weaponChangeNumber]
			isChangingWeapon = true
			mouseScrollValue = weaponChangeNumber
	
	# -----
	
	# -----
	# Fire away!
	if Input.is_action_just_pressed("weapon_fire"):
		print("Firing!")
		fireWeapon()
	
	if Input.is_action_just_released("weapon_fire"):
		if weapons[currentWeaponName] != null:
			if weapons[currentWeaponName].has_method("stopFiring"):
				weapons[currentWeaponName].stopFiring()
	
	# -----
	
	# -----
	# Using items
	if Input.is_action_just_pressed("ui_useitem"):
		useEquipment()
	
	# -----
	
	# -----
	# Changing equipment
	if Input.is_action_just_pressed("ui_changeequipment"):
		if currentEquipmentName == "GLOWSTICK":
			currentEquipmentName = "GRENADE"
		elif currentEquipmentName == "GRENADE":
			currentEquipmentName = "IMPACT"
		elif currentEquipmentName == "IMPACT":
			currentEquipmentName = "NVG" 
		elif currentEquipmentName == "NVG":
			currentEquipmentName = "GLOWSTICK"
	
	
	# -----
	
	

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()
	
	vel.y += delta*GRAV
	
	var hVel = vel
	hVel.y = 0
	
	var target = dir
	if isSprinting:
		target *= MAX_SPRINT_SPEED
	else:
		target *= MAX_SPEED
	
	var accel
	if dir.dot(hVel) > 0:
		if isSprinting:
			accel = SPRINT_ACCEL
		else:
			accel = ACCEL
	else:
		accel = DEACCEL
	
	hVel = hVel.linear_interpolate(target, accel * delta)
	vel.x = hVel.x
	vel.z = hVel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))
	
	

func process_changing_weapons(delta):
	
	if isChangingWeapon == true:
		if weaponGot[changingWeaponName] == true:
			var weaponUnequipped = false
			var currentWeapon = weapons[currentWeaponName]
			
			if currentWeapon == null:
				weaponUnequipped = true
			else:
				if currentWeapon.isWeaponEnabled == true:
					weaponUnequipped = currentWeapon.unequipWeapon()
					currentWeapon.hide()
				else:
					weaponUnequipped = true
			
			if weaponUnequipped == true:
				
				var weaponEquipped = false
				var weaponToEquip = weapons[changingWeaponName]
				
				# check if we have the weapon we are attempting to switch to
				if weaponToEquip == null:
					weaponEquipped = true
				else:
					if weaponToEquip.isWeaponEnabled == false:
						weaponToEquip.show()
						weaponEquipped = weaponToEquip.equipWeapon()
					else:
						weaponEquipped = true
				
				
				if weaponEquipped == true:
					isChangingWeapon = false
					currentWeaponName = changingWeaponName
					changingWeaponName = ""
		elif weaponGot[changingWeaponName] == false:
			mouseScrollValue = WEAPON_NAME_TO_NUMBER[currentWeaponName]
			isChangingWeapon = false
	

func process_UI(delta):
	UIHealthLabel.text = str(health)
	if currentWeaponName == "UNARMED":
		UIWeaponLabel.text = ""
	else:
		UIWeaponLabel.text = currentWeaponName + "\n" + weapons[currentWeaponName].AMMO_TYPE + "\n" + str(ammunition[weapons[currentWeaponName].AMMO_TYPE])
	
	if ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "NVG"].has(currentEquipmentName):
		UIEquipmentLabel.text = currentEquipmentName + "\n" + str(grenadeAmounts[currentEquipmentName])
	else:
		UIEquipmentLabel.text = currentEquipmentName + "\n" + "unusable"
	
	# Radiation UI Handling
	if radiationPresent == true:
		if UIRadiationPanel.is_visible() != true:
			UIRadiationPanel.show()
			#print("you should see RAD")
		
		UIRadiationBar.value = radTotal
	elif radiationPresent == false:
		if UIRadiationPanel.is_visible() == true:
			UIRadiationPanel.hide()
	
	
	

func fireWeapon():
	
	
	
	if isChangingWeapon == true:
		return
	
	if weapons[currentWeaponName] != null:
		#if ammunition[weapons[currentWeaponName].AMMO_TYPE] >= weapons[currentWeaponName].AMMO_COST:
		#	ammunition[weapons[currentWeaponName].AMMO_TYPE] -= weapons[currentWeaponName].AMMO_COST
		#	weapons[currentWeaponName].fireWeapon()
		
		#if hasAmmo(weapons[currentWeaponName].AMMO_TYPE, weapons[currentWeaponName].AMMO_COST):
		#	costAmmo(weapons[currentWeaponName].AMMO_TYPE, weapons[currentWeaponName].AMMO_COST)
		#	weapons[currentWeaponName].fireWeapon()
		
		if hasAmmo(weapons[currentWeaponName].AMMO_TYPE, weapons[currentWeaponName].AMMO_COST):
			weapons[currentWeaponName].fireWeapon()
		
		
	
	

func hasAmmo(ammoType, ammoAmount):
	if ammunition[ammoType] >= ammoAmount:
		return true
	else:
		return false
	

func costAmmo(ammoType, ammoAmount):
	ammunition[ammoType] -= ammoAmount
	gatherFiringHeuristic(currentWeaponName)
	

func useEquipment():
	# What equipment is being used
	if ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK"].has(currentEquipmentName):
		if grenadeAmounts[currentEquipmentName] > 0:
			grenadeAmounts[currentEquipmentName] -= 1
			var clone
			var throwForce = throwStrength
			
			if currentEquipmentName == "GRENADE":
				clone = grenade.instance()
			elif currentEquipmentName == "GLOWSTICK":
				clone = glowstick.instance()
			elif currentEquipmentName == "IMPACT":
				clone = impactGrenade.instance()
				throwForce += 15
			
			var sceneRoot = get_tree().root.get_children()[0]
			sceneRoot.add_child(clone)
			
			clone.global_transform = grenadePoint.global_transform
			clone.scale = Vector3(1, 1, 1)
			if clone.has_method("apply_impulse"):
				
				clone.apply_impulse(Vector3(0, 0, 0), -clone.global_transform.basis.z.normalized() * throwForce)
				#clone.rotation_degrees = Vector3(90 + clone.rotation_degrees.x, clone.rotation_degrees.y, clone.rotation_degrees.z)
	elif ["MEDPACK", "MEDSTIM"].has(currentEquipmentName):
		health += 10
	elif currentEquipmentName == "NVG":
		if NVGOn == false and grenadeAmounts["NVG"] > 15:
			NVGOn = true
		elif NVGOn == true:
			NVGOn = false
	

func pickup(itemType, amount, newWeapon="null"):
	# if the pickup is equipment or ammunition or a weapon.
	# weapons always come with ammo, but ammo doesn't always come with guns
	if ["GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "MEDSTIM", "MEDPACK"].has(itemType):
		grenadeAmounts[itemType] += amount
	elif ["9mm", "Buckshot", "12.7mm", "Gauss", "PowerCell", "Bolt", "Warhead", "40mm HE"].has(itemType):
		ammunition[itemType] += amount
		if newWeapon != "null":
			print("new weapon!")
			weaponGot[newWeapon] = true
			print(weaponGot[newWeapon])
	elif ["INSTA-MED"].has(itemType):
		restoreHealth(amount)
	
	if newWeapon != "null":
		gatherPickupHeuristic(newWeapon)
	else:
		gatherPickupHeuristic(itemType)
	
	
	

func restoreHealth(amount):
	health += amount

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotationHelper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * -1))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))
		
		var cameraRot = rotationHelper.rotation_degrees
		cameraRot = clamp(cameraRot.x, -70, 70)
		rotationHelper.rotation_degrees.x = cameraRot
	
	# Supposed to be mouse-scrolling, does not actually work for some reason
	if event in InputEventMouseButton and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event.button_index == BUTTON_WHEEL_UP or event.button_index == BUTTON_WHEEL_DOWN:
			print("mousewheel!")
			if event.button_index == BUTTON_WHEEL_UP:
				mouseScrollValue += MOUSE_SENSITIVITY_SCROLL_WHEEL
				print("mousewheel + up")
			elif event.button_index == BUTTON_WHEEL_DOWN:
				mouseScrollValue -= MOUSE_SENSITIVITY_SCROLL_WHEEL
				print("mousewheel + down")
			
			mouseScrollValue = clamp(mouseScrollValue, 0, WEAPON_NUMBER_TO_NAME.size() - 1)
			print (str(mouseScrollValue))
			
			if isChangingWeapon == false:
				var roundMouseScrollValue = int(round(mouseScrollValue))
				if WEAPON_NUMBER_TO_NAME[roundMouseScrollValue] != currentWeaponName:
					changingWeaponName = WEAPON_NUMBER_TO_NAME[roundMouseScrollValue]
					isChangingWeapon = true
					mouseScrollValue = roundMouseScrollValue
	

func process_NVG(delta):
	if NVGOn == true:
		if camera.environment != NVGEnvironment:
			camera.environment = NVGEnvironment
		
		nvgRechargeTime += delta
		if nvgRechargeTime > NVG_DRAIN_TIMER:
			grenadeAmounts["NVG"] -= 1
			nvgRechargeTime -= NVG_DRAIN_TIMER
		
		if grenadeAmounts["NVG"] <= 0:
			NVGOn = false
			nvgRechargeTime = 0
	
	if NVGOn == false:
		if camera.environment != normalEnvironment:
			camera.environment = normalEnvironment
		
		nvgRechargeTime += delta
		if nvgRechargeTime > NVG_RECHARGE_TIMER:
			grenadeAmounts["NVG"] += 1
			nvgRechargeTime -= NVG_RECHARGE_TIMER
			
			if grenadeAmounts["NVG"] > 100:
				grenadeAmounts["NVG"] = 100
	

func process_misc(delta):
	# process used for handling radiation hazards, and other misc.
	if radiationPresent == true:
		radTime += delta
		if radTime >= RAD_DAMAGE_TIMER:
			radTime -= RAD_DAMAGE_TIMER
			radTotal -= radiationStep
			deal_damage(radiationStep, "radiation")
			if radTotal <= 0.0:
				radTime = 0
				radTotal = 0
				radiationPresent = false
			
		
	
	

# Gathering of Firing statistics
func gatherFiringHeuristic(gunFired):
	if (HeuristicsEngineObject != null):
		HeuristicsEngineObject.gatherFiringHeuristic(gunFired)

# Gathering of Picking up Heuristics
func gatherPickupHeuristic(pickupAcquired):
	if (HeuristicsEngineObject != null):
		HeuristicsEngineObject.gatherPickupHeuristic(pickupAcquired)

# Gathering of Taking Damage
func gatherDamageHeuristic(source):
	if (HeuristicsEngineObject != null):
		HeuristicsEngineObject.gatherDamageHeuristic(source)

func applyRadiation(radiationAmount):
	radiationPresent = true
	radTotal += radiationAmount
	if radTotal >= 100:
		radTotal = 100
	

func hitDetect(damage, impactGlobalTrans, originPoint=null, damageType="normal"):
	deal_damage(damage, damageType)
	


func deal_damage(damageAmount, damageType="normal"):
	health -= damageAmount
	#gatherDamageHeuristic(damageType)
	

func heuristicHitDetect(damage, impactGlobalTrans, originPoint=null, damageType="normal", source=null):
	hitDetect(damage, impactGlobalTrans)
	if (source != null):
		gatherDamageHeuristic(source)
	

func getTeam():
	return teamDesignation
