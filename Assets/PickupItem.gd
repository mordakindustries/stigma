extends RigidBody

export(String, "null", "PISTOL", "SHOTGUN", "HORNET", "RAILGUN", "REDEEMER", "LAUNCHER") var weapon = "null"
export(String, "GRENADE", "GLOWSTICK", "IMPACT", "DETPACK", "MEDSTIM", "MEDPACK", "INSTA-MED", "9mm", "Buckshot", "12.7mm", "Gauss", "PowerCell", "Bolt", "Warhead", "40mm HE") var pickupType = "INSTA-MED"
export(int) var amount = 10


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$PickupArea.connect("body_entered", self, "trigger_entered")
	#print("collision should be setup")
	
	# Heuristics Engine finding
	var HeuristicsEngineObject
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x > 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is Spatial:
			if (sceneNode.get_children()[sceneNode.get_child_count() - x].has_method("GetHeuristic")):
				HeuristicsEngineObject = sceneNode.get_children()[sceneNode.get_child_count() - x]
				x = 0
				print(HeuristicsEngineObject.name)
		x -= 1
	
	if (HeuristicsEngineObject):
		if (HeuristicsEngineObject.has_method("gatherCollectibleHeuristic")):
			HeuristicsEngineObject.gatherCollectibleHeuristic(self)
	
	

func trigger_entered(body):
	#print("collision")
	#print(body.name)
	if body.has_method("superPickup"):
		body.superPickup(self)
		queue_free()
	if body.has_method("pickup"):
		if weapon != "null":
			body.pickup(pickupType, amount, weapon)
		else:
			body.pickup(pickupType, amount)
		queue_free()
	
	#if body.name == "Player":
	#	body.pickup(pickupType, amount)
	

func identifyPickup():
	return pickupType

func getAmount():
	return amount
	

func getItemType():
	return pickupType
	

func getWeapon():
	return weapon
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
