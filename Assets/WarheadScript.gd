extends Spatial

var BULLET_SPEED = 12
var BULLET_DAMAGE = 40
var EXPLOSION_DAMAGE = 75

# Radiation zone variables
var radiationZone = preload("res://Assets/WarheadImpactArea.tscn")
var radiationTime = 4.5
var radiationDamage = 9.0

const KILL_TIMER = 4
var timer = 0

const EXPLOSION_WAIT_TIME = 2.5
var explosionWaitTimer = 0

var rigidShape
var grenadeMesh
var blastArea
var explosionParticles

var hitDetected = false

# Called when the node enters the scene tree for the first time.
func _ready():
	
	
	$Area.connect("body_entered", self, "collided")
	
	rigidShape = $Area
	grenadeMesh = $CSGMeshing
	blastArea = $BlastArea
	explosionParticles = $Explosion
	
	explosionParticles.emitting = false
	explosionParticles.one_shot = true
	
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var forDir = global_transform.basis.z.normalized()
	global_translate(forDir * BULLET_SPEED * delta)

	
	if timer < KILL_TIMER:
		timer += delta
		return
	else:
		BULLET_SPEED = 0
		if explosionWaitTimer <= 0:
			explosionParticles.emitting = true
			grenadeMesh.visible = false
			rigidShape.get_children()[0].disabled = true
			
			var bodies = blastArea.get_overlapping_bodies()
			for body in bodies:
				if body.has_method("hitDetect"):
					body.hitDetect(EXPLOSION_DAMAGE, body.global_transform.looking_at(global_transform.origin, Vector3(0, 1, 0)))
			
			create_radiation_zone()
			
			# Can add sound here
			
		if explosionWaitTimer < EXPLOSION_WAIT_TIME:
			explosionWaitTimer += delta
		
		if explosionWaitTimer >= EXPLOSION_WAIT_TIME:
			queue_free()

func create_radiation_zone():
	var zone = radiationZone.instance()
	var sceneRoot = get_tree().root.get_children()[0]
	sceneRoot.add_child(zone)
	
	zone.global_transform = $Explosion.global_transform
	zone.scale = Vector3(1, 1, 1)
	
	zone.areaDamage = radiationDamage
	zone.EXPIRY_TIME = radiationTime
	
	


func collided(body):
	if hitDetected == false:
		if body.has_method("hitDetect"):
			body.hitDetect(BULLET_DAMAGE, global_transform)

	print(body.name)

	hitDetected = true
	BULLET_SPEED = 0
	timer += KILL_TIMER
