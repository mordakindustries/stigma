extends RigidBody

const GRENADE_DAMAGE = 60

const GRENADE_TIME = 5
var grenadeTimer = 0

const EXPLOSION_WAIT_TIME = 0.5
var explosionWaitTimer = 0

var rigidShape
var grenadeMesh
var blastArea
var explosionParticles

var impactZone

# Entity that has shot the bullet
var originSource

# Origin point for the spawned bullet, stored in global space
var originPoint = null

var originWeapon = "LAUNCHER"

# Called when the node enters the scene tree for the first time.
func _ready():
	rigidShape = $CollisionArea
	grenadeMesh = $CSGMeshing
	blastArea = $BlastArea
	explosionParticles = $Explosion
	
	impactZone = $ImpactArea
	
	impactZone.connect("body_entered", self, "collided")

	explosionParticles.emitting = false
	explosionParticles.one_shot = true


func _physics_process(delta):
	if (!originPoint):
		originPoint = global_transform.origin
	
	
	if grenadeTimer < GRENADE_TIME:
		grenadeTimer += delta
		return
	else:
		if explosionWaitTimer <= 0:
			explosionParticles.emitting = true
			grenadeMesh.visible = false
			rigidShape.disabled = true
			mode = RigidBody.MODE_STATIC

			var bodies = blastArea.get_overlapping_bodies()
			for body in bodies:
				if body.has_method("hitDetect"):
					body.hitDetect(GRENADE_DAMAGE, body.global_transform.looking_at(global_transform.origin, Vector3(0, 1, 0)))


			# Can add sound here

		if explosionWaitTimer < EXPLOSION_WAIT_TIME:
			explosionWaitTimer += delta

		if explosionWaitTimer >= EXPLOSION_WAIT_TIME:
			queue_free()

func collided(body):
	if body.has_method("hitDetect"):
		if body.has_method("relegateNewRangeHeuristic"):
			if (originPoint):
				var distance = self.global_transform.origin.distance_to(originPoint)
				body.relegateNewRangeHeuristic(originWeapon, distance)
			
			
		
		
		if grenadeTimer >= 0.2:
			grenadeTimer += GRENADE_TIME

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
