extends MeshInstance


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Mesh Data Storage
var surfaceArray = []
var verts = PoolVector3Array()
var uvs = PoolVector2Array()
var normals = PoolVector3Array()
var indices = PoolIntArray()

var meshArray = ArrayMesh.new()

var meshMaterial = preload("res://ReusableMaterials/SpotlightMaterial2.tres")

onready var collisionZone = get_node("CollisionArea/CollisionZone")

var collisionVerts = PoolVector3Array()

# Mesh Generation Configuration
var noiseScale = 100.0
var noiseRoughness = 0.01

var heightOffset = 5
var widthOffset = 5
var cliffHeight = -250.0

var bDoCliffs = true
var bDoComplexCliffs = false

var arrayHeight = 70
var arrayWidth = 70

var gridSize = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#generateLandscape()
	
	#ResourceSaver.save("res://Assets/PCG/generatedLandscape.tres", meshArray, 32)
	

func generateLandscape():
	surfaceArray.resize(meshArray.ARRAY_MAX)
	
	# Code to generate a landscape here
	
	# Noise Generation
	var noise = OpenSimplexNoise.new()
	# Noise Configuration
	noise.seed = randi() # Seed
	noise.octaves = 4 # Number of regenerations, more = smoother at a performance cost
	noise.period = 20.0 # Lower period = higher noise variation
	noise.persistence = 0.8 # Amount of contribution per octave to noise generation
	
	# get_noise_3d(X, Y, Z)
	var xPoint = 0.0
	var yPoint = 0.0
	var zPoint = 0.0
	
	for row in range(arrayHeight):
		for col in range(arrayWidth):
			xPoint = col * gridSize
			zPoint = row * gridSize
			
			yPoint = noise.get_noise_2d(xPoint * noiseRoughness, zPoint * noiseRoughness) * noiseScale
			
			verts.append(Vector3(xPoint, yPoint, zPoint))
			
			uvs.append(Vector2(col * 1.0, row * 1.0))
			
			normals.append(Vector3(xPoint, yPoint, zPoint))
			
	
	var yHeight = 0.0 # Used for storing the original height of the vertice we are modifying to create the cliffs.
	var yLerpFactor = 0.0 # Used for storing the Lerp Factor to smooth the complex cliff faces.
	
	if (heightOffset >= 2 && widthOffset >= 2 && bDoCliffs == true):# If both HeightOffset and WidthOffset are set to at least 2 and "bDoCliffs" is true
		if (bDoComplexCliffs == false): # If we want simple cliffs that only exist on the border of the total map.
			for row in range(arrayHeight): # Nested Row/Col for loop
				for col in range(arrayWidth):
					# Bottom/Top Edge Case
					if (row == 0 || row == arrayHeight - 1): # If we are on the bottom/top row.
						verts[row * arrayWidth + col].y = cliffHeight
					elif (col == 0 || col == arrayWidth - 1): # Side Edge Cases
						# If we are using the edges of the base map.
						verts[row * arrayWidth + col].y = cliffHeight
						
						
					
			
		else:
			for row in range(arrayHeight): # Nested Row/Col for loop
				for col in range(arrayWidth):
					# Bottom/Top Edge Case
					if (row < (heightOffset - 1)):
						if (col < (widthOffset - 1)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < col):
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
								
							elif (col < row):
								yLerpFactor = ((float(widthOffset) - 1.0 - float(col)) / (float(widthOffset) - 1.0))
								
							else:
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1.0 - float(col)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0))
								
							
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						elif (col > (arrayWidth - widthOffset)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < (arrayWidth - col - 1.0)):
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
								
							elif((arrayWidth - col - 1.0) < row):
								yLerpFactor = ( float(widthOffset) /( float(widthOffset) - 1.0)) - ((( float(arrayWidth) + float(widthOffset) - float(col) - 1.0) / ( float(widthOffset) - 1.0)) - 1.0)
								
							else:
								yLerpFactor = (float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1 - (float(arrayWidth) - float(col) - 1.0)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0)
								
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						else:
							yHeight = verts[row * arrayWidth + col].y
							yLerpFactor = ((float(heightOffset) - 1.0 - row) / (float(heightOffset) - 1.0))
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
					elif (row > (arrayHeight - heightOffset)): # <- Need to figure out equivalent for ( widthOffset /( widthOffset - 1.0)) - ((( arrayWidth + widthOffset - col - 1.0) / ( widthOffset - 1.0)) - 1.0)
						if (col < (widthOffset - 1)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < col):
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
								
							elif (col < row):
								yLerpFactor = ((float(widthOffset) - 1.0 - float(col)) / (float(widthOffset) - 1.0))
								
							else:
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1.0 - float(col)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0))
								
							
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						elif (col > (arrayWidth - widthOffset)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < (arrayWidth - col - 1.0)):
								yLerpFactor = ((float(heightOffset) - 1.0 - row) / (heightOffset - 1.0))
								
							elif((arrayWidth - col - 1.0) < row):
								yLerpFactor = ( float(widthOffset) /( float(widthOffset) - 1.0)) - ((( float(arrayWidth) + float(widthOffset) - float(col) - 1.0) / ( float(widthOffset) - 1.0)) - 1.0)
								
							else:
								yLerpFactor = (float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1 - (float(arrayWidth) - float(col) - 1.0)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0)
								
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						else:
							yHeight = verts[row * arrayWidth + col].y
							yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
					
					
					
#					if (row == 0 || row == arrayHeight - 1): # If we are on the bottom/top row.
#						verts[row * arrayWidth + col].y = cliffHeight
#
#					elif (col == 0 || col == arrayWidth - 1): # Side Edge Cases
#						# If we are using the edges of the base map.
#						verts[row * arrayWidth + col].y = cliffHeight
#
#
					
			
	
	
	for row in range(arrayHeight - 1):
		for col in range(arrayWidth - 1):
			indices.append(row * arrayWidth + col)
			indices.append((row + 1) * arrayWidth + col)
			indices.append(row * arrayWidth + (col + 1))
			
			indices.append(row * arrayWidth + col + 1)
			indices.append((row + 1) * arrayWidth + col)
			indices.append((row + 1) * arrayWidth + (col + 1))
			
			# Setting up collision data for the shape
			collisionVerts.append(verts[row * arrayWidth + col])
			collisionVerts.append(verts[(row + 1) * arrayWidth + col])
			collisionVerts.append(verts[row * arrayWidth + (col + 1)])
			
			collisionVerts.append(verts[row * arrayWidth + col + 1])
			collisionVerts.append(verts[(row + 1) * arrayWidth + col])
			collisionVerts.append(verts[(row + 1) * arrayWidth + (col + 1)])
			
			
	
	
	
	
	#
	
	# Assigning arrays to the mesh array for mesh instantiation
	surfaceArray[Mesh.ARRAY_VERTEX] = verts
	surfaceArray[Mesh.ARRAY_TEX_UV] = uvs
	surfaceArray[Mesh.ARRAY_NORMAL] = normals
	surfaceArray[Mesh.ARRAY_INDEX] = indices
	
	# Create a mesh from the generated mesh array
	meshArray.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, surfaceArray)
	
	self.mesh = meshArray
	self.mesh.surface_set_material(0, meshMaterial)
	
	#print("OG Collision: " + str(collisionZone.get("map_data")))
	#
	#collisionZone.shape = HeightMapShape.new()
	#collisionZone.shape.set("map_width", arrayWidth)
	#collisionZone.shape.set("map_depth", arrayHeight)
	#collisionZone.shape.set("map_data", verts)
	#
	#print("NG Collision: " + str(collisionZone.get("map_data")))
	
	collisionZone.shape = ConcavePolygonShape.new()
	collisionZone.shape.set_faces(collisionVerts)
	

func generateSegmentedLandscape(arrayOfFlatSegments, flatSegmentHeight=0.0):
	surfaceArray.resize(meshArray.ARRAY_MAX)
	
	# Code to generate a landscape here
	
	# Noise Generation
	var noise = OpenSimplexNoise.new()
	# Noise Configuration
	noise.seed = randi() # Seed
	noise.octaves = 4 # Number of regenerations, more = smoother at a performance cost
	noise.period = 20.0 # Lower period = higher noise variation
	noise.persistence = 0.8 # Amount of contribution per octave to noise generation
	
	# get_noise_3d(X, Y, Z)
	var xPoint = 0.0
	var yPoint = 0.0
	var zPoint = 0.0
	
	for row in range(arrayHeight):
		for col in range(arrayWidth):
			xPoint = col * gridSize
			zPoint = row * gridSize
			
			yPoint = noise.get_noise_2d(xPoint * noiseRoughness, zPoint * noiseRoughness) * noiseScale
			
			verts.append(Vector3(xPoint, yPoint, zPoint))
			
			uvs.append(Vector2(col * 1.0, row * 1.0))
			
			normals.append(Vector3(xPoint, yPoint, zPoint))
			
	
	var yHeight = 0.0 # Used for storing the original height of the vertice we are modifying to create the cliffs.
	var yLerpFactor = 0.0 # Used for storing the Lerp Factor to smooth the complex cliff faces.
	
	if (heightOffset >= 2 && widthOffset >= 2 && bDoCliffs == true):# If both HeightOffset and WidthOffset are set to at least 2 and "bDoCliffs" is true
		if (bDoComplexCliffs == false): # If we want simple cliffs that only exist on the border of the total map.
			for row in range(arrayHeight): # Nested Row/Col for loop
				for col in range(arrayWidth):
					# Bottom/Top Edge Case
					if (row == 0 || row == arrayHeight - 1): # If we are on the bottom/top row.
						verts[row * arrayWidth + col].y = cliffHeight
					elif (col == 0 || col == arrayWidth - 1): # Side Edge Cases
						# If we are using the edges of the base map.
						verts[row * arrayWidth + col].y = cliffHeight
						
						
					
			
		else:
			for row in range(arrayHeight): # Nested Row/Col for loop
				for col in range(arrayWidth):
					# Bottom/Top Edge Case
					if (row < (heightOffset - 1)):
						if (col < (widthOffset - 1)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < col):
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
								
							elif (col < row):
								yLerpFactor = ((float(widthOffset) - 1.0 - float(col)) / (float(widthOffset) - 1.0))
								
							else:
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1.0 - float(col)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0))
								
							
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						elif (col > (arrayWidth - widthOffset)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < (arrayWidth - col - 1.0)):
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
								
							elif((arrayWidth - col - 1.0) < row):
								yLerpFactor = ( float(widthOffset) /( float(widthOffset) - 1.0)) - ((( float(arrayWidth) + float(widthOffset) - float(col) - 1.0) / ( float(widthOffset) - 1.0)) - 1.0)
								
							else:
								yLerpFactor = (float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1 - (float(arrayWidth) - float(col) - 1.0)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0)
								
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						else:
							yHeight = verts[row * arrayWidth + col].y
							yLerpFactor = ((float(heightOffset) - 1.0 - row) / (float(heightOffset) - 1.0))
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
					elif (row > (arrayHeight - heightOffset)): # <- Need to figure out equivalent for ( widthOffset /( widthOffset - 1.0)) - ((( arrayWidth + widthOffset - col - 1.0) / ( widthOffset - 1.0)) - 1.0)
						if (col < (widthOffset - 1)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < col):
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
								
							elif (col < row):
								yLerpFactor = ((float(widthOffset) - 1.0 - float(col)) / (float(widthOffset) - 1.0))
								
							else:
								yLerpFactor = ((float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1.0 - float(col)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0))
								
							
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						elif (col > (arrayWidth - widthOffset)):
							yHeight = verts[row * arrayWidth + col].y
							if (row < (arrayWidth - col - 1.0)):
								yLerpFactor = ((float(heightOffset) - 1.0 - row) / (heightOffset - 1.0))
								
							elif((arrayWidth - col - 1.0) < row):
								yLerpFactor = ( float(widthOffset) /( float(widthOffset) - 1.0)) - ((( float(arrayWidth) + float(widthOffset) - float(col) - 1.0) / ( float(widthOffset) - 1.0)) - 1.0)
								
							else:
								yLerpFactor = (float(heightOffset) - 1.0 - float(row) + float(widthOffset) - 1 - (float(arrayWidth) - float(col) - 1.0)) / (float(heightOffset) - 1.0 + float(widthOffset) - 1.0)
								
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
						else:
							yHeight = verts[row * arrayWidth + col].y
							yLerpFactor = ((float(heightOffset) - 1.0 - float(row)) / (float(heightOffset) - 1.0))
							yPoint = lerp(yHeight, cliffHeight, yLerpFactor)
							verts[row * arrayWidth + col].y = yPoint
					
					
					
#					if (row == 0 || row == arrayHeight - 1): # If we are on the bottom/top row.
#						verts[row * arrayWidth + col].y = cliffHeight
#
#					elif (col == 0 || col == arrayWidth - 1): # Side Edge Cases
#						# If we are using the edges of the base map.
#						verts[row * arrayWidth + col].y = cliffHeight
#
#
					
			
	
	# Flattening out ground around the flat segments
	# 
	
	
	# Compiling mesh parameters
	for flatSectionArray in arrayOfFlatSegments:
		# We first need to find the section of ground in which to flatten
		var topRow = 0
		var botRow = arrayHeight
		var topCol = 0
		var botCol = arrayWidth
		
		# Let's assume that the flatSectionArray contains real-world co-ordinate data
		# Convert real-world co-ords into row and columns for flattening
		
		# Reminder: real-world position: X = gridSize * col, Z = gridSize * row
		for dataPoint in flatSectionArray:
			var xCoord = dataPoint[0]
			var zCoord = dataPoint[1]
			
			# let's assume that in all cases, we need to round to nearest gridSize
			# this will give us the row/col affected by flat ground
			
			var adjustedCol = round(xCoord / gridSize)
			var adjustedRow = round(zCoord / gridSize)
			
			print("adjCol: " + str(adjustedCol) + " adjRow: " + str(adjustedRow))
			
			if (botCol > adjustedCol):
				botCol = adjustedCol
			if (topCol < adjustedCol):
				topCol = adjustedCol
			
			if (botRow > adjustedRow):
				botRow = adjustedRow
			if (topRow < adjustedRow):
				topRow = adjustedRow
			
			print("botRow: " + str(botRow) + " topRow: " + str(topRow))
			print("botCol: " + str(botCol) + " topCol: " + str(topCol))
			
		
		# Once we have the rows and columns affected, flatten that section of the map
		var row = botRow
		var col = botCol
		while row < topRow + 1:
			while col < topCol + 1:
				verts[row * arrayWidth + col].y = flatSegmentHeight
				
				col = col + 1
			row = row + 1
			col = botCol
		
		
	
	
	
	for row in range(arrayHeight - 1):
		for col in range(arrayWidth - 1):
			indices.append(row * arrayWidth + col)
			indices.append((row + 1) * arrayWidth + col)
			indices.append(row * arrayWidth + (col + 1))
			
			indices.append(row * arrayWidth + col + 1)
			indices.append((row + 1) * arrayWidth + col)
			indices.append((row + 1) * arrayWidth + (col + 1))
			
			# Setting up collision data for the shape
			collisionVerts.append(verts[row * arrayWidth + col])
			collisionVerts.append(verts[(row + 1) * arrayWidth + col])
			collisionVerts.append(verts[row * arrayWidth + (col + 1)])
			
			collisionVerts.append(verts[row * arrayWidth + col + 1])
			collisionVerts.append(verts[(row + 1) * arrayWidth + col])
			collisionVerts.append(verts[(row + 1) * arrayWidth + (col + 1)])
			
			
	
	
	
	
	#
	
	# Assigning arrays to the mesh array for mesh instantiation
	surfaceArray[Mesh.ARRAY_VERTEX] = verts
	surfaceArray[Mesh.ARRAY_TEX_UV] = uvs
	surfaceArray[Mesh.ARRAY_NORMAL] = normals
	surfaceArray[Mesh.ARRAY_INDEX] = indices
	
	# Create a mesh from the generated mesh array
	meshArray.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, surfaceArray)
	
	self.mesh = meshArray
	self.mesh.surface_set_material(0, meshMaterial)
	
	#print("OG Collision: " + str(collisionZone.get("map_data")))
	#
	#collisionZone.shape = HeightMapShape.new()
	#collisionZone.shape.set("map_width", arrayWidth)
	#collisionZone.shape.set("map_depth", arrayHeight)
	#collisionZone.shape.set("map_data", verts)
	#
	#print("NG Collision: " + str(collisionZone.get("map_data")))
	
	collisionZone.shape = ConcavePolygonShape.new()
	collisionZone.shape.set_faces(collisionVerts)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
