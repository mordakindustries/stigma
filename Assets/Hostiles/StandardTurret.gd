extends Spatial

#targetNode
var target_node = null

#targetNode targetting transition
var current_transform : Transform
var next_transform : Transform
var next_transform_degrees
var target_pos : Vector3

var LastTargetPos : Vector3

var RotationFactorXZ = 0.0

var TargetMovementThreshold = 0.5 # How 'precise' we want the motion tracking to be

var CurrentTransformY : Transform
var NextTransformY : Transform
var TargetPosXZ : Vector3
var TargetPosY : Vector3

onready var WeaponComponent = get_node("WeaponComponent")
onready var YComponent = get_node("WeaponComponent/YComponent")

# variables for the use of target acquisition
var rotationdifference
var rotationattackthreshold = 45

var turnSpeed = 90 # degrees for now
var timetoturn : float #time stuffffff
onready var tween = get_node("TweenNode")


# Called when the node enters the scene tree for the first time.
func _ready():
	print(self.get_children())
#	print(tween.get_name())
	
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x > 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is KinematicBody:
			target_node = sceneNode.get_children()[sceneNode.get_child_count() - x]
			x = 0
			print(target_node.name)


		x -= 1
		
	
	if (target_node != null):
		LastTargetPos = target_node.transform.origin

#	#prevDir = self.get_rotation()
#	self.look_at($AimPoint.global_transform.origin, Vector3(0, 1, 0))
#	
#	#trying to get angledif stuff
#	var angleDiffZ = acos(transform.basis.z.dot(
#	transform.looking_at(target_node.get_translation(),
#	Vector3.UP).basis.z))
#	print(angleDiffZ)
#	
#	var angleDiffY = acos(transform.basis.y.dot(
#	transform.looking_at(target_node.get_translation(),
#	Vector3.UP).basis.y))
#	print(angleDiffY)
#	
#	timetoturn = atan2(angleDiffZ,angleDiffY)/deg2rad(turnSpeed)
#	print("time to turn is ", timetoturn)
#	
#	current_transform = self.global_transform
#	next_transform = current_transform.looking_at(target_node.global_transform.origin, Vector3.UP)
#	print(current_transform.basis.get_scale())
#	print(next_transform.basis.get_scale())
#	var next_transform_degrees = next_transform.basis.get_euler()
#	print ("haha:" + str(current_transform.basis.get_euler()))
#	print ("NTD: " + str(next_transform_degrees))
#	
#	print(self.scale)
#	#IT WORKS!!!!
#	tween.interpolate_property(self,"rotation",null,next_transform_degrees,1,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
#	target_pos = target_node.global_transform.origin
#	tween.start()
	
	#works with a buuuuuuuuug
#	tween.interpolate_property(self,"transform",null,next_transform,1,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
#	target_pos = target_node.global_transform.origin
#	tween.start()
#	tween.interpolate_property(self,"rotation_degrees",null,Vector3(0,40,0),1,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	#target_node = get_tree().root.get_children()[0].get_child_count()

func _physics_process(delta):
	#print(self.scale)
	if target_node != null:
		next_transform = current_transform.looking_at(target_node.global_transform.origin, Vector3.UP)
		target_pos = target_node.global_transform.origin
		var LocationDifference = 0.0
		if LastTargetPos != null:
			LocationDifference = target_pos.distance_to(LastTargetPos)
			#print("using blah")
			#print(str(target_pos) + " - " + str(LastTargetPos))
		else:
			LocationDifference = target_pos.distance_to(target_node.global_transform.basis)
		#print(str(LocationDifference))
		
		if LocationDifference > TargetMovementThreshold: #This means the target has moved
			# if target_pos != target_node.global_transform.origin
			#print("target movement threshold achieved")
			
		#if target_pos != target_node.global_transform.origin:
			#print("target has moved, recalculating trajectory")
			
			RotationFactorXZ = 0.0
			#print("1 and done?")
			#tween.interpolate_property(self,"transform",null,next_transform,1,Tween.TRANS_LINEAR,Tween.EASE_OUT_IN)
			
			#tween.start()
			
			TargetPosXZ = Vector3(target_pos.x, self.global_transform.origin.y, target_pos.z)
			TargetPosY = Vector3(0.0, target_pos.y, 0.0)
			
			
			var angleDiffZ = acos(transform.basis.z.dot(
			transform.looking_at(target_node.get_translation(),
			Vector3.UP).basis.z))
			#print(angleDiffZ)
			
			var angleDiffY = acos(transform.basis.y.dot(
			transform.looking_at(target_node.get_translation(),
			Vector3.UP).basis.y))
			#print(angleDiffY)
			
			timetoturn = atan2(angleDiffZ,angleDiffY)/deg2rad(turnSpeed)
			#print("time to turn is ", timetoturn)
			
			current_transform = WeaponComponent.global_transform
			next_transform = current_transform.looking_at(TargetPosXZ, Vector3.UP)
			#print(current_transform.basis.get_scale())
			#print(next_transform.basis.get_scale())
			next_transform_degrees = next_transform.basis.get_euler()
			#print(str(next_transform_degrees))
			#print ("haha:" + str(current_transform.basis.get_euler()))
			
			
			# Calculating for wrap-around functionality, in the event that it's quicker to wrap beyond standard rotation to acquire the target
			
			# Upper wrap-around, based on next_transform_degrees + 360 on XZ plane
			#var upper_NTD = next_transform.basis.get_euler()
			#upper_NTD.y = deg2rad(rad2deg(upper_NTD.y) + 360)
			#if (upper_NTD.y != next_transform_degrees.y):
			#	print ("NTD: " + str(next_transform_degrees))
			#	print ("Upper NTD: " + str(next_transform_degrees))
			
			#var currentAngleLooking = current_transform.basis.get_euler().y
			#var angleDifference = current_transform.basis.tdoty(next_transform_degrees)
			#var angleDifferenceUpper = current_transform.basis.tdoty(upper_NTD)
			#print ("CAL: " + str(currentAngleLooking))
			#print ("NAD: " + str(angleDifference))
			#print ("UAD: " + str(angleDifferenceUpper))
			#print ("Upper Angle Difference: " + str(angleDifferenceUpper - currentAngleLooking))
			#print ("Mid Angle Difference: " + str(angleDifference - currentAngleLooking))
			
			#if (abs(angleDifferenceUpper - currentAngleLooking) < abs(angleDifference - currentAngleLooking)):
			#	next_transform_degrees = upper_NTD
			#	#shortest rotation to face target is to rotate clockwise
			
			CurrentTransformY = YComponent.global_transform
			NextTransformY = CurrentTransformY.looking_at(TargetPosY, Vector3.UP)
			var NextTransformYDegrees = NextTransformY.basis.get_euler()
			#print("CTY: " + str(CurrentTransformY.basis.get_euler()))
			#print("NTY: " + str(NextTransformY.basis.get_euler()))
			
			# current viewing direction: 
			# new viewing direction: next_transform_degrees
			#var newRotation = Quat(WeaponComponent.get_transform().basis).slerp(next_transform.basis, 1)
			#WeaponComponent.set_transform(Transform(newRotation, WeaponComponent.get_transform().origin))
			
			#print(self.scale)
			#IT WORKS!!!!
			#tween.interpolate_property(WeaponComponent,"rotation",null,next_transform_degrees,timetoturn,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
			tween.interpolate_property(YComponent,"rotation",null,NextTransformYDegrees,timetoturn,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
			#target_pos = target_node.global_transform.origin
			tween.start()
			
			LastTargetPos = target_node.transform.origin
		
		#rotationdifference = Quat(current_transform.basis).dot(Quat(next_transform.basis))
		#print(rotationdifference)
		
#		var directiondifference = rad2deg(current_transform.basis.get_euler().angle_to(next_transform.basis.get_euler()))
#
#		var direction = current_transform.origin - target_node.transform.origin
#		rotationdifference = direction.dot(current_transform.basis.z)
#		if direction.dot(current_transform.basis.z) < rotationattackthreshold:
#			#print("Within attack threshold")
#			print(directiondifference)
#		else:
#			print("Nope")
	#if WeaponComponent.transform.basis.get_euler().y >= deg2rad(360):
	#	WeaponComponent.transform.basis.get_euler().y -= deg2rad(360)
	
	if (target_node != null):
		if RotationFactorXZ <= 1.0:
			#print("should be turning")
			RotationFactorXZ += delta
			if RotationFactorXZ > 1.0:
				RotationFactorXZ = 1.0
			
			# convert time into speed/distance factors
			#
			# distance = time * speed
			# distance is a known quantity, new_transform - current_transform
			# speed is a known quantity, turnSpeed
			# theoretical time is a known quantity, timetoturn
			#
			var newRotation = Quat(WeaponComponent.get_transform().basis).slerp(next_transform.basis, RotationFactorXZ)
			WeaponComponent.set_transform(Transform(newRotation, WeaponComponent.get_transform().origin))
			


#	if target_node != null:
#		var facingAngle = self.get_global_transform().basis.get_euler()
#
#		var rotSpeed = 90 #rotation speed in degrees per second
#		var angleDiff = acos(transform.basis.z.dot(
#		transform.looking_at(target_node.get_translation(),
#		Vector3(0,1,0)).basis.z))
#
#		var timeToTurn = angleDiff/deg2rad(rotSpeed)
#		var trans_type = Tween.TRANS_LINEAR
#		var ease_type = Tween.EASE_OUT_IN
#
#		#print(tween.get_name())
#
#		tween.interpolate_property(self,"rotation_degrees",
#        rotation_degrees,
#        Vector3(rad2deg(facingAngle.x),
#            rad2deg(facingAngle.y),
#            rad2deg(facingAngle.z)),
#        timeToTurn,
#        trans_type,
#        ease_type)
#		tween.start()




# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
