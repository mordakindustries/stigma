extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var target_node = null

var maxTurnSpeed = 5
var lockonMod = 0.0
var lockonModIncrement = 0.1

#var prevDir

# Called when the node enters the scene tree for the first time.
func _ready():
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x >= 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is KinematicBody:
			target_node = sceneNode.get_children()[sceneNode.get_child_count() - x]
			x = 0
			print(target_node.name)


		x -= 1

	#prevDir = self.get_rotation()

	#target_node = get_tree().root.get_children()[0].get_child_count()

func _physics_process(delta):
	if target_node != null:
		#var dir = (target_node.get_translation() - self.get_translation()).normalized()
		#if dir != prevDir:
		#	print (str(dir))
		#	prevDir = dir
		var curAngle = self.get_rotation()
		var angleTo = self.get_transform().looking_at(target_node.get_translation(), Vector3(0, 1, 0))
		#print(str(curAngle) + " " + str(dir))
		var dir = angleTo.basis.get_euler()
		
		self.set_rotation(self.rotation.linear_interpolate(dir, lockonMod))
		if lockonMod < 1:
			lockonMod += lockonModIncrement * delta
		else:
			print("would fire at player")
		
		
	else:
		if lockonMod > 0.0:
			lockonMod -= lockonModIncrement * delta
	
	




# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
