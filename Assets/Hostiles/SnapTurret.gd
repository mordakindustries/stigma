extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var target_node = null

var turnSpeed = 5

#var prevDir

# Called when the node enters the scene tree for the first time.
func _ready():
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x >= 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is KinematicBody:
			target_node = sceneNode.get_children()[sceneNode.get_child_count() - x]
			x = 0
			print(target_node.name)
		
		
		x -= 1
	
	#prevDir = self.get_rotation()
	self.look_at($AimPoint.global_transform.origin, Vector3(0, 1, 0))
	#target_node = get_tree().root.get_children()[0].get_child_count()

func _physics_process(delta):
	if target_node != null:
		#var dir = (target_node.get_translation() - self.get_translation()).normalized()
		
		#var dir = (target_node.get_translation()).normalized()
		#var angle = dir.dot(Vector3(0, 0, 1))
		
		#print (str(dir) + " " + str(angle))
		
		#var angleThreshold = 0.5
		
		#if angle < -angleThreshold:
		#	rotate_y(turnSpeed * delta)
		#if angle > angleThreshold:
		#	rotate_y(-turnSpeed * delta)
		
		
		var target_point = Vector3(target_node.get_translation().x, target_node.get_translation().y + 1.1, target_node.get_translation().z)
		var currentLook = self.get_global_transform().basis.get_euler()
#		var currentLookZ = currentBasis.z
#		var currentLookY = currentBasis.y
#		var currentLookX = currentBasis.x
#		var currentLook = Vector3(currentLookX, currentLookY, currentLookZ)
		#var currentLook = Vector3(self.get_global_transform().basis.tdotx(, self.get_global_transform().basis.y, self.get_global_transform().basis.z)
		
		#self.look_at(target_point, Vector3(0, 1, 0))
		
		var dir = self.get_transform().looking_at(target_point, Vector3(0, 1, 0)).basis.get_euler().normalized()
		
		#var dir = (target_node.get_translation()).normalized()
		var angle = acos(deg2rad(dir.dot(currentLook)))
		
		
		#print (str(dir) + " " + str(currentLook))
		#print (str(target_point) + " " + str(self.get_global_transform().origin))
		#print (str(angle) + " " + str(dir.dot(currentLook)))
		var angleThreshold = 0.5
		
		if angle < -angleThreshold:
			rotate_y(turnSpeed * delta)
		if angle > angleThreshold:
			rotate_y(-turnSpeed * delta)
		
		
		# --- Quaternion based rotate towards
		# replace t with get_transform()
		#var lookDir = get_node(lookTarget).get_transform().origin - t.origin 
		#var rotTransform = t.looking_at(lookDir,Vector3(0,1,0))
		#var thisRotation = Quat(t.basis).slerp(rotTransform.basis,value)
		#value += delta
		#if value>1:
		#	value = 1
		#set_transform(Transform(thisRotation,t.origin))
		
		#var angleTo = self.get_transform().looking_at(target_node.get_transform(), Vector3(0, 1, 0))
		
		#var dir = (target_node.get_translation() - self.get_translation()).normalized()
		#if dir != prevDir:
		#	print (str(dir))
		#	prevDir = dir
		#var curAngle = self.get_rotation()
		
		#print(str(curAngle) + " " + str(dir))
		
		
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
