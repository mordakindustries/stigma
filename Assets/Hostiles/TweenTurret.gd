extends Spatial

#targetNode
var target_node = null

#targetNode tranform
var current_transform : Transform
var next_transform : Transform
var target_pos : Vector3

# variables for the use of target acquisition
var rotationdifference
var rotationattackthreshold = 45

var turnSpeed = 90 # degrees for now
var timetoturn : float #time stuffffff
onready var tween = get_node("TweenNode")


# Called when the node enters the scene tree for the first time.
func _ready():
	print(self.get_children())
#	print(tween.get_name())
	
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x > 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is KinematicBody:
			target_node = sceneNode.get_children()[sceneNode.get_child_count() - x]
			x = 0
			print(target_node.name)


		x -= 1

	#prevDir = self.get_rotation()
	self.look_at($AimPoint.global_transform.origin, Vector3(0, 1, 0))
	
	#trying to get angledif stuff
	var angleDiffZ = acos(transform.basis.z.dot(
	transform.looking_at(target_node.get_translation(),
	Vector3.UP).basis.z))
	print(angleDiffZ)
	
	var angleDiffY = acos(transform.basis.y.dot(
	transform.looking_at(target_node.get_translation(),
	Vector3.UP).basis.y))
	print(angleDiffY)
	
	timetoturn = atan2(angleDiffZ,angleDiffY)/deg2rad(turnSpeed)
	print("time to turn is ", timetoturn)
	
	current_transform = self.global_transform
	next_transform = current_transform.looking_at(target_node.global_transform.origin, Vector3.UP)
	print(current_transform.basis.get_scale())
	print(next_transform.basis.get_scale())
	var next_transform_degrees = next_transform.basis.get_euler()
	print ("haha:" + str(current_transform.basis.get_euler()))
	print ("NTD: " + str(next_transform_degrees))
	
	print(self.scale)
	#IT WORKS!!!!
	tween.interpolate_property(self,"rotation",null,next_transform_degrees,1,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
	target_pos = target_node.global_transform.origin
	tween.start()
	
	#works with a buuuuuuuuug
#	tween.interpolate_property(self,"transform",null,next_transform,1,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
#	target_pos = target_node.global_transform.origin
#	tween.start()
#	tween.interpolate_property(self,"rotation_degrees",null,Vector3(0,40,0),1,Tween.TRANS_LINEAR,Tween.EASE_IN_OUT)
	#target_node = get_tree().root.get_children()[0].get_child_count()

func _physics_process(delta):
	#print(self.scale)
	if target_node != null:
		next_transform = current_transform.looking_at(target_node.global_transform.origin, Vector3.UP)
		if target_pos != target_node.global_transform.origin: #This means the target has moved
			#print("1 and done?")
			#tween.interpolate_property(self,"transform",null,next_transform,1,Tween.TRANS_LINEAR,Tween.EASE_OUT_IN)
			#target_pos = target_node.global_transform.origin
			#tween.start()
			
			
			var angleDiffZ = acos(transform.basis.z.dot(
			transform.looking_at(target_node.get_translation(),
			Vector3.UP).basis.z))
			#print(angleDiffZ)
			
			var angleDiffY = acos(transform.basis.y.dot(
			transform.looking_at(target_node.get_translation(),
			Vector3.UP).basis.y))
			#print(angleDiffY)
			
			timetoturn = atan2(angleDiffZ,angleDiffY)/deg2rad(turnSpeed)
			#print("time to turn is ", timetoturn)
			
			current_transform = self.global_transform
			next_transform = current_transform.looking_at(target_node.global_transform.origin, Vector3.UP)
			#print(current_transform.basis.get_scale())
			#print(next_transform.basis.get_scale())
			var next_transform_degrees = next_transform.basis.get_euler()
			#print ("haha:" + str(current_transform.basis.get_euler()))
			#print ("NTD: " + str(next_transform_degrees))
			
			#print(self.scale)
			#IT WORKS!!!!
			tween.interpolate_property(self,"rotation",null,next_transform_degrees,1,Tween.TRANS_SINE,Tween.EASE_OUT_IN)
			target_pos = target_node.global_transform.origin
			tween.start()
		
		#rotationdifference = Quat(current_transform.basis).dot(Quat(next_transform.basis))
		#print(rotationdifference)
		
#		var directiondifference = rad2deg(current_transform.basis.get_euler().angle_to(next_transform.basis.get_euler()))
#
#		var direction = current_transform.origin - target_node.transform.origin
#		rotationdifference = direction.dot(current_transform.basis.z)
#		if direction.dot(current_transform.basis.z) < rotationattackthreshold:
#			#print("Within attack threshold")
#			print(directiondifference)
#		else:
#			print("Nope")


#	if target_node != null:
#		var facingAngle = self.get_global_transform().basis.get_euler()
#
#		var rotSpeed = 90 #rotation speed in degrees per second
#		var angleDiff = acos(transform.basis.z.dot(
#		transform.looking_at(target_node.get_translation(),
#		Vector3(0,1,0)).basis.z))
#
#		var timeToTurn = angleDiff/deg2rad(rotSpeed)
#		var trans_type = Tween.TRANS_LINEAR
#		var ease_type = Tween.EASE_OUT_IN
#
#		#print(tween.get_name())
#
#		tween.interpolate_property(self,"rotation_degrees",
#        rotation_degrees,
#        Vector3(rad2deg(facingAngle.x),
#            rad2deg(facingAngle.y),
#            rad2deg(facingAngle.z)),
#        timeToTurn,
#        trans_type,
#        ease_type)
#		tween.start()




# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
