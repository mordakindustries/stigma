extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(float) var areaDamage = 5.0
export(float) var EXPIRY_TIME = -0.1
export(float) var directDamageMultiplier = 0.4

var radiationParticles

var radArea
var timeLeft = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	radiationParticles = $Particles
	radArea = $RadArea
	#print("radiating")
	

func _physics_process(delta):
	var bodies = radArea.get_overlapping_bodies()
	for body in bodies:
		#print(body.get_name())
		if body.has_method("applyRadiation"):
			#print("radiating body")
			body.applyRadiation(delta * areaDamage)
		elif body.has_method("hitDetect"):
			body.hitDetect(delta * areaDamage * directDamageMultiplier, body.global_transform.looking_at(global_transform.origin, Vector3(0, 1, 0)))
			
	
	if EXPIRY_TIME >= 0.0:
		timeLeft += delta
		if timeLeft >= EXPIRY_TIME:
			queue_free()
	
	
	
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
