extends RigidBody

const BASE_BULLET_BOOST = 0.04





# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func hitDetect(damage, impactGlobalTrans, originPoint=Vector3(0.0, 0.0, 0.0)):
	var directionVector = impactGlobalTrans.basis.z.normalized() * BASE_BULLET_BOOST
	
	apply_impulse((impactGlobalTrans.origin - global_transform.origin).normalized(), directionVector * damage)
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
