extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var sceneRoot

var buildingRoot

var mapRoomList
var rooms = [preload("res://Rooms/Room2.tscn")]
var spawnRoom = preload("res://Rooms/SpawnRoom1.tscn")
var roomNumber = 0
var playerStartingRoom
var roomsLeft = 0

onready var landscapeNode = $Landscape/LandscapeMesh
var multiArrayOfFlatSegments
var arrayOfFlatSegments

export(int) var mapX = 5
export(int) var mapZ = 5
export(int) var genSeed = 14071995

var rng

var roomLiteralSize = 40

var roomXLeft = mapX
var roomZLeft = mapZ

var curRoomCount = 0
export(int) var numRooms = 12

var genStage = 0

var roomX = 0
var roomZ = 0

var playerStartX = 0
var playerStartZ = 0

# Ammunition sources
var pistolAmmo = [preload("res://Assets/PickupPistolClip.tscn"), preload("res://Assets/PickupPistol.tscn")]
var buckshotAmmo = [preload("res://Assets/PickupBuckshot.tscn"), preload("res://Assets/PickupShotgun.tscn")]
var hornetAmmo = [preload("res://Assets/PickupHornetClip.tscn"), preload("res://Assets/PickupHornet.tscn")]
var launcherAmmo = [preload("res://Assets/PickupLauncher.tscn"), preload("res://Assets/PickupLauncherGrenade.tscn")]

# Equipment sources
var healthSources = [preload("res://Assets/InstantMedkit.tscn")]
var grenadeSources = [preload("res://Assets/GrenadePack.tscn"), preload("res://Assets/GlowstickPack.tscn")]

# Enemy sources
var turretHostiles = [preload("res://Assets/Hostiles/TweenTurret.tscn")]
var moderateHostiles = [preload("res://Assets/Hostiles/GenericHumanoidEnemy.tscn")]

# Tilemap reference
var WFFTileMap
var tiles = ["P", "S", "A", "H", "G", "T"]

# Called when the node enters the scene tree for the first time.
func _ready():
	sceneRoot = get_tree().root.get_children()[0]
	
	buildingRoot = Spatial.new()
	buildingRoot.name = "Building"
	sceneRoot.add_child(buildingRoot)
	# Here we move the building to a theoretically correct position within the map
	#buildingRoot.translate(Vector3(-roomLiteralSize * float(mapX) / 2.0, 0.0, -roomLiteralSize * float(mapZ) / 2.0))
	#landscapeNode.translate(Vector3(-roomLiteralSize * float(mapX), 0.0, -roomLiteralSize * float(mapZ)))
	#buildingRoot.translate(Vector3(roomLiteralSize * float(mapX), 0.0, roomLiteralSize * float(mapZ)))
	
	print("X: " + str(landscapeNode.get_parent().transform.origin.x) + " MX: " + str(roomLiteralSize * float(mapX)))
	
	rng = RandomNumberGenerator.new()
	rng.seed = genSeed
	
	# Setting up a 2D Array for roomList
	mapRoomList = []
	for x in range(mapX):
		mapRoomList.append([])
		mapRoomList[x] = []
		for z in range(mapZ):
			mapRoomList[x].append([])
			mapRoomList[x][z] = null
			# Blank room
	
	# Temporary interception of tilemap
	WFFTileMap = []
	for x in range(mapX):
		WFFTileMap.append([])
		WFFTileMap[x] = []
		for z in range(mapZ):
			WFFTileMap[x].append([])
			var tileNumber = rng.randi_range(0, tiles.size() - 1)
			var tileSelect = tiles[tileNumber]
			
			WFFTileMap[x][z] = tileSelect
			# Blank room
	
	multiArrayOfFlatSegments = []
	arrayOfFlatSegments = []
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
# Generation of the level
# Step one (0): generate the level floor (genStage 0 - 1)
# Step two (1): instance rooms (genStage 2)
# Step three (2): generate/utilise Waveform Function Collapse to generate items
# Step four (3) : calculate segments for landscape flattening
# Step five (4) : generate a segmented landscape using the building position as a basis
# Step six (5) : spawn in the player

# For reference:
# Room label - room type
# "0" - non room
# "P" - player starting point
# "R" - generic room
func _process(delta):
	if genStage == 0:
		roomX = rng.randi_range(0, mapX - 1)
		roomZ = rng.randi_range(0, mapZ - 1)
		playerStartingRoom = spawnRoom.instance()
		buildingRoot.add_child(playerStartingRoom)
		playerStartingRoom.global_translate(Vector3(roomLiteralSize * roomX, 0, roomLiteralSize * roomZ))
		mapRoomList[roomX][roomZ] = playerStartingRoom
		print("Co-ordinates for spawn: " + str(roomX) + " " + str(roomZ) + " Actual Transform: " + str(playerStartingRoom.global_transform.origin.x) + " " + str(playerStartingRoom.global_transform.origin.z))
		genStage = 1
		curRoomCount += 1
		playerStartX = roomX
		playerStartZ = roomZ
		
		# each time a room is created, add the room global co-ordinates into the multiarray
		var newCoordinateArray = []
		newCoordinateArray.append(playerStartingRoom.global_transform.origin.x+300-roomLiteralSize)
		newCoordinateArray.append(playerStartingRoom.global_transform.origin.z-roomLiteralSize)
		arrayOfFlatSegments.append(newCoordinateArray)
		newCoordinateArray = []
		newCoordinateArray.append(playerStartingRoom.global_transform.origin.x+300+roomLiteralSize)
		newCoordinateArray.append(playerStartingRoom.global_transform.origin.z+roomLiteralSize)
		arrayOfFlatSegments.append(newCoordinateArray)
		
	elif genStage == 1:
		if curRoomCount < numRooms:
			roomX = rng.randi_range(0, mapX - 1)
			roomZ = rng.randi_range(0, mapZ - 1)
			if isAdjacentToRoom(roomX, roomZ, mapX, mapZ, mapRoomList):
				var newRoom = rooms[0].instance()
				buildingRoot.add_child(newRoom)
				newRoom.global_translate(Vector3(roomLiteralSize * roomX, 0, roomLiteralSize * roomZ))
				mapRoomList[roomX][roomZ] = newRoom
				print("Co-ordinates for room" + str(curRoomCount) + ": " + str(roomX) + " " + str(roomZ) + " Actual Transform: " + str(newRoom.global_transform.origin.x) + " " + str(newRoom.global_transform.origin.z))
				curRoomCount += 1
				
				# each time a room is created, add the room global co-ordinates into the multiarray
				var newCoordinateArray = []
				newCoordinateArray.append(newRoom.global_transform.origin.x+300-roomLiteralSize)
				newCoordinateArray.append(newRoom.global_transform.origin.z-roomLiteralSize)
				arrayOfFlatSegments.append(newCoordinateArray)
				newCoordinateArray = []
				newCoordinateArray.append(newRoom.global_transform.origin.x+300+roomLiteralSize)
				newCoordinateArray.append(newRoom.global_transform.origin.z+roomLiteralSize)
				arrayOfFlatSegments.append(newCoordinateArray)
				
		else:
			roomX = 0
			roomZ = 0
			genStage = 2
	elif genStage == 2:
		if roomX < mapX:
			if roomZ < mapZ:
				if mapRoomList[roomX][roomZ] != null:
					var itemList
					itemList = []
					
					if WFFTileMap[roomX][roomZ] == tiles[0]:
						print(str(tiles[0]))
						#mapRoomList[roomX][roomZ].spawn_items(pistolAmmo)
						itemList += pistolAmmo
					if WFFTileMap[roomX][roomZ] == tiles[1]:
						print(str(tiles[1]))
						#mapRoomList[roomX][roomZ].spawn_items(buckshotAmmo)
						itemList += buckshotAmmo
					if WFFTileMap[roomX][roomZ] == tiles[2]:
						print(str(tiles[2]))
						#mapRoomList[roomX][roomZ].spawn_items(hornetAmmo)
						itemList += hornetAmmo
					if WFFTileMap[roomX][roomZ] == tiles[3]:
						print(str(tiles[3]))
						#mapRoomList[roomX][roomZ].spawn_items(healthSources)
						itemList += healthSources
					if WFFTileMap[roomX][roomZ] == tiles[4]:
						print(str(tiles[4]))
						#mapRoomList[roomX][roomZ].spawn_items(launcherAmmo)
						itemList += launcherAmmo
					
					if (playerStartX != roomX || playerStartZ != roomZ):
						itemList += moderateHostiles
					else:
						itemList += healthSources
					
					print(str(itemList))
					mapRoomList[roomX][roomZ].spawn_items(itemList)
					
					print("!")
				roomZ += 1
			
			if roomZ >= mapZ:
				roomZ = 0
				roomX += 1
		
		if roomX >= mapX:
			genStage = 3
			roomX = 0
			roomZ = 0
	elif genStage == 3:
		#buildingRoot.translate(Vector3(-roomLiteralSize * float(mapX), 0.0, -roomLiteralSize * float(mapZ)))
		multiArrayOfFlatSegments.append(arrayOfFlatSegments)
		arrayOfFlatSegments = []
		
		genStage = 4
	elif genStage == 4:
		if (landscapeNode != null):
			if (landscapeNode.has_method("generateLandscape")):
				landscapeNode.generateSegmentedLandscape(multiArrayOfFlatSegments, -0.99)
				# For debug purposes, can replace multiArray with [[[560, 560], [200, 200]]]
			
		genStage = 5
	elif genStage == 5:
		genStage = 6
	elif genStage == 6:
		playerStartingRoom.spawn_player()
		
		genStage = 99

func isAdjacentToRoom(xCoOrd, zCoOrd, xRef, zRef, mapRef):
	# Directions to test for adjacency with
	var testUp = true
	var testDown = true
	var testLeft = true
	var testRight = true
	
	if xCoOrd == xRef - 1:
		testUp = false
	if xCoOrd == 0:
		testDown = false
	if zCoOrd == zRef - 1:
		testRight = false
	if zCoOrd == 0:
		testLeft = false
	
	if testUp == true:
		if mapRef[xCoOrd + 1][zCoOrd] != null:
			return true
	
	if testDown == true:
		if mapRef[xCoOrd - 1][zCoOrd] != null:
			return true
	
	if testRight == true:
		if mapRef[xCoOrd][zCoOrd + 1] != null:
			return true
	
	if testLeft == true:
		if mapRef[xCoOrd][zCoOrd - 1] != null:
			return true
	
	return false
	
