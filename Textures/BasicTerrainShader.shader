shader_type spatial;

uniform float maxGrassHeight = 2.0;
uniform float minRockHeight = 18.0;
uniform float slopeFactor = 8.0;

uniform sampler2D grassTexture;
uniform vec2 grassScale;

uniform sampler2D dirtTexture;
uniform vec2 dirtScale;

uniform sampler2D rockTexture;
uniform vec2 rockScale;

varying float heightValue;
varying vec3 normal;

void vertex(){
	heightValue = VERTEX.y;
	normal = NORMAL;
}

float getSlopeOfTerrain(float heightNormal){
	float slope = 1.0 - heightNormal;
	slope *= slope;
	return (slope * slopeFactor);
}

float getTextureMix(float currentHeight){
	if (currentHeight > minRockHeight) {
		return 1.0;
	}
	
	if (currentHeight < maxGrassHeight){
		return 0.0;
	}
	
	float heightMix = (currentHeight - maxGrassHeight) / (minRockHeight - currentHeight);
	return heightMix;
}

void fragment() {
	vec3 dirt = vec3(texture(dirtTexture, UV*dirtScale).rgb) * 0.35;
	vec3 grass = vec3(texture(grassTexture, UV*grassScale).rgb) * 0.5;
	vec3 rock = vec3(texture(rockTexture, UV*rockScale).rgb) * 1.0;
	
	float slope = clamp(getSlopeOfTerrain(normal.y), 0.0, 1.0);
	float terrainMix = clamp(getTextureMix(heightValue), 0.0, 1.0);
	
	vec3 grassMix = mix(grass, dirt, slope);
	vec3 dirtMix = mix(dirt, rock, terrainMix/(slope + 0.1));
	vec3 rockMix = mix(rock, dirtMix, slope);
	vec3 overallMix = mix(grassMix, rockMix, terrainMix);
	
	
	ALBEDO = overallMix;
}