extends Spatial

var BULLET_SPEED = 12
var BULLET_DAMAGE = 40

const KILL_TIMER = 4
var timer = 0

var hitDetected = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$Area.connect("body_entered", self, "collided")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var forDir = global_transform.basis.z.normalized()
	global_translate(forDir * BULLET_SPEED * delta)
	
	timer += delta
	if timer > KILL_TIMER:
		queue_free()
	

func collided(body):
	if hitDetected == false:
		if body.has_method("hitDetect"):
			body.hitDetect(BULLET_DAMAGE, global_transform)
	
	print(body.name)
	
	hitDetected = true
	queue_free()
