extends Spatial

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const BULLET_VELOCITY = 45
const DAMAGE = 0
const SPREAD = 0
const PROJECTILE_COUNT = 1

const AMMO_TYPE = "40mm HE"
const AMMO_COST = 1

var isWeaponEnabled = false

var projectile = preload("res://Assets/BounceGrenade.tscn")

var isFiring = false
var FIRE_RATE = 1.2
var curFireTimer = 0

var playerNode = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func fireWeapon():
	isFiring = true
	#createProjectile()

func createProjectile():
	var clone
	var sceneRoot = get_tree().root.get_children()[0]
	
	#var i = 0
	
	#while i < PROJECTILE_COUNT:
	clone = projectile.instance()
	sceneRoot.add_child(clone)
	
	var randomSpread = Vector3(rand_range(-SPREAD, SPREAD), rand_range(-SPREAD, SPREAD), 0)
	
	$SpreadPoint.rotation_degrees = randomSpread
	clone.global_transform = $SpreadPoint.global_transform
	clone.scale = Vector3(1, 1, 1)
	#clone.rotate_y(-145)
	#clone.BULLET_DAMAGE = DAMAGE
	#clone.BULLET_SPEED = BULLET_VELOCITY
	
	if (playerNode != null):
		clone.originSource = playerNode
	
	
	if clone.has_method("apply_impulse"):
		
		clone.apply_impulse(Vector3(0, 0, 0), global_transform.basis.z.normalized() * BULLET_VELOCITY)
	
	#i += 1
	#$SpreadPoint.rotation_degrees = Vector3(0, 0, 0)
	

func equipWeapon():
	if isWeaponEnabled == false:
		isWeaponEnabled = true
		return true
	else:
		return false

func unequipWeapon():
	if isWeaponEnabled == true:
		isWeaponEnabled = false
		return true
	else:
		return false

func _physics_process(delta):
	if isWeaponEnabled:
		if curFireTimer > 0:
			curFireTimer -= delta
		else:
			if isFiring:
				if playerNode.hasAmmo(AMMO_TYPE, AMMO_COST):
					playerNode.costAmmo(AMMO_TYPE, AMMO_COST)
					createProjectile()
					curFireTimer = FIRE_RATE
				

func stopFiring():
	#curFireTimer = 0
	isFiring = false

