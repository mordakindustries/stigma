extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var levelGenerationScene = preload("res://Scenes/LevelGenerationScene.tscn")

onready var sceneRoot = get_tree().root.get_children()[0]

var levelGenerator

# original seeds: [104, 512, 1672, -907, -4]
# different seeds for seed test: [42, 760, 51, -123, 90, 92]
var seeds = [104, 512, 1672, -907, -4]
var levelCount = 0
var targetLevelCount = 4
var surrogateLevels = [2]

var levelCycleStep = 0
var levelCooldown = 0
var levelCooldownMax = 10

var HeuristicsEngineObject

var pointScorePistol = 0.0
var pointScoreHornet = 0.0
var pointScoreShotgun = 0.0
var pointScoreLauncher = 0.0

var pointScoreLight = 0.0
var pointScoreModerate = 0.0
var pointScoreHeavy = 0.0

var bUseRangeHeuristics = false
var surrogateWeaponRangeHornet = [15.0, 35.0]
var surrogateWeaponRangeShotgun = [0.0, 15.0]
var surrogateWeaponRangeLauncher = [75.0, 120.0]

# Called when the node enters the scene tree for the first time.
func _ready():
	
	# Heuristics Engine finding
	var sceneNode = get_tree().root.get_children()[0]
	var x = sceneNode.get_child_count()
	while x > 0:
		if sceneNode.get_children()[sceneNode.get_child_count() - x] is Spatial:
			if (sceneNode.get_children()[sceneNode.get_child_count() - x].has_method("GetHeuristic")):
				HeuristicsEngineObject = sceneNode.get_children()[sceneNode.get_child_count() - x]
				x = 0
				print(HeuristicsEngineObject.name)
		x -= 1
	
	#pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if (levelCycleStep == 2):
		beginLevelGeneration()
		levelCycleStep += 1
	
	if (levelCycleStep == 1):
		if (levelGenerator):
			if is_instance_valid(levelGenerator):
				levelCycleStep += 1
				if (levelGenerator.has_method("changePlayerSurrogateUsage")):
					if (levelCount in surrogateLevels):
						levelGenerator.changePlayerSurrogateUsage(true)
					else:
						levelGenerator.changePlayerSurrogateUsage(false)
					
				
				if (pointScorePistol != 0.0):
					if(levelGenerator.has_method("setPointScores")):
						levelGenerator.setPointScores(pointScorePistol, pointScoreHornet, pointScoreShotgun, pointScoreLauncher, pointScoreLight, pointScoreModerate, pointScoreHeavy)
						
				
		
	
	
	if (levelCycleStep == 0):
		instantiateLevelGeneration()
		if (levelCount >= 1):
			feedHeuristicsIntoLevel()
			print("Heuristics fed.")
		
		levelCycleStep += 1
	
	if (levelCycleStep == -1):
		levelCooldown += delta
		if (levelCooldown >= levelCooldownMax):
			levelCooldown = 0
			levelCycleStep = 0
			incorporateHeuristics(levelCount - 1)
			
		
		
		
	
	#pass

func instantiateLevelGeneration():
	levelGenerator = levelGenerationScene.instance()
	sceneRoot.add_child(levelGenerator)
	
	levelGenerator.global_translate(Vector3(0.0, 0.0, 0.0))
	levelGenerator.scale = Vector3(1.0, 1.0, 1.0)
	
	levelGenerator.setMainObject(self)
	levelGenerator.levelNumber = levelCount
	
	#pass

func beginLevelGeneration():
	if (levelGenerator):
		levelGenerator.setSeed(seeds[levelCount])
		levelGenerator.beginGenerationFromScratch()
	#pass

func exitLevel():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	if (HeuristicsEngineObject):
		if (HeuristicsEngineObject.has_method("publishHeuristicInfo")):
			HeuristicsEngineObject.publishHeuristicInfo(levelCount)
			print("File should be saved.")
		
		if (HeuristicsEngineObject.has_method("endCurrentLevelHeuristics")):
			HeuristicsEngineObject.endCurrentLevelHeuristics()
		
		
		
		
	
	# On level exit, clear generation
	# Update the level count
	# Set up the generation of the next level
	# Incorporate heuristics in some manner to the next generation
	levelCount += 1
	if levelCount < targetLevelCount:
		levelCycleStep = -1
		
		
		
		levelGenerator.queue_free()
	
	#pass

func incorporateHeuristicsForSpecificLevel(levelNo = 0):
	if (HeuristicsEngineObject):
		if (HeuristicsEngineObject.has_method("getPointValueForHeuristic")):
			pointScorePistol = HeuristicsEngineObject.getPointValueForHeuristic("PISTOL", levelNo)
			pointScoreHornet = HeuristicsEngineObject.getPointValueForHeuristic("HORNET", levelNo)
			pointScoreShotgun = HeuristicsEngineObject.getPointValueForHeuristic("SHOTGUN", levelNo)
			pointScoreLauncher = HeuristicsEngineObject.getPointValueForHeuristic("LAUNCHER", levelNo)
			
			pointScoreLight = HeuristicsEngineObject.getPointValueForHeuristic("light", levelNo)
			pointScoreModerate = HeuristicsEngineObject.getPointValueForHeuristic("moderate", levelNo)
			pointScoreHeavy = HeuristicsEngineObject.getPointValueForHeuristic("heavy", levelNo)
			
			print("psP: " + str(pointScorePistol) + "\npsH: " + str(pointScoreHornet) + "\npsS: " + str(pointScoreShotgun) + "\nnpL: " + str(pointScoreLauncher))
			
	
	
	#pass

func incorporateHeuristics(levelNo = 0):
	if (HeuristicsEngineObject):
		if (HeuristicsEngineObject.has_method("getPointValueForHeuristic")):
			pointScorePistol = 0.0
			pointScoreHornet = 0.0
			pointScoreShotgun = 0.0
			pointScoreLauncher = 0.0
			
			pointScoreLight = 0.0
			pointScoreModerate = 0.0
			pointScoreHeavy = 0.0
			
			var localLevelCount = levelNo
			while (localLevelCount >= 0):
				pointScorePistol = HeuristicsEngineObject.getPointValueForHeuristic("PISTOL", localLevelCount)
				pointScoreHornet = HeuristicsEngineObject.getPointValueForHeuristic("HORNET", localLevelCount)
				pointScoreShotgun = HeuristicsEngineObject.getPointValueForHeuristic("SHOTGUN", localLevelCount)
				pointScoreLauncher = HeuristicsEngineObject.getPointValueForHeuristic("LAUNCHER", localLevelCount)
				
				pointScoreLight = HeuristicsEngineObject.getPointValueForHeuristic("light", localLevelCount)
				pointScoreModerate = HeuristicsEngineObject.getPointValueForHeuristic("moderate", localLevelCount)
				pointScoreHeavy = HeuristicsEngineObject.getPointValueForHeuristic("heavy", localLevelCount)
				
				
				localLevelCount -= 1
			
			
			#pointScorePistol = HeuristicsEngineObject.getPointValueForHeuristic("PISTOL", levelNo)
			#pointScoreHornet = HeuristicsEngineObject.getPointValueForHeuristic("HORNET", levelNo)
			#pointScoreShotgun = HeuristicsEngineObject.getPointValueForHeuristic("SHOTGUN", levelNo)
			#pointScoreLauncher = HeuristicsEngineObject.getPointValueForHeuristic("LAUNCHER", levelNo)
			
			#pointScoreLight = HeuristicsEngineObject.getPointValueForHeuristic("light", levelNo)
			#pointScoreModerate = HeuristicsEngineObject.getPointValueForHeuristic("moderate", levelNo)
			#pointScoreHeavy = HeuristicsEngineObject.getPointValueForHeuristic("heavy", levelNo)
			
			print("AH;\npsP: " + str(pointScorePistol) + "\npsH: " + str(pointScoreHornet) + "\npsS: " + str(pointScoreShotgun) + "\nnpL: " + str(pointScoreLauncher))
			
		if (HeuristicsEngineObject.has_method("getRangeHeuristicForLevel")):
			surrogateWeaponRangeHornet = HeuristicsEngineObject.getRangeHeuristicForLevel("HORNET", levelNo)
			surrogateWeaponRangeShotgun = HeuristicsEngineObject.getRangeHeuristicForLevel("SHOTGUN", levelNo)
			surrogateWeaponRangeLauncher = HeuristicsEngineObject.getRangeHeuristicForLevel("LAUNCHER", levelNo)
			
			print("swr;\nswrH: " + str(surrogateWeaponRangeHornet) + "\nswrS: " + str(surrogateWeaponRangeShotgun) + "\nswrL: " + str(surrogateWeaponRangeLauncher))
			
	
	#pass

func feedHeuristicsIntoLevel():
	if (levelGenerator):
		levelGenerator.setPointScores(pointScorePistol, pointScoreHornet, pointScoreShotgun, pointScoreLauncher, pointScoreLight, pointScoreModerate, pointScoreHeavy)
		
	pass

func getWeaponRange(weaponName="HORNET"):
	if (bUseRangeHeuristics):
		var returnArray = [0.0, 150.0]
		if (weaponName == "HORNET"):
			return surrogateWeaponRangeHornet
		elif (weaponName == "LAUNCHER"):
			return surrogateWeaponRangeLauncher
		elif (weaponName == "SHOTGUN"):
			return surrogateWeaponRangeShotgun
		
		return returnArray
	else:
		return null
	
	#pass
