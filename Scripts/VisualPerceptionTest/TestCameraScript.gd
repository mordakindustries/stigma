extends Camera


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var possibleObjects = []
var seenObjects = []

var knownPickups = []
var knownPickupLocations = []
var knownPickupCertainty = []

var knownHostiles = []

var recheckTime = 0.0
var recheckTimer = 3.0

var teamDesignation = "AI"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func checkActualVisibility(otherObject):
	#print("Visible?")
	
	# Add in logic to potentially detect visibility of the object
	var spaceState = get_world().direct_space_state
	if (otherObject):
		var result = spaceState.intersect_ray(self.global_transform.origin, otherObject.global_transform.origin, [self])
		if (result.collider == otherObject):
			#print("I see " + result.collider.name)
			return true
		else:
			#print("I do not see")
			return false
	else:
		print("False Alarm")
		return false
	
	

func addObjectToCheck(otherObject):
	possibleObjects.append(otherObject)
	

func removeObjectToCheck(otherObject):
	if (possibleObjects.has(otherObject)):
		possibleObjects.remove(possibleObjects.find(otherObject))
		print("Bye possible object.")
	
	if (seenObjects.has(otherObject)):
		seenObjects.remove(seenObjects.find(otherObject))
		# could double-check if the object is a known pickup/enemy
		print("Bye seen object.")
	

func _physics_process(delta):
	process_vision(delta)
	if (recheckTime >= recheckTimer):
		reprocess_vision(delta)
		recheckTime = recheckTime - recheckTimer
	else:
		recheckTime = recheckTime + delta
	
	

func process_vision(delta):
	if possibleObjects.size() >= 1:
		var curCount = 0
		var maxCount = possibleObjects.size()
		var testItem
		
		while curCount < maxCount:
			testItem = possibleObjects[curCount]
			if (checkActualVisibility(testItem)):
				if (!seenObjects.has(testItem)):
					seenObjects.append(testItem)
					interpretObject(testItem)
				
				possibleObjects.remove(curCount)
				maxCount = maxCount - 1
				
			else:
				curCount = curCount + 1
				
			
			
		
	
	
	

func reprocess_vision(delta):
	if seenObjects.size() >= 1:
		var curCount = 0
		var maxCount = seenObjects.size()
		var testItem
		
		while curCount < maxCount:
			testItem = seenObjects[curCount]
			if (!checkActualVisibility(testItem)):
				print("No longer see " + seenObjects[curCount].name)
				if (!possibleObjects.has(testItem)):
					possibleObjects.append(testItem)
				
				seenObjects.remove(curCount)
				maxCount = maxCount - 1
				
			else:
				curCount = curCount + 1
				
			
			
		
	
	

func interpretObject(otherObject):
	if (otherObject):
		if (otherObject.has_method("identifyPickup")):
			if (knownPickups.find(otherObject) == -1): # If the pickup is unknown
				knownPickups.append(otherObject)
				knownPickupLocations.append(otherObject.global_transform.origin)
				knownPickupCertainty.append(0.99)
		elif (otherObject.has_method("getTeam")):
			if (otherObject.getTeam() != self.teamName):
				knownHostiles.append(otherObject)
		else:
			print("Unable to ID otherObject")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
