extends VisibilityNotifier


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var ParentObject

# Called when the node enters the scene tree for the first time.
func _ready():
	print(get_parent().name)
	ParentObject = get_parent()
	connect("camera_entered", self, "_on_camera_entered")
	connect("camera_exited", self, "_on_camera_exited")
	

func _on_camera_entered(cameraObject):
	if cameraObject.has_method("checkActualVisibility"):
		cameraObject.checkActualVisibility(ParentObject)
	
	if cameraObject.has_method("addObjectToCheck"):
		cameraObject.addObjectToCheck(ParentObject)
	
	

func _on_camera_exited(cameraObject):
	if cameraObject.has_method("removeObjectToCheck"):
		cameraObject.removeObjectToCheck(ParentObject)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
