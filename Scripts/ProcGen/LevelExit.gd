extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var mainGameObject
var objectToDetect

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func exitLevel():
	if (mainGameObject):
		if(mainGameObject.has_method("exitLevel")):
			mainGameObject.exitLevel()
			
	
	
	#pass

func setObjectToDetect(newObject):
	objectToDetect = newObject

func setMainGameObject(newMain):
	mainGameObject = newMain

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_DetectionArea_body_entered(body):
	print("blah blah blah")
	if (objectToDetect):
		if (str(body) == str(objectToDetect)):
			exitLevel()
		else:
			print(str(body))
	else:
		print("unexpected result: " + str(body))
	
	print("DtBd: " + str(body) + ", obTD: " + str(objectToDetect))
	
	pass # Replace with function body.
