extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Scene management
var sceneRoot

onready var landscapeNode = $LandscapeGen
onready var navManager = $NavigationManager
onready var navMeshA = $NavigationManager/NavMeshA

var buildingList

var validBuildingGenGridSegments

var rng
export(int) var rngSeed = 104
export(bool) var bUseHeuristicPointScore = false

# Types of generate-able buildings
var buildingTypes = ["ARMORY", "MEDICAL", "BARRACKS"]

var armoryRooms = [preload("res://Rooms/OffsetRoomArmoryA.tscn"), preload("res://Rooms/OffsetRoomArmoryB.tscn")]
var medicalRooms = [preload("res://Rooms/OffsetRoomA.tscn")]
var barracksRooms = [preload("res://Rooms/OffsetRoomA.tscn")]

# Types of loot/hostiles
# Ammunition sources
var pistolAmmo = [preload("res://Assets/PickupPistolClip.tscn"), preload("res://Assets/PickupPistol.tscn")]
var buckshotAmmo = [preload("res://Assets/PickupBuckshot.tscn"), preload("res://Assets/PickupShotgun.tscn")]
var hornetAmmo = [preload("res://Assets/PickupHornetClip.tscn"), preload("res://Assets/PickupHornet.tscn")]
var launcherAmmo = [preload("res://Assets/PickupLauncher.tscn"), preload("res://Assets/PickupLauncherGrenade.tscn")]

# Equipment sources
var healthSources = [preload("res://Assets/InstantMedkit.tscn")]
var grenadeSources = [preload("res://Assets/GlowstickPack.tscn")]

# Enemy sources
var turretHostiles = [preload("res://Assets/Hostiles/TweenTurret.tscn")]
#var moderateHostiles = [preload("res://Assets/Hostiles/GenericHumanoidEnemy.tscn")]
var lightHostiles = [preload("res://Assets/Hostiles/GenericHumanoidEnemyLight.tscn")]
var moderateHostiles = [preload("res://Assets/Hostiles/GenericHumanoidEnemyHornet.tscn")]
var heavyHostiles = [preload("res://Assets/Hostiles/GenericHumanoidEnemyRedeemer.tscn")]

# Room Creation/Management
var flatLandSegments

var flatLandBuildingTileMaps = []

var validMapTiles = ["a1", "m1", "b1"]
var allMapTiles = ["emptyTile", "a1", "m1", "b1"]

var armoryRoomList = []
var medicalRoomList = []
var barracksRoomList = []

# Misc
var mainObject

var levelBarriers = []
var levelBarrierTemplate = preload("res://Assets/PCG/LevelBarrier.tscn")

var levelStart
var levelStartTemplate = preload("res://Assets/PCG/LevelStart.tscn")

var levelExit
var levelExitTemplate = preload("res://Assets/PCG/LevelExit.tscn")

# player/playersurrogate data

var player
var playerTemplate = preload("res://Assets/Player.tscn")

var playerSurrogateTemplate = preload("res://Assets/AI/PlayerSurrogateAgent.tscn")
var bUseSurrogate = true

# Generation Management
export(int) var genStage = 0
var maxGenStage = 20

export(int) var numHostilesToSpawn = 30
export(int) var numItemsToSpawn = 300

# Point scores for loot/hostile generation influence
var pointScorePistol = 0.0
var pointScoreHornet = 0.0
var pointScoreShotgun = 0.0
var pointScoreLauncher = 0.0

var pointScoreLight = 0.0
var pointScoreModerate = 0.0
var pointScoreHeavy = 0.0

export(float) var initialScorePistol = 1.0
export(float) var initialScoreHornet = 1.0
export(float) var initialScoreShotgun = 1.0
export(float) var initialScoreLauncher = 1.0

export(float) var initialScoreLight = 3.0
export(float) var initialScoreModerate = 8.0
export(float) var initialScoreHeavy = 1.0

export(int) var levelNumber = 0

# Generate Landscape
# Generate Building
# Generate level borders
# Distribute pickups
# Generate level start/exit
# Instantiate hostiles
# Launch player into the zone


# Called when the node enters the scene tree for the first time.
func _ready():
	if (genStage == 0):
		beginGenerationFromScratch()
	else:
		genStage = maxGenStage + 1
	#establishSceneRoot()
	#beginGeneration()
	
	#pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Generate Landscape
	# Generate Building
	# Generate level borders
	# Generate NavMesh
	# Distribute pickups
	# Generate level start/exit
	# Instantiate hostiles
	# Launch player into the zone
	if genStage < maxGenStage:
		if genStage == 0:
			# Generate Landscape
			if (landscapeNode):
				landscapeNode.generateBasicLandscapeWithCentralFlatArea()
			print("genStage: " + str(genStage))
		if genStage == 1:
			# Generate Building - Extrapolate Flat Areas
			calculateFlatLandSegments()
			print("genStage: " + str(genStage))
		if genStage == 2:
			# Generate Building - Generate Tile Data Template
			generateBasicFlatLandTileMaps()
			print("genStage: " + str(genStage))
		if genStage == 3:
			# Generate Building - Generate Tile Data for buildings
			generateBuildingMetaData()
			print("genStage: " + str(genStage))
		if genStage == 4:
			# Generate Building - Instantiate Buildings
			if (!bUseSurrogate):
				instantiateBuildings()
			print("genStage: " + str(genStage))
		if genStage == 5:
			# Generate Level Barriers
			generateLevelBarriers()
			print("genStage: " + str(genStage))
		if genStage == 6:
			# Generate Navigation Mesh
			bakeNavigationMesh()
			print("genStage: " + str(genStage))
		if genStage == 7:
			# Distribute pickups - Room-based loot generation
			instantiateLootInRooms()
			print("genStage: " + str(genStage))
		if genStage == 8:
			# Distribute pickups - Distributing items over the map
			distributeItemsOverMap()
			print("genStage: " + str(genStage))
		if genStage == 9:
			# Instantiate Hostiles - Hostile Distribution over map
			distributeHostilesOverMap()
			print("genStage: " + str(genStage))
		if genStage == 10:
			# Generate level start/exit - Instantiating Start
			instantiateLevelStart()
			print("genStage: " + str(genStage))
		if genStage == 11:
			# Generate level start/exit - Instantiating Exit
			instantiateLevelExit()
			print("genStage: " + str(genStage))
		if genStage == 12:
			# Launch player into the zone
			if (bUseSurrogate):
				spawnPlayerSurrogate()
			else:
				spawnPlayer()
			
			if (levelExit.has_method("setObjectToDetect")):
				
				levelExit.setObjectToDetect(player)
			
			print("genStage: " + str(genStage))
		if genStage == 13:
			# Stage Name - Substage function
			
			print("genStage: " + str(genStage))
		
		
		genStage += 1
	
	
	#pass

func calculateFlatLandSegments():
	if (landscapeNode):
		if (landscapeNode.has_method("getFlatLandSegments")):
			flatLandSegments = landscapeNode.getFlatLandSegments()
	#pass

func generateBasicFlatLandTileMaps():
	var lowerCorner
	var upperCorner
	var xMax
	var zMax
	
	var individualBuildingAreaArray
	
	# Setting up multiple 2D Arrays for building generation areas
	for coordArray in flatLandSegments:
		lowerCorner = coordArray[0]
		upperCorner = coordArray[2]
		xMax = upperCorner.x - lowerCorner.x
		zMax = upperCorner.y - lowerCorner.y
		
		# Setting up individual 2D arrays
		individualBuildingAreaArray = []
		for xCount in range(xMax):
			individualBuildingAreaArray.append([])
			individualBuildingAreaArray[xCount] = []
			for zCount in range(zMax):
				individualBuildingAreaArray[xCount].append([])
				individualBuildingAreaArray[xCount][zCount] = "emptyTile"
				
		
		flatLandBuildingTileMaps.append(individualBuildingAreaArray)
		
	
	#pass

func generateBuildingMetaData():
	# dynamically calculate how many buildings can exist within each segment of flat ground
	
	# for each segment, augment the individual tile array accordingly
	var maxBuildings = 0
	var curBuildings = 0
	
	var areaSize
	var areaX
	var areaZ
	
	var ranX
	var ranZ
	
	var averageBuildingSize = 9
	var bufferQuotient = 15
	var hardMaxBuildings = 4
	var maxBuildingSizeVariance = 3
	
	var buildingOriginCoords
	var buildValid = false
	
	var buildSize = 9
	var curSize = 0
	
	var buildAttempts = 0
	var maxBuildAttempts = 3
	
	for flatAreaSegment in flatLandBuildingTileMaps:
		
		areaX = flatAreaSegment.size() - 1
		areaZ = flatAreaSegment[0].size() - 1
		areaSize = areaX * areaZ
		
		if (areaSize <= averageBuildingSize):
			maxBuildings = 1
		else:
			
			
			
			maxBuildings = 6
		
		# building generation
		curBuildings = 0
		while (curBuildings < maxBuildings):
			buildAttempts = 0
			
			curSize = 0
			buildSize = averageBuildingSize + rng.randi_range(-maxBuildingSizeVariance, maxBuildingSizeVariance)
			
			ranX = rng.randi_range(1, areaX-2)
			ranZ = rng.randi_range(1, areaZ-2)
			
			
			buildingOriginCoords = Vector2(ranX, ranZ)
			buildValid = false
			while (!buildValid):
				if (flatAreaSegment[buildingOriginCoords.x][buildingOriginCoords.y] == "emptyTile"):
					buildValid = true
				elif (buildAttempts < maxBuildAttempts):
					buildAttempts += 1
					ranX = rng.randi_range(1, areaX-2)
					ranZ = rng.randi_range(1, areaZ-2)
					buildingOriginCoords = Vector2(ranX, ranZ)
				else:
					curSize = buildSize
					buildValid = true
				#buildValid = true
			
			while (curSize < buildSize):
				if (curSize == 0):
					flatAreaSegment[buildingOriginCoords.x][buildingOriginCoords.y] = "a1"
				else:
					#Implement a breadth first search to spread the building out
					
					flatAreaSegment[buildingOriginCoords.x][buildingOriginCoords.y] = "a1"
				
				curSize += 1
			
			curBuildings += 1
		
	
	
	
	
	#pass

func instantiateBuildings():
	var spawnCoords = Vector3(0.0, 0.0, 0.0)
	var segmentIndex = 0
	var newRoom
	var arrayCoord
	var newRoomIndex = 0
	
	for buildingTileArray in flatLandBuildingTileMaps:
		for xCount in range(buildingTileArray.size()):
			for zCount in range(buildingTileArray[0].size()):
				if (buildingTileArray[xCount][zCount] != "emptyTile"):
					# Instantiate new rooms
					if (landscapeNode.has_method("getAbstractMeshArray")):
						# Will spawn rooms in the exact middle of the whole field
						arrayCoord = int((landscapeNode.getArrayWidth() / 2) * landscapeNode.getArrayWidth() + landscapeNode.getArrayHeight() / 2)
						
						# flatLandSegments is a 3 - dimensional array consisting of zones of co-ordinates for computing flat land
						
						
						#arrayCoord = (flatLandSegments[segmentIndex][xCount].x + xCount) * landscapeNode.getArrayWidth() + flatLandSegments[segmentIndex][xCount].y
						arrayCoord = (flatLandSegments[segmentIndex][0].x + xCount) * landscapeNode.getArrayWidth() + flatLandSegments[segmentIndex][0].y + zCount
						
						spawnCoords = landscapeNode.getAbstractMeshArray()[arrayCoord]
						
						#flatLandSegments[segmentIndex][0].x
						#flatLandSegments[segmentIndex][0].z
						
						#landscapeNode.getArrayHeight()
						#landscapeNode.getArrayWidth()
						
						#Instantiation of rooms starts here
						
						newRoomIndex = int(rng.randi_range(0, armoryRooms.size()-1))
						newRoom = armoryRooms[newRoomIndex].instance()
						self.add_child(newRoom)
						newRoom.global_translate(spawnCoords)
						
						if (newRoom.has_method("setInformedParent")):
							newRoom.setInformedParent(self)
							
						
						armoryRoomList.append(newRoom)
						
					
				
		
		segmentIndex += 1
		
	
	
	
	#pass

func instantiateLootInRooms():
	if (armoryRoomList.size() > 0):
		for room in armoryRoomList:
			if (room.has_method("spawn_items")):
				var itemList = []
				itemList += pistolAmmo
				itemList += buckshotAmmo
				itemList += hornetAmmo
				itemList += launcherAmmo
				room.spawn_items(itemList)
				
	
	if (medicalRoomList.size() > 0):
		for room in medicalRoomList:
			if (room.has_method("spawn_items")):
				var itemList = []
				itemList += healthSources
				room.spawn_items(itemList)
				
	
	if (barracksRoomList.size() > 0):
		for room in barracksRoomList:
			if (room.has_method("spawn_items")):
				var itemList = []
				itemList += moderateHostiles
				itemList += moderateHostiles
				itemList += pistolAmmo
				room.spawn_items(itemList)
				
	
	

func distributeItemsOverMap():
	var xPoint
	var zPoint
	var refPoint
	
	var landscapeArray = landscapeNode.getAbstractMeshArray()
	
	var spawnCoords = Vector3()
	var spawnCoordPool = []
	
	var validItemList = []
	validItemList += healthSources
	validItemList += pistolAmmo
	validItemList += buckshotAmmo
	validItemList += hornetAmmo
	validItemList += launcherAmmo
	
	
	# Adding functionality to incorporate heuristics for pickups
	var intervalPistol = initialScorePistol
	var intervalShotgun = initialScoreShotgun
	var intervalHornet = initialScoreHornet
	var intervalLauncher = initialScoreLauncher
	var intervalMed = 3.0 + (levelNumber / 5.0)
	if (pointScorePistol > 0.0 || pointScoreShotgun > 0.0 || pointScoreHornet > 0.0 || pointScoreLauncher > 0.0):
		intervalPistol += pointScorePistol
		intervalShotgun += pointScoreShotgun
		intervalHornet += pointScoreHornet
		intervalLauncher += pointScoreLauncher
	
	var maxIntervalValue = intervalPistol + intervalShotgun + intervalHornet + intervalLauncher + intervalMed
	
	
	
	for iCount in range(numItemsToSpawn):
		xPoint = rng.randi_range(1, landscapeNode.getArrayWidth() - 2)
		zPoint = rng.randi_range(1, landscapeNode.getArrayHeight() - 2)
		
		refPoint = xPoint * landscapeNode.getArrayWidth() + zPoint
		
		spawnCoords = Vector3(landscapeArray[refPoint].x, landscapeArray[refPoint].y+0.5, landscapeArray[refPoint].z)
		
		spawnCoordPool.append(spawnCoords)
		
		
	
	var newItem
	
	for spawningCoords in spawnCoordPool:
		if (validItemList.size() > 0):
			
			if (bUseHeuristicPointScore):
				# When using the heuristic point score,
				# Generate a random number between zero and the maxinterval
				# Depending on this number, select a list to use
				var ranNum = rand_range(0.0, maxIntervalValue)
				if (ranNum >= 0.0 && ranNum < intervalPistol):
					newItem = pistolAmmo[randi() % (pistolAmmo.size())].instance()
				elif (ranNum >= intervalPistol && ranNum < (intervalPistol + intervalShotgun)):
					newItem = buckshotAmmo[randi() % (buckshotAmmo.size())].instance()
				elif (ranNum >= (intervalPistol + intervalShotgun) && ranNum < (intervalPistol + intervalShotgun + intervalHornet)):
					newItem = hornetAmmo[randi() % (hornetAmmo.size())].instance()
				elif (ranNum >= (intervalPistol + intervalShotgun + intervalHornet) && ranNum < (intervalPistol + intervalShotgun + intervalHornet + intervalLauncher)):
					newItem = launcherAmmo[randi() % (launcherAmmo.size())].instance()
				elif (ranNum >= (intervalPistol + intervalShotgun + intervalHornet + intervalLauncher) && ranNum < (intervalPistol + intervalShotgun + intervalHornet + intervalLauncher + intervalMed)):
					newItem = healthSources[randi() % (healthSources.size())].instance()
				else:
					newItem = validItemList[randi() % (validItemList.size())].instance()
				
				
			else:
				newItem = validItemList[randi() % (validItemList.size())].instance()
			
			
			
			self.add_child(newItem)
			
			#newItem.global_transform = spawnpoints.get_child(iCount).global_transform
			newItem.global_translate(spawningCoords)
			newItem.scale = Vector3(1, 1, 1)
		
	
	
	#pass

func distributeHostilesOverMap():
	var xPoint
	var zPoint
	var refPoint
	
	var landscapeArray = landscapeNode.getAbstractMeshArray()
	
	var spawnCoords = Vector3()
	var spawnCoordPool = []
	
	var validHostilesList = []
	validHostilesList += lightHostiles
	validHostilesList += moderateHostiles
	validHostilesList += heavyHostiles
	
	# Functionality for Interval-based hostile selection
	var intervalLight = initialScoreLight
	var intervalModerate = initialScoreModerate
	var intervalHeavy = initialScoreHeavy
	
	if (pointScoreLight > 0.0 || pointScoreModerate > 0.0 || pointScoreHeavy > 0.0):
		intervalLight += pointScoreLight
		intervalModerate += pointScoreModerate
		intervalHeavy += pointScoreHeavy
		
	
	var maxIntervalValue = intervalLight + intervalModerate + intervalHeavy
	
	
	for iCount in range(numHostilesToSpawn):
		xPoint = rng.randi_range(1, landscapeNode.getArrayWidth() - 2)
		zPoint = rng.randi_range(1, landscapeNode.getArrayHeight() - 2)
		
		refPoint = xPoint * landscapeNode.getArrayWidth() + zPoint
		
		spawnCoords = Vector3(landscapeArray[refPoint].x, landscapeArray[refPoint].y+0.5, landscapeArray[refPoint].z)
		
		spawnCoordPool.append(spawnCoords)
		
		
	
	var newItem
	var chosenTemplate
	
	for spawningCoords in spawnCoordPool:
		if (validHostilesList.size() > 0):
			if (bUseHeuristicPointScore):
				var ranNum = rand_range(0.0, maxIntervalValue)
				if (ranNum >= 0.0 && ranNum < intervalLight):
					chosenTemplate = lightHostiles[randi() % (lightHostiles.size())]
				elif (ranNum >= intervalLight && ranNum < (intervalLight + intervalModerate)):
					chosenTemplate = moderateHostiles[randi() % (moderateHostiles.size())]
				elif (ranNum >= (intervalLight + intervalModerate) && ranNum < (intervalLight + intervalModerate + intervalHeavy)):
					chosenTemplate = heavyHostiles[randi() % (heavyHostiles.size())]
				else:
					chosenTemplate = validHostilesList[randi() % (validHostilesList.size())]
				
			else:
				chosenTemplate = validHostilesList[randi() % (validHostilesList.size())]
			
			
			
			#newItem = validHostilesList[randi() % (validHostilesList.size())].instance()
			
			
			#sceneRoot.add_child(newItem)
			
			#newItem.global_transform = spawnpoints.get_child(iCount).global_transform
			#newItem.global_translate(spawningCoords)
			#newItem.scale = Vector3(1, 1, 1)
			
			spawnHostile(chosenTemplate, spawningCoords)
			
		
	
	
	#pass

func spawnHostile(hostileTemplate, spawnCoords):
	var newHostile = hostileTemplate.instance()
	
	navManager.add_child(newHostile)
	
	#newItem.global_transform = spawnpoints.get_child(iCount).global_transform
	newHostile.global_translate(spawnCoords)
	newHostile.scale = Vector3(1, 1, 1)
	
	
	
	#pass

func generateLevelBarriers():
	var pointA = Vector3(0.0, 0.0, 0.0)
	
	var landscapeArray = landscapeNode.getAbstractMeshArray()
	var arrayWidth = landscapeNode.getArrayWidth()
	var arrayHeight = landscapeNode.getArrayHeight()
	
	var widthMidPoint = int(arrayWidth/2.0)
	var heightMidPoint = int(arrayHeight/2.0)
	
	var gridSize = landscapeNode.getGridSize()
	
	# Array Center at arrayWidth/2 * arrayWidth + arrayHeight/2
	# Scaling needed for "bottom" and "top": arrayWidth*gridSize for X
	# Scaling needed for "sides": arrayHeight*gridSize for Z
	var topBotScale = Vector3(arrayWidth*gridSize/2.0, 200.0, 1.0)
	var sideScale = Vector3(1.0, 200.0, arrayHeight*gridSize/2.0)
	
	var levelBarrier1 = levelBarrierTemplate.instance()
	self.add_child(levelBarrier1)
	
	levelBarrier1.global_translate(Vector3(widthMidPoint*gridSize, 0.0, -1.0))
	levelBarrier1.scale = topBotScale
	
	var levelBarrier2 = levelBarrierTemplate.instance()
	self.add_child(levelBarrier2)
	
	levelBarrier2.global_translate(Vector3(widthMidPoint*gridSize, 0.0, (arrayHeight-1)*gridSize+1.0))
	levelBarrier2.scale = topBotScale
	
	var levelBarrier3 = levelBarrierTemplate.instance()
	self.add_child(levelBarrier3)
	
	levelBarrier3.global_translate(Vector3(-1.0, 0.0, heightMidPoint*gridSize-1.0))
	levelBarrier3.scale = sideScale
	
	var levelBarrier4 = levelBarrierTemplate.instance()
	self.add_child(levelBarrier4)
	
	levelBarrier4.global_translate(Vector3((arrayWidth-1)*gridSize+1.0, 0.0, heightMidPoint*gridSize-1.0))
	levelBarrier4.scale = sideScale
	

func bakeNavigationMesh():
	
	navMeshA.navmesh.create_from_mesh(landscapeNode.getGeneratedMesh())
	
	#pass

func instantiateLevelStart():
	levelStart = levelStartTemplate.instance()
	self.add_child(levelStart)
	
	var spawningCoords = getRandomMapPosition()
	
	levelStart.global_translate(spawningCoords)
	levelStart.scale = Vector3(1.0, 1.0, 1.0)
	
	
	#pass

func instantiateLevelExit():
	levelExit = levelExitTemplate.instance()
	self.add_child(levelExit)
	
	var spawningCoords = getRandomMapPosition()
	
	levelExit.global_translate(spawningCoords)
	levelExit.scale = Vector3(1.0, 1.0, 1.0)
	
	levelExit.setMainGameObject(mainObject)
	
	#pass

func getRandomMapPosition():
	var areaX = landscapeNode.getArrayWidth()
	var areaZ = landscapeNode.getArrayHeight()
	
	var ranX = rng.randi_range(1, areaX-2)
	var ranZ = rng.randi_range(1, areaZ-2)
	
	
	
	var meshArray = landscapeNode.getAbstractMeshArray()
	
	var randomPosition = meshArray[ranX * areaX + ranZ]
	
	return randomPosition

func spawnPlayer():
	player = playerTemplate.instance()
	self.add_child(player)
	
	var spawningCoords = getStartCoords() + Vector3(0.0, 0.5, 0.0)
	
	player.global_translate(spawningCoords)
	player.scale = Vector3(1.0, 1.0, 1.0)
	
	#pass

func spawnPlayerSurrogate():
	player = playerSurrogateTemplate.instance()
	navManager.add_child(player)
	
	var spawningCoords = getStartCoords() + Vector3(0.0, 0.5, 0.0)
	
	player.global_translate(spawningCoords)
	player.scale = Vector3(1.0, 1.0, 1.0)
	
	#pass

func getStartCoords():
	var startCoords
	if (levelStart):
		startCoords = levelStart.global_transform.origin
	else:
		startCoords = Vector3(0.0, 0.0, 0.0)
	
	
	return startCoords
	

func softReset():
	levelBarriers = []
	levelStart = null
	levelExit = null
	
	player = null
	
	flatLandSegments = null
	flatLandBuildingTileMaps = []
	
	buildingList = null
	validBuildingGenGridSegments = null
	
	armoryRoomList = []
	medicalRoomList = []
	barracksRoomList = []
	
	rngSeed = 104
	rng = RandomNumberGenerator.new()
	rng.seed = rngSeed
	
	numHostilesToSpawn = 30
	numItemsToSpawn = 300
	

func setGenerationParameters(newSeed=104, numHostiles=30, numGenericItems=300):
	rngSeed = newSeed
	numHostilesToSpawn = numHostiles
	numItemsToSpawn = numGenericItems
	
	#pass

func establishSceneRoot():
	sceneRoot = get_tree().root.get_children()[0]
	
	#pass

func beginGeneration():
	genStage = 0
	rng = RandomNumberGenerator.new()
	rng.seed = rngSeed
	
	#pass

func beginGenerationFromScratch():
	genStage = 0
	sceneRoot = get_tree().root.get_children()[0]
	rng = RandomNumberGenerator.new()
	rng.seed = rngSeed
	
	
	#pass

func setSeed(newSeed):
	rngSeed = newSeed
	rng = RandomNumberGenerator.new()
	rng.seed = rngSeed
	if (landscapeNode.has_method("setSeed")):
		landscapeNode.setSeed(rngSeed)
	

func changePlayerSurrogateUsage(bUtiliseSurrogate):
	bUseSurrogate = bUtiliseSurrogate

func getGeneratedExit():
	return levelExit

func setMainObject(newMain):
	mainObject = newMain

func setPointScores(newPistolScore=0.0, newHornetScore=0.0, newShotgunScore=0.0, newLauncherScore=0.0, newLightScore=0.0, newModerateScore=0.0, newHeavyScore=0.0):
	pointScorePistol = newPistolScore
	pointScoreHornet = newHornetScore
	pointScoreShotgun = newShotgunScore
	pointScoreLauncher = newLauncherScore
	
	pointScoreLight = newLightScore
	pointScoreModerate = newModerateScore
	pointScoreHeavy = newHeavyScore
	
	var intervalPistol = initialScorePistol
	var intervalShotgun = initialScoreShotgun
	var intervalHornet = initialScoreHornet
	var intervalLauncher = initialScoreLauncher
	var intervalMed = 3.0
	if (pointScorePistol > 0.0):
		intervalPistol += pointScorePistol
		intervalShotgun += pointScoreShotgun
		intervalHornet += pointScoreHornet
		intervalLauncher += pointScoreLauncher
	
	print("Point Scores Set!")
	print("psP: " + str(pointScorePistol))
	print("iP: " + str(intervalPistol))
	
