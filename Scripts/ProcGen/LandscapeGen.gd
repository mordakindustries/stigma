extends CSGMesh

# Procedural Landscape Parameters
var surfaceTool

var abstractMeshArray = []
var noise
var noiseSeed = 104
var noiseOctaves = 4
var noisePeriod = 20.0
var noisePersistence = 0.7
var noiseRoughness = 0.01
var noiseScale = 500.0

var arrayHeight = 100
var arrayWidth = 100
var heightOffset = 5
var widthOffset = 5

var gridSize = 10.0

var flatLandSegments = []

# Collision
onready var collisionZone = get_node("CollisionArea/CollisionShape")

var collisionVerts = PoolVector3Array()

export(bool) var defaultGen = true

var meshMaterial = preload("res://ReusableMaterials/PlainMaterial.tres")


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if (defaultGen):
		generateBasicLandscapeWithCentralFlatArea()
	
	#ResourceSaver.save("res://Assets/PCG/generatedLandscapeFromST.tres", getGeneratedMesh(), 32)
	
	pass # Replace with function body.

func generateBasicLandscape():
	generateLandArray()
	generateLandscape()
	self.mesh.surface_set_material(0, meshMaterial)
	#self.invert_faces = true

func generateBasicLandscapeWithCentralFlatArea():
	generateLandArray()
	addFlatArea_Square(int(arrayWidth/2.0), int(arrayHeight/2.0), 15, 5)
	
	generateLandscape()
	self.mesh.surface_set_material(0, meshMaterial)
	#self.invert_faces = true

func generateLandArray():
	noise = OpenSimplexNoise.new()
	noise.seed = noiseSeed
	noise.octaves = noiseOctaves
	noise.persistence = noisePersistence
	
	var xPoint = 0.0
	var zPoint = 0.0
	var yPoint = 0.0
	
	for row in range(arrayHeight):
		for col in range(arrayWidth):
			xPoint = col * gridSize
			zPoint = row * gridSize
			
			yPoint = noise.get_noise_2d(xPoint * noiseRoughness, zPoint * noiseRoughness) * noiseScale
			
			abstractMeshArray.append(Vector3(xPoint, yPoint, zPoint))
			
	
	
	#pass

func addFlatArea_Square(centerX, centerZ, areaSize=2, smoothArea=2):
	#var bFlattenedGround = false
	var rowIndex = -1
	var colIndex = -1
	var maxRowIndex = -1
	var maxColIndex = -1
	var widthIndex = -1
	var heightIndex = -1
	var row = -1
	var col = -1
	var averageHeight = 0.0
	
	
	#var radialDistance = gridSize * radialIndex
	#row = centerX - radialIndex
	#col = centerZ - radialIndex
	if (heightOffset < 2 && widthOffset < 2):
		row = centerX - areaSize - smoothArea
		if (row < 0):
			row = 0
		
		maxRowIndex = row + 2*(areaSize + smoothArea)
		if (maxRowIndex > arrayHeight):
			maxRowIndex = arrayHeight
		
		col = centerZ - areaSize - smoothArea
		if (col < 0):
			col = 0
		
		maxColIndex = col + 2*(areaSize + smoothArea)
		if (maxColIndex > arrayWidth):
			maxColIndex = arrayWidth
		
		
	else:
		row = centerX - areaSize - smoothArea
		if (row < heightOffset):
			row = heightOffset
		
		maxRowIndex = row + 2*(areaSize + smoothArea)
		if (maxRowIndex > arrayHeight - heightOffset):
			maxRowIndex = arrayHeight - heightOffset
		
		col = centerZ - areaSize - smoothArea
		if (col < widthOffset):
			col = widthOffset
		
		maxColIndex = col + 2*(areaSize + smoothArea)
		if (maxColIndex > arrayWidth - widthOffset):
			maxColIndex = arrayWidth - widthOffset
		
	
	averageHeight = (abstractMeshArray[(centerX - areaSize) * arrayWidth + (centerZ - areaSize)].y
		 + abstractMeshArray[(centerX - areaSize) * arrayWidth + (centerZ + areaSize)].y
		 + abstractMeshArray[(centerX + areaSize) * arrayWidth + (centerZ - areaSize)].y
		 + abstractMeshArray[(centerX + areaSize) * arrayWidth + (centerZ + areaSize)].y) / 4.0
	
	var flatRow = row
	var flatCol = col
	var localRowIndex = 0
	var localColIndex = 0
	var debugTrackA
	var debugTrackB
	var debugTrackC
	
	while flatRow < maxRowIndex:
		while flatCol < maxColIndex:
			debugTrackA = abstractMeshArray[flatRow * arrayWidth + flatCol].y
			if (localRowIndex > smoothArea && localRowIndex < (2 * areaSize + smoothArea)):
				if (localColIndex > smoothArea && localColIndex < (2 * areaSize + smoothArea)):
					abstractMeshArray[flatRow * arrayWidth + flatCol].y = averageHeight
					debugTrackC = averageHeight
				else:
					abstractMeshArray[flatRow * arrayWidth + flatCol].y = lerp(averageHeight, abstractMeshArray[flatRow * arrayWidth + flatCol].y, 0.5)
					debugTrackC = lerp(averageHeight, abstractMeshArray[flatRow * arrayWidth + flatCol].y, 0.5)
				
			else:
				abstractMeshArray[flatRow * arrayWidth + flatCol].y = lerp(averageHeight, abstractMeshArray[flatRow * arrayWidth + flatCol].y, 0.5)
				debugTrackC = lerp(averageHeight, abstractMeshArray[flatRow * arrayWidth + flatCol].y, 0.5)
			
			debugTrackB = abstractMeshArray[flatRow * arrayWidth + flatCol].y
			debugTrackC = abstractMeshArray[flatRow * arrayWidth + flatCol].y
			localColIndex = localColIndex + 1
			flatCol = flatCol + 1
		
		flatCol = col
		localColIndex = 0
		localRowIndex = localRowIndex + 1
		flatRow = flatRow + 1
	
	#bFlattenedGround = true
	
	# Here we compute co-ordinates for the corners of the flat area
	var coordArray = []
	coordArray.append(Vector2(centerX - areaSize, centerZ - areaSize))
	coordArray.append(Vector2(centerX + areaSize, centerZ - areaSize))
	coordArray.append(Vector2(centerX + areaSize, centerZ + areaSize))
	coordArray.append(Vector2(centerX - areaSize, centerZ + areaSize))
	flatLandSegments.append(coordArray)
	
	#return averageHeight
	

func addFlatArea_Circle(centerX, centerZ, radialIndex=5, flatAreaIndex=3):
	var bFlattenedGround = false
	var rowIndex = -1
	var colIndex = -1
	var maxRowIndex = -1
	var maxColIndex = -1
	var widthIndex = -1
	var heightIndex = -1
	var row = -1
	var col = -1
	var averageHeight = 0.0
	
	while (!bFlattenedGround):
		var radialDistance = gridSize * radialIndex
		#row = centerX - radialIndex
		#col = centerZ - radialIndex
		if (heightOffset < 2 && widthOffset < 2):
			row = centerX - radialIndex
			if (row < 0):
				row = 0
			
			maxRowIndex = row + radialIndex + radialIndex
			if (maxRowIndex > arrayHeight):
				maxRowIndex = arrayHeight
			
			col = centerZ - radialIndex
			if (col < 0):
				col = 0
			
			maxColIndex = col + radialIndex + radialIndex
			if (maxColIndex > arrayWidth):
				maxColIndex = arrayWidth
			
			
		else:
			row = centerX - radialIndex
			if (row < heightOffset):
				row = heightOffset
			
			maxRowIndex = row + radialIndex + radialIndex
			if (maxRowIndex > arrayHeight - heightOffset):
				maxRowIndex = arrayHeight - heightOffset
			
			col = centerZ - radialIndex
			if (col < widthOffset):
				col = widthOffset
			
			maxColIndex = col + radialIndex + radialIndex
			if (maxColIndex > arrayWidth - widthOffset):
				maxColIndex = arrayWidth - widthOffset
			
		
		var centerOffset = int(sqrt((flatAreaIndex*flatAreaIndex)/2))
		var flatRadialDistance = gridSize * flatAreaIndex
		
		averageHeight = (abstractMeshArray[(centerX - centerOffset) * arrayWidth + (centerZ - centerOffset)].y
			 + abstractMeshArray[(centerX - centerOffset) * arrayWidth + (centerZ + centerOffset)].y
			 + abstractMeshArray[(centerX + centerOffset) * arrayWidth + (centerZ - centerOffset)].y
			 + abstractMeshArray[(centerX + centerOffset) * arrayWidth + (centerZ + centerOffset)].y) / 4.0
		
		var flatRow = row
		var flatCol = col
		
		while flatRow < maxRowIndex:
			while flatCol < maxColIndex:
				if (horizontalDistance(abstractMeshArray[row * arrayWidth + col], abstractMeshArray[centerX * arrayWidth + centerZ]) < flatRadialDistance):
					abstractMeshArray[flatRow * arrayWidth + flatCol].y = averageHeight
				else:
					abstractMeshArray[flatRow * arrayWidth + flatCol].y = lerp(averageHeight, abstractMeshArray[flatRow * arrayWidth + flatCol].y, 0.5)
				
				flatCol = flatCol + 1
			
			flatCol = col
			flatRow = flatRow + 1
		
		bFlattenedGround = true
		
	
	var coordArray = []
	coordArray.append(Vector2(centerX - flatAreaIndex, centerZ - flatAreaIndex))
	coordArray.append(Vector2(centerX + flatAreaIndex, centerZ - flatAreaIndex))
	coordArray.append(Vector2(centerX + flatAreaIndex, centerZ + flatAreaIndex))
	coordArray.append(Vector2(centerX - flatAreaIndex, centerZ + flatAreaIndex))
	flatLandSegments.append(coordArray)
	
	
	#return averageHeight
	

func horizontalDistance(pointA, pointB):
	var ePointA = Vector3(pointA.x, 0.0, pointA.z)
	var ePointB = Vector3(pointB.x, 0.0, pointB.z)
	return ePointA.distance_to(ePointB)

func generateLandscape():
	surfaceTool = SurfaceTool.new()
	surfaceTool.begin(Mesh.PRIMITIVE_TRIANGLES)
	
	for row in range(arrayHeight - 1):
		for col in range(arrayWidth - 1):
			# Add a triangle-pair, with each point being coloured differently
			#surfaceTool.add_color(Color.azure)
			#surfaceTool.add_uv(Vector2(0, 0))
			#surfaceTool.add_vertex(Vector3(0, 0, 0))
			
			# Triangle Logic visualised
			#	4		3
			#	
			#	
			#	2		1
			#
			#Clockwise: (1, 2, 3) , (3, 2, 4)
			#C-Clockwise: (1, 3, 2), (3, 4, 2)
			
			# Clockwise Mesh Assembly
			# First Triangle logic
			# For each point in the first Triangle, colour individually
			# Also add an individual UV point
			
			#point1
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[row * arrayWidth + col].x, abstractMeshArray[row * arrayWidth + col].z))
			surfaceTool.add_vertex(abstractMeshArray[row * arrayWidth + col])
			#point2
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[(row + 1) * arrayWidth + col].x, abstractMeshArray[(row + 1) * arrayWidth + col].z))
			surfaceTool.add_vertex(abstractMeshArray[(row + 1) * arrayWidth + col])
			#point3
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[row * arrayWidth + (col + 1)].x, abstractMeshArray[row * arrayWidth + (col + 1)].z))
			surfaceTool.add_vertex(abstractMeshArray[row * arrayWidth + (col + 1)])
			
			# Second Triangle logic
			#point3
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[row * arrayWidth + col + 1].x, abstractMeshArray[row * arrayWidth + col + 1].z))
			surfaceTool.add_vertex(abstractMeshArray[row * arrayWidth + col + 1])
			#point2
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[(row + 1) * arrayWidth + col].x, abstractMeshArray[(row + 1) * arrayWidth + col].z))
			surfaceTool.add_vertex(abstractMeshArray[(row + 1) * arrayWidth + col])
			#point4
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[(row + 1) * arrayWidth + (col + 1)].x, abstractMeshArray[(row + 1) * arrayWidth + (col + 1)].z))
			surfaceTool.add_vertex(abstractMeshArray[(row + 1) * arrayWidth + (col + 1)])
			
			# Counter-Clockwise Mesh Assembly
			# First Triangle logic
			# For each point in the first Triangle, colour individually
			# Also add an individual UV point
			
			#point1
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[row * arrayWidth + col].x, abstractMeshArray[row * arrayWidth + col].z))
			surfaceTool.add_vertex(abstractMeshArray[row * arrayWidth + col])
			#point3
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[row * arrayWidth + (col + 1)].x, abstractMeshArray[row * arrayWidth + (col + 1)].z))
			surfaceTool.add_vertex(abstractMeshArray[row * arrayWidth + (col + 1)])
			#point2
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[(row + 1) * arrayWidth + col].x, abstractMeshArray[(row + 1) * arrayWidth + col].z))
			surfaceTool.add_vertex(abstractMeshArray[(row + 1) * arrayWidth + col])
			
			# Second Triangle logic
			#point3
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[row * arrayWidth + col + 1].x, abstractMeshArray[row * arrayWidth + col + 1].z))
			surfaceTool.add_vertex(abstractMeshArray[row * arrayWidth + col + 1])
			#point4
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[(row + 1) * arrayWidth + (col + 1)].x, abstractMeshArray[(row + 1) * arrayWidth + (col + 1)].z))
			surfaceTool.add_vertex(abstractMeshArray[(row + 1) * arrayWidth + (col + 1)])
			#point2
			surfaceTool.add_color(Color.azure)
			surfaceTool.add_uv(Vector2(abstractMeshArray[(row + 1) * arrayWidth + col].x, abstractMeshArray[(row + 1) * arrayWidth + col].z))
			surfaceTool.add_vertex(abstractMeshArray[(row + 1) * arrayWidth + col])
			
			
			# Setting up collision data for the shape
			
			# Standard (clockwise) way, albeit incorrect
			collisionVerts.append(abstractMeshArray[row * arrayWidth + col])
			collisionVerts.append(abstractMeshArray[(row + 1) * arrayWidth + col])
			collisionVerts.append(abstractMeshArray[row * arrayWidth + (col + 1)])
			
			collisionVerts.append(abstractMeshArray[row * arrayWidth + col + 1])
			collisionVerts.append(abstractMeshArray[(row + 1) * arrayWidth + col])
			collisionVerts.append(abstractMeshArray[(row + 1) * arrayWidth + (col + 1)])
			
			# Inverse (counter-clockwise) way, possibly correct
			collisionVerts.append(abstractMeshArray[row * arrayWidth + col])
			collisionVerts.append(abstractMeshArray[row * arrayWidth + (col + 1)])
			collisionVerts.append(abstractMeshArray[(row + 1) * arrayWidth + col])
			
			collisionVerts.append(abstractMeshArray[row * arrayWidth + col + 1])
			collisionVerts.append(abstractMeshArray[(row + 1) * arrayWidth + (col + 1)])
			collisionVerts.append(abstractMeshArray[(row + 1) * arrayWidth + col])
			
			
	
	
	self.mesh = getGeneratedMesh()
	
	collisionZone.shape = ConcavePolygonShape.new()
	collisionZone.shape.set_faces(collisionVerts)
	
	#pass

func setSeed(newSeed):
	noiseSeed = newSeed

func getArrayWidth():
	return arrayWidth

func getFlatLandSegments():
	return flatLandSegments

func getArrayHeight():
	return arrayHeight

func getGridSize():
	return gridSize

func getAbstractMeshArray():
	return abstractMeshArray

func getGeneratedMesh():
	return surfaceTool.commit()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
