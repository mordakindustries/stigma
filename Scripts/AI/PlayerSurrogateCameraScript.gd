extends Camera


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var possibleObjects = []
var seenObjects = []

var recheckTime = 0.0
var recheckTimer = 3.0

var teamDesignation = "Player"

var hostileOffset = Vector3(0, 1.2, 0)
var itemOffset = Vector3(0, 0.01, 0)

var PlayerSurrogate

# Debugging
var bUseDebugLines = false
var debugLines = []

# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerSurrogate = get_parent().get_parent() # Maybe replace with a path
	
	pass # Replace with function body.

func checkActualVisibility(otherObject):
	#print("Visible?")
	
	# Add in logic to potentially detect visibility of the object
	var spaceState = get_world().direct_space_state
	if (otherObject):
		var ignoreList = [self, PlayerSurrogate, null]
		ignoreList.append_array(PlayerSurrogate.get_children())
		var result
		if (otherObject.has_method("getTeam")):
			result = spaceState.intersect_ray(self.global_transform.origin, otherObject.global_transform.origin + hostileOffset, ignoreList, 0b0111)
			if (bUseDebugLines):
				drawDebugLine(self.global_transform.origin, otherObject.global_transform.origin + hostileOffset)
		elif (otherObject.has_method("exitLevel")):
			result = spaceState.intersect_ray(self.global_transform.origin, otherObject.global_transform.origin, ignoreList)
		else:
			result = spaceState.intersect_ray(self.global_transform.origin, otherObject.global_transform.origin + itemOffset, ignoreList, 0b0111)
			if (bUseDebugLines):
				drawDebugLine(self.global_transform.origin, otherObject.global_transform.origin + itemOffset)
		if (result):
			if (result.collider == otherObject):
				#print("I see " + result.collider.name)
				return true
			else:
				return false
		else:
			#print("I do not see")
			return false
	else:
		print("False Alarm")
		return false
	
	

func addObjectToCheck(otherObject):
	possibleObjects.append(otherObject)
	

func removeObjectToCheck(otherObject):
	if (possibleObjects.has(otherObject)):
		possibleObjects.remove(possibleObjects.find(otherObject))
		print("Bye possible object.")
	
	if (seenObjects.has(otherObject)):
		seenObjects.remove(seenObjects.find(otherObject))
		# could double-check if the object is a known pickup/enemy
		print("Bye seen object.")
	

func _physics_process(delta):
	process_vision(delta)
	if (recheckTime >= recheckTimer):
		reprocess_vision(delta)
		if (bUseDebugLines):
			clearDebugLines()
		recheckTime = recheckTime - recheckTimer
	else:
		recheckTime = recheckTime + delta
	
	
	

func process_vision(delta):
	if possibleObjects.size() >= 1:
		var curCount = 0
		var maxCount = possibleObjects.size()
		var testItem
		
		while curCount < maxCount:
			testItem = possibleObjects[curCount]
			if (checkActualVisibility(testItem)):
				if (!seenObjects.has(testItem)):
					seenObjects.append(testItem)
					if PlayerSurrogate.has_method("interpretObject"):
						PlayerSurrogate.interpretObject(testItem)
				
				possibleObjects.remove(curCount)
				maxCount = maxCount - 1
				
			else:
				curCount = curCount + 1
				
			
			
		
	
	
	

func reprocess_vision(delta):
	if seenObjects.size() >= 1:
		var curCount = 0
		var maxCount = seenObjects.size()
		var testItem
		
		while curCount < maxCount:
			testItem = seenObjects[curCount]
			if (!checkActualVisibility(testItem)):
				print("No longer see " + seenObjects[curCount].name)
				if (!possibleObjects.has(testItem)):
					possibleObjects.append(testItem)
				
				seenObjects.remove(curCount)
				maxCount = maxCount - 1
				
			else:
				curCount = curCount + 1
				
			
			
		
	
	

func getSeenObjects():
	return seenObjects
	

func getPossibleObjects():
	return possibleObjects
	

func getSeenObjectIDs():
	var objectIDArray = []
	for item in seenObjects:
		objectIDArray.append(item.name)
	
	
	return objectIDArray

func drawDebugLine(pointA, pointB):
	var newDebugLine = Mesh.new()
	newDebugLine.begin(PrimitiveMesh.PRIMITIVE_LINES)
	newDebugLine.set_color(Color(1, 1, 1))
	newDebugLine.add_vertex(pointA)
	newDebugLine.add_vertex(pointB)
	newDebugLine.end()
	
	debugLines.append(newDebugLine)
	
	
	
	
	
	

func clearDebugLines():
	debugLines.clear()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
